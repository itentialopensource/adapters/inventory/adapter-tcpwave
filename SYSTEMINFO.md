# TCPWave

Vendor: TCPWave
Homepage: https://www.tcpwave.com/

Product: TCPWave
Product Page: https://www.tcpwave.com/

## Introduction
We classify TCPWave into the Inventory and Network Services domains as TCPWave provides information on IP Addresses and DHCP. 

"Empower your network with seamless DDI and ADC solutions for enhanced performance, security, and efficiency." 

## Why Integrate
The TCPWave adapter from Itential is used to integrate the Itential Automation Platform (IAP) with TCPWave DDI to offer a DNS, DHCP and IPAM solution for configuration and orchestration. 

With this adapter you have the ability to perform operations with TCPWave such as:

- IPAM
- DNS
- DHCP

## Additional Product Documentation
The [API documents for TCPWave](https://www.tcpwave.com/restful-api/)
The [Information for TCPWave API](https://www.tcpwave.com/Guides/TCPWave_IPAM_RESTAPI_Guide_v11.33P1_February_2023.pdf)
