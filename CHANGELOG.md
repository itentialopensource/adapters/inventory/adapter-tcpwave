
## 0.4.4 [10-15-2024]

* Changes made at 2024.10.14_21:23PM

See merge request itentialopensource/adapters/adapter-tcpwave!22

---

## 0.4.3 [09-18-2024]

* add workshop and fix vulnerabilities

See merge request itentialopensource/adapters/adapter-tcpwave!20

---

## 0.4.2 [08-14-2024]

* Changes made at 2024.08.14_19:40PM

See merge request itentialopensource/adapters/adapter-tcpwave!19

---

## 0.4.1 [08-08-2024]

* Changes made at 2024.08.08_13:17PM

See merge request itentialopensource/adapters/adapter-tcpwave!18

---

## 0.4.0 [05-10-2024]

* Minor/2024 auto migration

See merge request itentialopensource/adapters/adapter-tcpwave!16

---

## 0.3.4 [03-26-2024]

* Changes made at 2024.03.26_14:47PM

See merge request itentialopensource/adapters/inventory/adapter-tcpwave!15

---

## 0.3.3 [03-13-2024]

* Changes made at 2024.03.13_14:04PM

See merge request itentialopensource/adapters/inventory/adapter-tcpwave!14

---

## 0.3.2 [03-11-2024]

* Changes made at 2024.03.11_14:13PM

See merge request itentialopensource/adapters/inventory/adapter-tcpwave!13

---

## 0.3.1 [02-26-2024]

* Changes made at 2024.02.26_13:42PM

See merge request itentialopensource/adapters/inventory/adapter-tcpwave!12

---

## 0.3.0 [12-30-2023]

* Adapter Engine has been updated and the changes are being migrated to the adapter

See merge request itentialopensource/adapters/inventory/adapter-tcpwave!11

---

## 0.2.0 [05-23-2022]

* Migration to the latest Adapter Foundation

See merge request itentialopensource/adapters/inventory/adapter-tcpwave!4

---

## 0.1.4 [03-15-2021]

- Migration to bring up to the latest foundation
	- Change to .eslintignore (adapter_modification directory)
	- Change to README.md (new properties, new scripts, new processes)
	- Changes to adapterBase.js (new methods)
	- Changes to package.json (new scripts, dependencies)
	- Changes to propertiesSchema.json (new properties and changes to existing)
	- Changes to the Unit test
	- Adding several test files, utils files and .generic entity
	- Fix order of scripts and dependencies in package.json
	- Fix order of properties in propertiesSchema.json
	- Update sampleProperties, unit and integration tests to have all new properties.
	- Add all new calls to adapter.js and pronghorn.json
	- Add suspend piece to older methods

See merge request itentialopensource/adapters/inventory/adapter-tcpwave!3

---

## 0.1.3 [07-10-2020]

- Update to the latest adapter foundation

See merge request itentialopensource/adapters/inventory/adapter-tcpwave!2

---

## 0.1.2 [01-15-2020]

- Update the adapter to the latest adapter foundation

See merge request itentialopensource/adapters/inventory/adapter-tcpwave!1

---

## 0.1.1 [11-12-2019]

- Initial Commit

See commit fde9e41

---
