## Using this Adapter

The `adapter.js` file contains the calls the adapter makes available to the rest of the Itential Platform. The API detailed for these calls should be available through JSDOC. The following is a brief summary of the calls.

### Generic Adapter Calls

These are adapter methods that IAP or you might use. There are some other methods not shown here that might be used for internal adapter functionality.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">connect()</td>
    <td style="padding:15px">This call is run when the Adapter is first loaded by he Itential Platform. It validates the properties have been provided correctly.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">healthCheck(callback)</td>
    <td style="padding:15px">This call ensures that the adapter can communicate with Adapter for TCPWave. The actual call that is used is defined in the adapter properties and .system entities action.json file.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">refreshProperties(properties)</td>
    <td style="padding:15px">This call provides the adapter the ability to accept property changes without having to restart the adapter.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">encryptProperty(property, technique, callback)</td>
    <td style="padding:15px">This call will take the provided property and technique, and return the property encrypted with the technique. This allows the property to be used in the adapterProps section for the credential password so that the password does not have to be in clear text. The adapter will decrypt the property as needed for communications with Adapter for TCPWave.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">iapUpdateAdapterConfiguration(configFile, changes, entity, type, action, callback)</td>
    <td style="padding:15px">This call provides the ability to update the adapter configuration from IAP - includes actions, schema, mockdata and other configurations.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapSuspendAdapter(mode, callback)</td>
    <td style="padding:15px">This call provides the ability to suspend the adapter and either have requests rejected or put into a queue to be processed after the adapter is resumed.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapUnsuspendAdapter(callback)</td>
    <td style="padding:15px">This call provides the ability to resume a suspended adapter. Any requests in queue will be processed before new requests.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetAdapterQueue(callback)</td>
    <td style="padding:15px">This call will return the requests that are waiting in the queue if throttling is enabled.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapFindAdapterPath(apiPath, callback)</td>
    <td style="padding:15px">This call provides the ability to see if a particular API path is supported by the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapTroubleshootAdapter(props, persistFlag, adapter, callback)</td>
    <td style="padding:15px">This call can be used to check on the performance of the adapter - it checks connectivity, healthcheck and basic get calls.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterHealthcheck(adapter, callback)</td>
    <td style="padding:15px">This call will return the results of a healthcheck.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterConnectivity(callback)</td>
    <td style="padding:15px">This call will return the results of a connectivity check.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterBasicGet(callback)</td>
    <td style="padding:15px">This call will return the results of running basic get API calls.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapMoveAdapterEntitiesToDB(callback)</td>
    <td style="padding:15px">This call will push the adapter configuration from the entities directory into the Adapter or IAP Database.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapDeactivateTasks(tasks, callback)</td>
    <td style="padding:15px">This call provides the ability to remove tasks from the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapActivateTasks(tasks, callback)</td>
    <td style="padding:15px">This call provides the ability to add deactivated tasks back into the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapExpandedGenericAdapterRequest(metadata, uriPath, restMethod, pathVars, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This is an expanded Generic Call. The metadata object allows us to provide many new capabilities within the generic request.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">genericAdapterRequest(uriPath, restMethod, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This call allows you to provide the path to have the adapter call. It is an easy way to incorporate paths that have not been built into the adapter yet.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">genericAdapterRequestNoBasePath(uriPath, restMethod, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This call is the same as the genericAdapterRequest only it does not add a base_path or version to the call.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterLint(callback)</td>
    <td style="padding:15px">Runs lint on the addapter and provides the information back.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterTests(callback)</td>
    <td style="padding:15px">Runs baseunit and unit tests on the adapter and provides the information back.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetAdapterInventory(callback)</td>
    <td style="padding:15px">This call provides some inventory related information about the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
  
### Adapter Cache Calls

These are adapter methods that are used for adapter caching. If configured, the adapter will cache based on the interval provided. However, you can force a population of the cache manually as well.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">iapPopulateEntityCache(entityTypes, callback)</td>
    <td style="padding:15px">This call populates the adapter cache.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRetrieveEntitiesCache(entityType, options, callback)</td>
    <td style="padding:15px">This call retrieves the specific items from the adapter cache.</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
  
### Adapter Broker Calls

These are adapter methods that are used to integrate to IAP Brokers. This adapter currently supports the following broker calls.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">hasEntities(entityType, entityList, callback)</td>
    <td style="padding:15px">This call is utilized by the IAP Device Broker to determine if the adapter has a specific entity and item of the entity.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getDevice(deviceName, callback)</td>
    <td style="padding:15px">This call returns the details of the requested device.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getDevicesFiltered(options, callback)</td>
    <td style="padding:15px">This call returns the list of devices that match the criteria provided in the options filter.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">isAlive(deviceName, callback)</td>
    <td style="padding:15px">This call returns whether the device status is active</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfig(deviceName, format, callback)</td>
    <td style="padding:15px">This call returns the configuration for the selected device.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetDeviceCount(callback)</td>
    <td style="padding:15px">This call returns the count of devices.</td>
    <td style="padding:15px">No</td>
  </tr>
</table>
<br>

### Specific Adapter Calls

Specific adapter calls are built based on the API of the TCPWave. The Adapter Builder creates the proper method comments for generating JS-DOC for the adapter. This is the best way to get information on the calls.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Path</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">aclCreate(body, callback)</td>
    <td style="padding:15px">ACL Create</td>
    <td style="padding:15px">{base_path}/{version}/acls/create?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">aclUpdate(body, callback)</td>
    <td style="padding:15px">ACL Edit</td>
    <td style="padding:15px">{base_path}/{version}/acls/update?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">aclList(callback)</td>
    <td style="padding:15px">ACL List</td>
    <td style="padding:15px">{base_path}/{version}/acls/list?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">aclGet(name, callback)</td>
    <td style="padding:15px">ACL Details</td>
    <td style="padding:15px">{base_path}/{version}/acls/get?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">aclGetReferences(name, callback)</td>
    <td style="padding:15px">ACL References</td>
    <td style="padding:15px">{base_path}/{version}/acls/getreferences?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">aclMultiDelete(body, callback)</td>
    <td style="padding:15px">ACL Delete Multiple</td>
    <td style="padding:15px">{base_path}/{version}/acls/multidelete?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">aclPage(page, rows, callback)</td>
    <td style="padding:15px">Search ACL</td>
    <td style="padding:15px">{base_path}/{version}/acls/search?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">adUserAdd(body, callback)</td>
    <td style="padding:15px">Active Directory User Add</td>
    <td style="padding:15px">{base_path}/{version}/activedirectory/useradd?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">adUserList(callback)</td>
    <td style="padding:15px">Active Directory User list</td>
    <td style="padding:15px">{base_path}/{version}/activedirectory/userlist?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">adUserDelete(body, callback)</td>
    <td style="padding:15px">Active Directory User Delete</td>
    <td style="padding:15px">{base_path}/{version}/activedirectory/delete?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">adUserAuthenticate(body, callback)</td>
    <td style="padding:15px">Authenticate Active Directory User</td>
    <td style="padding:15px">{base_path}/{version}/activedirectory/authenticate?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">del(id, name, organizationId, organizationName, callback)</td>
    <td style="padding:15px">Admin Group Delete</td>
    <td style="padding:15px">{base_path}/{version}/admingroup/delete?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">edit(body, callback)</td>
    <td style="padding:15px">Admin Group Modify</td>
    <td style="padding:15px">{base_path}/{version}/admingroup/update?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">adminGroupPage(page, rows, callback)</td>
    <td style="padding:15px">Search Admin Group</td>
    <td style="padding:15px">{base_path}/{version}/admingroup/search?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">get(adminGroupId, adminGroupName, callback)</td>
    <td style="padding:15px">Admin Group Details</td>
    <td style="padding:15px">{base_path}/{version}/admingroup/get?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">list(orgId, orgName, callback)</td>
    <td style="padding:15px">Admin Group List</td>
    <td style="padding:15px">{base_path}/{version}/admingroup/list?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">create(body, callback)</td>
    <td style="padding:15px">Admin Group Add</td>
    <td style="padding:15px">{base_path}/{version}/admingroup/add?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">categoryList(callback)</td>
    <td style="padding:15px">Threshold Category List</td>
    <td style="padding:15px">{base_path}/{version}/alarmSubscription/categoryList?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listCategoryMonitorService(category, callback)</td>
    <td style="padding:15px">List Monitoring Services</td>
    <td style="padding:15px">{base_path}/{version}/alarmSubscription/thresholdList?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listComponents(serviceList, orgName, draw, start, length, filterRules, sort, order, callback)</td>
    <td style="padding:15px">List Alarm Subscription Components</td>
    <td style="padding:15px">{base_path}/{version}/alarmSubscription/componentList?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addSubscriptions(body, callback)</td>
    <td style="padding:15px">Add Alarm Subscriptions</td>
    <td style="padding:15px">{base_path}/{version}/alarmSubscription/add?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteSubscription(body, callback)</td>
    <td style="padding:15px">Delete Alarm Subscriptions</td>
    <td style="padding:15px">{base_path}/{version}/alarmSubscription/delete?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAlarmSubscriptionList(draw, start, length, filterRules, sort, order, callback)</td>
    <td style="padding:15px">Subscription List</td>
    <td style="padding:15px">{base_path}/{version}/alarmSubscription/list?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">tsigList(callback)</td>
    <td style="padding:15px">Algorithm TSIG List</td>
    <td style="padding:15px">{base_path}/{version}/algo/tsiglist?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAlgoList(callback)</td>
    <td style="padding:15px">Algorithm List</td>
    <td style="padding:15px">{base_path}/{version}/algo/list?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postAppliancegroupEdit(body, callback)</td>
    <td style="padding:15px">Edit Appliance Group</td>
    <td style="padding:15px">{base_path}/{version}/appliancegroup/edit?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getApplainceGroupReferences(name, organization, callback)</td>
    <td style="padding:15px">References of Appliance Group</td>
    <td style="padding:15px">{base_path}/{version}/appliancegroup/references?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getApplianceGroup(name, organization, callback)</td>
    <td style="padding:15px">Details of Appliance Group</td>
    <td style="padding:15px">{base_path}/{version}/appliancegroup/get?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteApplianceGroup(name, organization, callback)</td>
    <td style="padding:15px">Appliance Group Delete</td>
    <td style="padding:15px">{base_path}/{version}/appliancegroup/delete?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listByOrg(organization, callback)</td>
    <td style="padding:15px">Appliance Group List</td>
    <td style="padding:15px">{base_path}/{version}/appliancegroup/listbyorg?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">applianceGroupSearchPage(draw, start, length, userId, filterRules, sort, order, callback)</td>
    <td style="padding:15px">Searches for appliance groups</td>
    <td style="padding:15px">{base_path}/{version}/appliancegroup/search?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAppliancegroupList(draw, start, length, filterRules, sort, order, callback)</td>
    <td style="padding:15px">List Appliance Group</td>
    <td style="padding:15px">{base_path}/{version}/appliancegroup/list?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postAppliancegroupAdd(body, callback)</td>
    <td style="padding:15px">Add Appliance Group</td>
    <td style="padding:15px">{base_path}/{version}/appliancegroup/add?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postAssetEdit(body, callback)</td>
    <td style="padding:15px">Update Asset</td>
    <td style="padding:15px">{base_path}/{version}/asset/edit?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteAll(body, callback)</td>
    <td style="padding:15px">Delete Assets</td>
    <td style="padding:15px">{base_path}/{version}/asset/deleteAll?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAssetGet(serviceTag, callback)</td>
    <td style="padding:15px">Get Asset Details</td>
    <td style="padding:15px">{base_path}/{version}/asset/get?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAssetList(callback)</td>
    <td style="padding:15px">Asset List</td>
    <td style="padding:15px">{base_path}/{version}/asset/list?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postAssetAdd(body, callback)</td>
    <td style="padding:15px">Add Asset</td>
    <td style="padding:15px">{base_path}/{version}/asset/add?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getReportRecordsForGrid(fromDate, toDate, draw, start, length, filterRules, sort, order, callback)</td>
    <td style="padding:15px">Backup Audit</td>
    <td style="padding:15px">{base_path}/{version}/auditbackup/reportlistbydate?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">upload(body, callback)</td>
    <td style="padding:15px">Audit backup files Upload</td>
    <td style="padding:15px">{base_path}/{version}/auditbackup/upload?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">auditBackupList(callback)</td>
    <td style="padding:15px">Audit Backup files List</td>
    <td style="padding:15px">{base_path}/{version}/auditbackup/list?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">auditBackupListBySearch(searchVal, callback)</td>
    <td style="padding:15px">Audit Backup files List</td>
    <td style="padding:15px">{base_path}/{version}/auditbackup/listsearchbackupfiles?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">auditBackupFilesReportList(fileName, draw, start, length, filterRules, sort, order, callback)</td>
    <td style="padding:15px">Audit Backup Report by file name</td>
    <td style="padding:15px">{base_path}/{version}/auditbackup/reportlistbyfilename?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAuditreportsReportlist(reportType, fromDate, toDate, actionType, ipAddressDomainName, user, domain, sourceIp, recordType, changeTicket, orgId, proxy, draw, start, length, filterRules, action, sort, order, mac, organization, id, callback)</td>
    <td style="padding:15px">Admin Audit</td>
    <td style="padding:15px">{base_path}/{version}/auditreports/reportlist?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">sendReportByEmail(body, adminName, adminRole, action, callback)</td>
    <td style="padding:15px">Email Report</td>
    <td style="padding:15px">{base_path}/{version}/auditreports/emailreport?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAdminsList(actionType, callback)</td>
    <td style="padding:15px">Admin List</td>
    <td style="padding:15px">{base_path}/{version}/auditreports/adminslist?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAdminRolesList(callback)</td>
    <td style="padding:15px">Admin Role List</td>
    <td style="padding:15px">{base_path}/{version}/auditreports/adminroleslist?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getActionsList(callback)</td>
    <td style="padding:15px">Action List</td>
    <td style="padding:15px">{base_path}/{version}/auditreports/actionslist?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAuditNetworksList(orgId, draw, start, length, actionType, filterRules, q, callback)</td>
    <td style="padding:15px">Zone List</td>
    <td style="padding:15px">{base_path}/{version}/auditreports/auditzoneslist?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAuditreportsAuditnetworkslist(draw, start, length, actionType, orgId, filterRules, q, callback)</td>
    <td style="padding:15px">IPV4 Network List</td>
    <td style="padding:15px">{base_path}/{version}/auditreports/auditnetworkslist?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getIPv4NetworkList(callback)</td>
    <td style="padding:15px">IPv4 Network Mask Length</td>
    <td style="padding:15px">{base_path}/{version}/auditreports/ipv4Network?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getIPv4SubnetList(callback)</td>
    <td style="padding:15px">IPv4 Subnet Subnets mask lengths</td>
    <td style="padding:15px">{base_path}/{version}/auditreports/ipv4Subnet?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getOFACCountryGridList(callback)</td>
    <td style="padding:15px">ccTLDs Grid List</td>
    <td style="padding:15px">{base_path}/{version}/auditreports/ofaccountriesgridlist?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getReportsReportlist(reportType, fromDate, toDate, actionType, ipAddressDomainName, orgId, organization, draw, start, length, filterRules, action, sort, serverIp, macAddress, hostName, order, callback)</td>
    <td style="padding:15px">Lists all Reports</td>
    <td style="padding:15px">{base_path}/{version}/reports/reportlist?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSubnetGroupList(draw, start, length, filterRules, q, callback)</td>
    <td style="padding:15px">Subnet Groups List</td>
    <td style="padding:15px">{base_path}/{version}/reports/listsubnetgroup?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getReportsDhcpActiveLeases(reportType, fromDate, toDate, serverIp, page, rows, callback)</td>
    <td style="padding:15px">DHCP active leases audit report</td>
    <td style="padding:15px">{base_path}/{version}/reports/dhcpActiveLeases?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postReportsCsvreport(body, callback)</td>
    <td style="padding:15px">CSV Report</td>
    <td style="padding:15px">{base_path}/{version}/reports/csvreport?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getReportsServertozonecount(organizationName, callback)</td>
    <td style="padding:15px">DNS Server to zone count result</td>
    <td style="padding:15px">{base_path}/{version}/reports/servertozonecount?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postReportsPdfreport(body, callback)</td>
    <td style="padding:15px">PDF Report</td>
    <td style="padding:15px">{base_path}/{version}/reports/pdfreport?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postReportsEmailreport(body, callback)</td>
    <td style="padding:15px">Email Report</td>
    <td style="padding:15px">{base_path}/{version}/reports/emailreport?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">schedemailreport(body, repeat, frequency, runtime, starttime, endtime, day, description, repeatInterval, repeatCount, adminName, adminRole, callback)</td>
    <td style="padding:15px">Schedule Report Email</td>
    <td style="padding:15px">{base_path}/{version}/reports/sched_emailreport?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">allParamsList(serverTypeCode, entityTypeCode, draw, start, length, filterRules, q, callback)</td>
    <td style="padding:15px">Parameter List</td>
    <td style="padding:15px">{base_path}/{version}/reports/allparams?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">generateScheduleCsvReport(body, callback)</td>
    <td style="padding:15px">CSV report for Scheduled Events</td>
    <td style="padding:15px">{base_path}/{version}/reports/schedulecsvreport?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getReportRecordsForViwsGrid(reportType, fromDate, toDate, viewName, orgId, draw, start, length, q, filterRules, action, sort, order, callback)</td>
    <td style="padding:15px">Lists all Columns for DNS Views Report</td>
    <td style="padding:15px">{base_path}/{version}/reports/viewsgridlist?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getReportRecordsForRPZLogs(reportType, serverIp, clientIp, draw, start, length, q, filterRules, action, sort, order, callback)</td>
    <td style="padding:15px">Lists all Columns for RPZ Logs Report</td>
    <td style="padding:15px">{base_path}/{version}/reports/rpzlogsgridlist?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDHCPServerSubnetReportRecordsForGrid(reportType, ipAddressDomainName, draw, start, length, q, filterRules, action, sort, order, callback)</td>
    <td style="padding:15px">DHCP Appliance Association audit report</td>
    <td style="padding:15px">{base_path}/{version}/reports/dhcpserversubnetreportlist?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDHCPTemplAssoReportRecordsForGrid(reportType, optionTempl, assoType, draw, start, length, q, filterRules, action, sort, order, callback)</td>
    <td style="padding:15px">DHCP Option Template Association Report</td>
    <td style="padding:15px">{base_path}/{version}/reports/dhcptemplassoreportgrid?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDhcpOptionConfigReportRecordsForGrid(reportType, configName, paramVal, draw, start, length, q, filterRules, action, sort, order, callback)</td>
    <td style="padding:15px">DHCP Option Template Report</td>
    <td style="padding:15px">{base_path}/{version}/reports/dhcpoptionconfigreportlist?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getScheduleReportRecordsForGrid(reportType, fromDate, toDate, ipAddressDomainName, userId, jobId, draw, start, length, q, filterRules, action, sort, order, callback)</td>
    <td style="padding:15px">Schedule Events Audit Report</td>
    <td style="padding:15px">{base_path}/{version}/reports/schedulereportlist?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getServerConfigReportRecordsForGrid(reportType, fromDate, toDate, ipAddressDomainName, serverType, draw, start, length, q, filterRules, action, sort, order, callback)</td>
    <td style="padding:15px">Appliance configuration audit report</td>
    <td style="padding:15px">{base_path}/{version}/reports/serverConfigreportlist?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDHSSOAReportForGrid(reportType, serverIp, serverName, serverType, page, rows, callback)</td>
    <td style="padding:15px">DNS SOA Report Grids</td>
    <td style="padding:15px">{base_path}/{version}/reports/dnssoareportgrid?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDhcpLeaseReportForCsv(reportType, serverIp, page, rows, callback)</td>
    <td style="padding:15px">DHCP active leases Audit Report</td>
    <td style="padding:15px">{base_path}/{version}/reports/dhcpLeaseCsvReport?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDnsSOARptForCsv(reportType, serverName, serverIp, serverType, page, rows, callback)</td>
    <td style="padding:15px">DNS SOA Report CSV</td>
    <td style="padding:15px">{base_path}/{version}/reports/dnssoareportcsv?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDnsServersList(actionType, callback)</td>
    <td style="padding:15px">DNS Servers List</td>
    <td style="padding:15px">{base_path}/{version}/reports/dnsserverslist?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDnsAppliancebyViewlist(actionType, organization, orgId, callback)</td>
    <td style="padding:15px">DNS AUTH Appliance</td>
    <td style="padding:15px">{base_path}/{version}/reports/dnsappliancebyviewlist?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDnsZonebyViewlist(actionType, organization, orgId, callback)</td>
    <td style="padding:15px">DNS Zone list</td>
    <td style="padding:15px">{base_path}/{version}/reports/dnszonebyviewlist?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDnsTemplateList(actionType, callback)</td>
    <td style="padding:15px">DNS Template List</td>
    <td style="padding:15px">{base_path}/{version}/reports/dnstemplatelist?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getZoneTemplateList(actionType, callback)</td>
    <td style="padding:15px">Zone Template List</td>
    <td style="padding:15px">{base_path}/{version}/reports/zonetemplauditlist?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getResourseRecordList(callback)</td>
    <td style="padding:15px">Owner List</td>
    <td style="padding:15px">{base_path}/{version}/reports/resourserecordauditlist?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDnsOptionTemplateList(actionType, callback)</td>
    <td style="padding:15px">DNS option template List</td>
    <td style="padding:15px">{base_path}/{version}/reports/dnsoptiontemplauditlist?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDnsDhcpServersList(callback)</td>
    <td style="padding:15px">DNS and DHCP Appliance List</td>
    <td style="padding:15px">{base_path}/{version}/reports/dnsdhcpserverslist?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDhcpServerList(actionType, callback)</td>
    <td style="padding:15px">DHCP Appliance List</td>
    <td style="padding:15px">{base_path}/{version}/reports/DhcpServerlist?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDhcpLeaseServerList(callback)</td>
    <td style="padding:15px">DHCP Active Server List</td>
    <td style="padding:15px">{base_path}/{version}/reports/DhcpLeaseServerlist?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDhcpServerSubnetList(actionType, callback)</td>
    <td style="padding:15px">DHCP Appliance Subnet List</td>
    <td style="padding:15px">{base_path}/{version}/reports/DhcpServerSubnetlist?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDhcpOptionTemplateList(draw, start, length, actionType, filterRules, q, callback)</td>
    <td style="padding:15px">DHCP Option Template List</td>
    <td style="padding:15px">{base_path}/{version}/reports/dhcpoptiontemplauditlist?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDhcpPolicyTemplateList(actionType, callback)</td>
    <td style="padding:15px">DHCP Policy Template List</td>
    <td style="padding:15px">{base_path}/{version}/reports/dhcppolicytemplauditlist?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDhcpTemplateAssoList(draw, start, length, filterRules, q, callback)</td>
    <td style="padding:15px">DHCP Option Template List</td>
    <td style="padding:15px">{base_path}/{version}/reports/dhcpoptiontemplassolist?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getScheduleAdminAuditList(actionType, callback)</td>
    <td style="padding:15px">Administrator List</td>
    <td style="padding:15px">{base_path}/{version}/reports/scheduleadminauditlist?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getserverConfigList(callback)</td>
    <td style="padding:15px">DNS Appliances List</td>
    <td style="padding:15px">{base_path}/{version}/reports/serverConfigReportlist?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getmonitoringHostsList(callback)</td>
    <td style="padding:15px">Host List</td>
    <td style="padding:15px">{base_path}/{version}/reports/monitoringhostslist?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getMonitoringServicesList(hostId, callback)</td>
    <td style="padding:15px">Monitoring Service List</td>
    <td style="padding:15px">{base_path}/{version}/reports/monitoringserviceslist?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getObjectTypeList(callback)</td>
    <td style="padding:15px">Object Type List</td>
    <td style="padding:15px">{base_path}/{version}/reports/objecttypelist?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDNSViewsList(actionType, orgId, draw, start, length, filterRules, q, callback)</td>
    <td style="padding:15px">DNS Views List</td>
    <td style="padding:15px">{base_path}/{version}/reports/auditviewslist?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getServerIpsList(callback)</td>
    <td style="padding:15px">DNS Cache Appliance List</td>
    <td style="padding:15px">{base_path}/{version}/reports/dnscacheserverslist?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getClientIPsList(callback)</td>
    <td style="padding:15px">Object Type List</td>
    <td style="padding:15px">{base_path}/{version}/reports/clientserverslist?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getLast100EventsList(reportType, fromDate, toDate, ipAddressDomainName, page, rows, q, filterRules, callback)</td>
    <td style="padding:15px">Last 100 Event List</td>
    <td style="padding:15px">{base_path}/{version}/reports/last100eventslist?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNTPOffsetList(reportType, serverType, callback)</td>
    <td style="padding:15px">NTP Offset List</td>
    <td style="padding:15px">{base_path}/{version}/reports/ntpoffsetlist?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">page(draw, start, length, filterRules, q, sort, order, success, loginName, sessionId, remoteIp, target, targetInstance, startDate, endDate, description, ipamIpAddress, callback)</td>
    <td style="padding:15px">Audit Log List</td>
    <td style="padding:15px">{base_path}/{version}/audit/page?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">searchAuditHistoryData(draw, start, length, filterRules, sort, order, callback)</td>
    <td style="padding:15px">Searches Change Tickets</td>
    <td style="padding:15px">{base_path}/{version}/audit/search?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">configUpdate(body, callback)</td>
    <td style="padding:15px">Update Audit Logging Configuration Information.</td>
    <td style="padding:15px">{base_path}/{version}/audit/configUpdate?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAuditConfig(callback)</td>
    <td style="padding:15px">Audit Logging Configuration</td>
    <td style="padding:15px">{base_path}/{version}/audit/auditLoggingConfigDetails?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAuditLicense(callback)</td>
    <td style="padding:15px">License Details</td>
    <td style="padding:15px">{base_path}/{version}/audit/license?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">update(body, callback)</td>
    <td style="padding:15px">Update License</td>
    <td style="padding:15px">{base_path}/{version}/audit/license/update?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postAuthconfEdit(body, callback)</td>
    <td style="padding:15px">Edit Authentication</td>
    <td style="padding:15px">{base_path}/{version}/authconf/edit?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAuthconfGet(callback)</td>
    <td style="padding:15px">Get Authentication Configuration</td>
    <td style="padding:15px">{base_path}/{version}/authconf/get?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deactivate(body, callback)</td>
    <td style="padding:15px">Session Token Deactivate</td>
    <td style="padding:15px">{base_path}/{version}/auth_token/deactivate?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postAuthTokenGet(body, callback)</td>
    <td style="padding:15px">Get Auth Token</td>
    <td style="padding:15px">{base_path}/{version}/auth_token/get?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAuthTokenList(callback)</td>
    <td style="padding:15px">Session Token List</td>
    <td style="padding:15px">{base_path}/{version}/auth_token/list?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">setActive(code, callback)</td>
    <td style="padding:15px">Authentication Set Active</td>
    <td style="padding:15px">{base_path}/{version}/authtype/setactive?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getActive(callback)</td>
    <td style="padding:15px">Active Authentication Configuration</td>
    <td style="padding:15px">{base_path}/{version}/authtype/getactive?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAuthtypeList(callback)</td>
    <td style="padding:15px">Authentication Configuration List</td>
    <td style="padding:15px">{base_path}/{version}/authtype/list?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">view(targetType, targetId, orgId, orgName, targetName, callback)</td>
    <td style="padding:15px">Permissions View</td>
    <td style="padding:15px">{base_path}/{version}/permission/view?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createMultiple(orgId, orgName, body, callback)</td>
    <td style="padding:15px">Add permissions</td>
    <td style="padding:15px">{base_path}/{version}/permission/addmultiple?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPermissionList(targetType, targetId, orgId, orgName, targetName, callback)</td>
    <td style="padding:15px">Permissions List</td>
    <td style="padding:15px">{base_path}/{version}/permission/list?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postPermissionAdd(orgId, orgName, body, callback)</td>
    <td style="padding:15px">Add permissions to an entity</td>
    <td style="padding:15px">{base_path}/{version}/permission/add?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postBookmarkDelete(id, type, queryString, callback)</td>
    <td style="padding:15px">Delete bookmark</td>
    <td style="padding:15px">{base_path}/{version}/bookmark/delete?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postBookmarkDeleteSection(type, callback)</td>
    <td style="padding:15px">Delete bookmark</td>
    <td style="padding:15px">{base_path}/{version}/bookmark/deleteSection?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">delAll(callback)</td>
    <td style="padding:15px">Delete bookmark</td>
    <td style="padding:15px">{base_path}/{version}/bookmark/deleteAll?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getBookmarkList(user, callback)</td>
    <td style="padding:15px">Get Bookmark List</td>
    <td style="padding:15px">{base_path}/{version}/bookmark/list?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postBookmarkAdd(body, callback)</td>
    <td style="padding:15px">Add bookmark</td>
    <td style="padding:15px">{base_path}/{version}/bookmark/add?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getClogConfig(callback)</td>
    <td style="padding:15px">Central Logging Configuration</td>
    <td style="padding:15px">{base_path}/{version}/centralLogging/centralLoggingConfig?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getClogFullConfig(callback)</td>
    <td style="padding:15px">Central Logging Configuration</td>
    <td style="padding:15px">{base_path}/{version}/centralLogging/centralLoggingFullConfig?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getClogServerLogConfig(serverType, callback)</td>
    <td style="padding:15px">Central Logging Server Logs</td>
    <td style="padding:15px">{base_path}/{version}/centralLogging/centralLoggingServerLogConfig?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">fullConfigupdate(body, callback)</td>
    <td style="padding:15px">Update Central Logging Configuration Information.</td>
    <td style="padding:15px">{base_path}/{version}/centralLogging/fullConfigUpdate?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">changeServerClogSetting(serverType, address, clogOption, callback)</td>
    <td style="padding:15px">Server Central Logging Setting</td>
    <td style="padding:15px">{base_path}/{version}/centralLogging/serverCentralLoggingSetting?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllCerts(callback)</td>
    <td style="padding:15px">certificate list</td>
    <td style="padding:15px">{base_path}/{version}/certmgmt/certlist?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNumberOfDays(callback)</td>
    <td style="padding:15px">Number of days Certificate is valid for</td>
    <td style="padding:15px">{base_path}/{version}/certmgmt/certValidDays?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">duplicateAliasCheck(body, callback)</td>
    <td style="padding:15px">Duplicate Alias check</td>
    <td style="padding:15px">{base_path}/{version}/certmgmt/duplicateAlias?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">certificateValidate(cfile, callback)</td>
    <td style="padding:15px">Certificate validation</td>
    <td style="padding:15px">{base_path}/{version}/certmgmt/certValidate?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">allcertValidDays(callback)</td>
    <td style="padding:15px">Get number of days all certificates are valid</td>
    <td style="padding:15px">{base_path}/{version}/certmgmt/allcertValidDays?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">certificateImport(body, callback)</td>
    <td style="padding:15px">Import Certificate</td>
    <td style="padding:15px">{base_path}/{version}/certmgmt/certImport?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">certificateDelete(body, callback)</td>
    <td style="padding:15px">Certificate Delete</td>
    <td style="padding:15px">{base_path}/{version}/certmgmt/certDelete?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">changeKeystorePassword(body, callback)</td>
    <td style="padding:15px">Change Keystore Password</td>
    <td style="padding:15px">{base_path}/{version}/certmgmt/changeKeystorePassword?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">jettyRestart(callback)</td>
    <td style="padding:15px">Web Server Restart</td>
    <td style="padding:15px">{base_path}/{version}/certmgmt/jettyRestart?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTop10Subnets(id, address, callback)</td>
    <td style="padding:15px">Network Statistics</td>
    <td style="padding:15px">{base_path}/{version}/chart/top10subnets?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTop10v6Subnets(id, orgName, address, callback)</td>
    <td style="padding:15px">Network Statistics</td>
    <td style="padding:15px">{base_path}/{version}/chart/top10v6subnets?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllocPercent(id, address, callback)</td>
    <td style="padding:15px">Network Statistics</td>
    <td style="padding:15px">{base_path}/{version}/chart/allocationpercent?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getv6AllocPercent(id, orgName, address, callback)</td>
    <td style="padding:15px">Network Statistics</td>
    <td style="padding:15px">{base_path}/{version}/chart/v6allocationpercent?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getObjectAllocChart(id, callback)</td>
    <td style="padding:15px">Object Allocation</td>
    <td style="padding:15px">{base_path}/{version}/chart/objectAlloc?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getObjectTypeChart(id, callback)</td>
    <td style="padding:15px">Object Type</td>
    <td style="padding:15px">{base_path}/{version}/chart/objectType?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getv6ObjectAllocChart(id, callback)</td>
    <td style="padding:15px">Object Allocation</td>
    <td style="padding:15px">{base_path}/{version}/chart/v6objectAlloc?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getv6ObjectTypeChart(id, callback)</td>
    <td style="padding:15px">Object Type</td>
    <td style="padding:15px">{base_path}/{version}/chart/v6objectType?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listDNS(serverType, serverIp, serverId, callback)</td>
    <td style="padding:15px">IPAM/DNS/DHCP appliance's checkouts information</td>
    <td style="padding:15px">{base_path}/{version}/checkouts/list?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">runAndGetCheckouts(serverType, serverIp, serverName, callback)</td>
    <td style="padding:15px">Performs checkouts operation and retrieves the information</td>
    <td style="padding:15px">{base_path}/{version}/checkouts/runAndList?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">setBaseLine(serverType, serverIp, body, callback)</td>
    <td style="padding:15px">Set baseline values for the specified checkouts</td>
    <td style="padding:15px">{base_path}/{version}/checkouts/baseline?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getIPAMList(callback)</td>
    <td style="padding:15px">List IPAM Appliances</td>
    <td style="padding:15px">{base_path}/{version}/checkouts/ipamlist?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getClassCodeLogoList(callback)</td>
    <td style="padding:15px">Class Code Logo List</td>
    <td style="padding:15px">{base_path}/{version}/classCodeLogo/list?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getClassCodeDetails(classcodename, callback)</td>
    <td style="padding:15px">Class Code Details</td>
    <td style="padding:15px">{base_path}/{version}/classcode/getClassCodeDetails?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">autoName(classCode, callback)</td>
    <td style="padding:15px">Object Name Auto generation</td>
    <td style="padding:15px">{base_path}/{version}/classcode/nextautohostname?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postClasscodeUpdate(body, callback)</td>
    <td style="padding:15px">Object Type Update</td>
    <td style="padding:15px">{base_path}/{version}/classcode/update?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getClasscodeList(dHCP, callback)</td>
    <td style="padding:15px">Object Type List</td>
    <td style="padding:15px">{base_path}/{version}/classcode/list?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">delete(body, callback)</td>
    <td style="padding:15px">Object Type Delete</td>
    <td style="padding:15px">{base_path}/{version}/classcode/delete?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postClasscodeAdd(body, callback)</td>
    <td style="padding:15px">Object Type Add</td>
    <td style="padding:15px">{base_path}/{version}/classcode/add?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createImage(cloudProviderId, cloudProviderName, id, name, desc, orgId, orgName, callback)</td>
    <td style="padding:15px">Create AWS Image</td>
    <td style="padding:15px">{base_path}/{version}/cloudCompute/createAWSImage?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createInstance(body, callback)</td>
    <td style="padding:15px">Cloud instance create</td>
    <td style="padding:15px">{base_path}/{version}/cloudCompute/instance/create?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listGoogleZones(orgName, providerName, callback)</td>
    <td style="padding:15px">Cloud Provider List</td>
    <td style="padding:15px">{base_path}/{version}/cloudCompute/listGoogleZones?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listGoogleMachines(orgName, providerName, googleZone, callback)</td>
    <td style="padding:15px">Cloud Provider List</td>
    <td style="padding:15px">{base_path}/{version}/cloudCompute/listGoogleMachines?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listGoogleOSImages(orgName, providerName, imageType, callback)</td>
    <td style="padding:15px">Cloud Provider List</td>
    <td style="padding:15px">{base_path}/{version}/cloudCompute/listGoogleOsImages?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createInstanceTemplate(body, callback)</td>
    <td style="padding:15px">Cloud instance template add</td>
    <td style="padding:15px">{base_path}/{version}/cloudCompute/instanceTemplate/add?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">editInstanceTemplate(body, callback)</td>
    <td style="padding:15px">Cloud instance template update</td>
    <td style="padding:15px">{base_path}/{version}/cloudCompute/instanceTemplate/edit?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listInstanceTemplates(providerType, cloudProviderId, callback)</td>
    <td style="padding:15px">Cloud instance template list</td>
    <td style="padding:15px">{base_path}/{version}/cloudCompute/instanceTemplate/list?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteInstanceTemplates(body, callback)</td>
    <td style="padding:15px">Cloud instance template delete</td>
    <td style="padding:15px">{base_path}/{version}/cloudCompute/instanceTemplate/delete?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getInstanceTemplate(templateId, templateName, orgId, orgName, callback)</td>
    <td style="padding:15px">Cloud instance template get</td>
    <td style="padding:15px">{base_path}/{version}/cloudCompute/instanceTemplate/get?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">changeState(objectID, objectAddress, orgName, action, callback)</td>
    <td style="padding:15px">Change cloud instance state</td>
    <td style="padding:15px">{base_path}/{version}/cloudCompute/instance/changeState?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listAzureRegions(orgName, providerName, callback)</td>
    <td style="padding:15px">Azure Region List</td>
    <td style="padding:15px">{base_path}/{version}/cloudCompute/listAzureRegions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listAzureImagePublishers(orgName, providerName, azureRegion, callback)</td>
    <td style="padding:15px">Azure Virtual Image Publisher List</td>
    <td style="padding:15px">{base_path}/{version}/cloudCompute/listAzureImagePublishers?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listAzureImageOffers(orgName, providerName, azureRegion, publisher, callback)</td>
    <td style="padding:15px">Azure Virtual Image Publisher's Offer List</td>
    <td style="padding:15px">{base_path}/{version}/cloudCompute/listAzureImageOffers?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listAzureImageSKUs(orgName, providerName, azureRegion, publisher, offer, callback)</td>
    <td style="padding:15px">Azure Virtual Image Publisher Offer's SKU List</td>
    <td style="padding:15px">{base_path}/{version}/cloudCompute/listAzureImageSKUs?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listAzureImageVersions(orgName, providerName, azureRegion, publisher, offer, sKU, callback)</td>
    <td style="padding:15px">Azure Virtual Image Version List</td>
    <td style="padding:15px">{base_path}/{version}/cloudCompute/listAzureImageVersions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listAzureVMSizes(orgName, providerName, azureRegion, callback)</td>
    <td style="padding:15px">Azure Virtual Machines Size List</td>
    <td style="padding:15px">{base_path}/{version}/cloudCompute/listAzureVMSizes?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCloudProviders(orgId, orgName, providerType, callback)</td>
    <td style="padding:15px">List Cloud Providers</td>
    <td style="padding:15px">{base_path}/{version}/cloudCompute/listCloudProviders?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getVolumeTypes(callback)</td>
    <td style="padding:15px">List Volume Types</td>
    <td style="padding:15px">{base_path}/{version}/cloudCompute/listAWSVolumeTypes?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getImages(orgId, orgName, cloudProviderId, cloudProviderName, callback)</td>
    <td style="padding:15px">List Amazon Machine Images</td>
    <td style="padding:15px">{base_path}/{version}/cloudCompute/listImages?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getInstanceFamilies(callback)</td>
    <td style="padding:15px">List AWS Instance Families</td>
    <td style="padding:15px">{base_path}/{version}/cloudCompute/listInstanceFamilies?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getInstanceTypes(instanceFamily, callback)</td>
    <td style="padding:15px">List Instance Types</td>
    <td style="padding:15px">{base_path}/{version}/cloudCompute/listInstanceTypes?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getIAMRoles(cloudProviderId, cloudProvider, orgId, orgName, callback)</td>
    <td style="padding:15px">Get IAM Roles</td>
    <td style="padding:15px">{base_path}/{version}/cloudCompute/getIAMRoles?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSecurityGroups(cloudProviderId, cloudProvider, vpcIds, orgId, orgName, callback)</td>
    <td style="padding:15px">Get Security Groups</td>
    <td style="padding:15px">{base_path}/{version}/cloudCompute/getSecurityGroups?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">isReservedIP(ip, subnetAddress, cloudProviderId, cloudProvider, orgId, orgName, callback)</td>
    <td style="padding:15px">Get Cloud Instance Template</td>
    <td style="padding:15px">{base_path}/{version}/cloudCompute/instance/isReservedIP?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getInstanceCreationError(objectId, callback)</td>
    <td style="padding:15px">Get Cloud Instance creation error</td>
    <td style="padding:15px">{base_path}/{version}/cloudCompute/instance/getCreationError?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getInstanceTemplateId(objectId, objectAddress, orgName, callback)</td>
    <td style="padding:15px">Get Cloud Instance Template Id</td>
    <td style="padding:15px">{base_path}/{version}/cloudCompute/instance/getTemplateId?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">recreateInstance(objectAddress, orgName, callback)</td>
    <td style="padding:15px">Cloud instance recreate</td>
    <td style="padding:15px">{base_path}/{version}/cloudCompute/instance/recreate?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">checkCloudInstances(ipList, orgName, callback)</td>
    <td style="padding:15px">Check for Cloud Instances</td>
    <td style="padding:15px">{base_path}/{version}/cloudCompute/instance/checkInstances?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">terminateInstances(cloudProviderId, objectList, ipList, orgName, subnetAddress, callback)</td>
    <td style="padding:15px">Terminate Cloud Instances</td>
    <td style="padding:15px">{base_path}/{version}/cloudCompute/instance/terminate?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getImagesPage(draw, start, length, filterRules, sort, order, callback)</td>
    <td style="padding:15px">List AWS Images</td>
    <td style="padding:15px">{base_path}/{version}/cloudCompute/awsImage/page?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteImage(id, cloudProviderId, cloudProvider, orgName, callback)</td>
    <td style="padding:15px">Delete AWS Image</td>
    <td style="padding:15px">{base_path}/{version}/cloudCompute/deleteAWSImage?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getInstance(objectId, objectAddress, orgName, callback)</td>
    <td style="padding:15px">Change cloud instance state</td>
    <td style="padding:15px">{base_path}/{version}/cloudCompute/instance/get?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getState(objectID, objectAddress, orgName, callback)</td>
    <td style="padding:15px">Cloud instance create</td>
    <td style="padding:15px">{base_path}/{version}/cloudCompute/instance/getState?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postClouddnsproviderEdit(oldname, body, callback)</td>
    <td style="padding:15px">Edit Cloud Provider</td>
    <td style="padding:15px">{base_path}/{version}/clouddnsprovider/edit?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateTemplate(type, name, orgId, orgName, body, callback)</td>
    <td style="padding:15px">Cloud Provider Templates Update</td>
    <td style="padding:15px">{base_path}/{version}/clouddnsprovider/update_template?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">removePermission(cpname, cpid, cptype, orgId, orgName, body, callback)</td>
    <td style="padding:15px">Dis-associate Cloud Provider</td>
    <td style="padding:15px">{base_path}/{version}/clouddnsprovider/disassociate?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listProviderTemplates(provider, template, orgName, orgId, callback)</td>
    <td style="padding:15px">Cloud Provider Templates</td>
    <td style="padding:15px">{base_path}/{version}/clouddnsprovider/list_provider_templates?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listProviderTypes(callback)</td>
    <td style="padding:15px">Cloud Provider Types</td>
    <td style="padding:15px">{base_path}/{version}/clouddnsprovider/list_provider_types?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">cloudPPage(draw, start, length, filterRules, sort, order, callback)</td>
    <td style="padding:15px">Cloud provider Search</td>
    <td style="padding:15px">{base_path}/{version}/clouddnsprovider/search?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">searchCloudPReferences(page, rows, tables, id, count, callback)</td>
    <td style="padding:15px">Lists cloud provider references</td>
    <td style="padding:15px">{base_path}/{version}/clouddnsprovider/references?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCloudProvider(providerID, providerName, orgID, orgName, callback)</td>
    <td style="padding:15px">Cloud Provider Details</td>
    <td style="padding:15px">{base_path}/{version}/clouddnsprovider/get?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">add(body, callback)</td>
    <td style="padding:15px">Add New Cloud Provider</td>
    <td style="padding:15px">{base_path}/{version}/clouddnsprovider/add?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getClouddnsproviderList(orgId, orgName, callback)</td>
    <td style="padding:15px">Cloud Provider List</td>
    <td style="padding:15px">{base_path}/{version}/clouddnsprovider/list?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postClouddnsproviderDelete(name, orgName, callback)</td>
    <td style="padding:15px">Delete Cloud Provider</td>
    <td style="padding:15px">{base_path}/{version}/clouddnsprovider/delete?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listByPage(organizationName, draw, start, length, filterRules, q, sort, order, inEdit, callback)</td>
    <td style="padding:15px">Contact List</td>
    <td style="padding:15px">{base_path}/{version}/contact/paged?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">roleslist(roles, callback)</td>
    <td style="padding:15px">Contact List for selected User Roles</td>
    <td style="padding:15px">{base_path}/{version}/contact/roleslist?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">auditroleslist(roles, callback)</td>
    <td style="padding:15px">Contact List based on Roles</td>
    <td style="padding:15px">{base_path}/{version}/contact/auditroleslist?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getdetails(id, callback)</td>
    <td style="padding:15px">Get Contact Information</td>
    <td style="padding:15px">{base_path}/{version}/contact/getdetails?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">contactPage(page, rows, callback)</td>
    <td style="padding:15px">Search Contacts</td>
    <td style="padding:15px">{base_path}/{version}/contact/search?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postContactGet(body, callback)</td>
    <td style="padding:15px">Get Contact Information</td>
    <td style="padding:15px">{base_path}/{version}/contact/get?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postContactUpdate(firstName, middleName, lastName, email, body, callback)</td>
    <td style="padding:15px">Contact Update</td>
    <td style="padding:15px">{base_path}/{version}/contact/update?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getContactList(callback)</td>
    <td style="padding:15px">Contact List</td>
    <td style="padding:15px">{base_path}/{version}/contact/list?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postContactDelete(body, callback)</td>
    <td style="padding:15px">Contact Delete</td>
    <td style="padding:15px">{base_path}/{version}/contact/delete?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postContactAdd(body, callback)</td>
    <td style="padding:15px">Contact Add</td>
    <td style="padding:15px">{base_path}/{version}/contact/add?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getServices(service, callback)</td>
    <td style="padding:15px">Service information</td>
    <td style="padding:15px">{base_path}/{version}/contingency/service/get?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listServiceMode(callback)</td>
    <td style="padding:15px">Service Mode information</td>
    <td style="padding:15px">{base_path}/{version}/contingency/serviceMode/list?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createServiceMode(body, callback)</td>
    <td style="padding:15px">Create Service Mode</td>
    <td style="padding:15px">{base_path}/{version}/contingency/serviceMode/add?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getServiceMode(serviceMode, callback)</td>
    <td style="padding:15px">Get Service Mode Details</td>
    <td style="padding:15px">{base_path}/{version}/contingency/serviceMode/get?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteServiceMode(body, callback)</td>
    <td style="padding:15px">Delete Service Mode</td>
    <td style="padding:15px">{base_path}/{version}/contingency/serviceMode/delete?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateServiceMode(body, callback)</td>
    <td style="padding:15px">Updated Service Mode</td>
    <td style="padding:15px">{base_path}/{version}/contingency/serviceMode/edit?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listServices(callback)</td>
    <td style="padding:15px">List Services</td>
    <td style="padding:15px">{base_path}/{version}/contingency/service/list?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createService(body, callback)</td>
    <td style="padding:15px">Create Service</td>
    <td style="padding:15px">{base_path}/{version}/contingency/service/add?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteService(body, callback)</td>
    <td style="padding:15px">Delete Service</td>
    <td style="padding:15px">{base_path}/{version}/contingency/service/delete?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateService(body, callback)</td>
    <td style="padding:15px">Updated Service</td>
    <td style="padding:15px">{base_path}/{version}/contingency/service/edit?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteSchedule(body, callback)</td>
    <td style="padding:15px">Deletes ServiceSchedule</td>
    <td style="padding:15px">{base_path}/{version}/contingency/service/schedule/delete?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">executeNow(body, callback)</td>
    <td style="padding:15px">Execute Scheduled Service</td>
    <td style="padding:15px">{base_path}/{version}/contingency/service/schedule/execute?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSchedulesForService(service, callback)</td>
    <td style="padding:15px">Service information</td>
    <td style="padding:15px">{base_path}/{version}/contingency/service/schedule/list?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">switchService(newMode, body, callback)</td>
    <td style="padding:15px">Switch Service Mode</td>
    <td style="padding:15px">{base_path}/{version}/contingency/service/switch?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">switchMultipleService(newMode, body, callback)</td>
    <td style="padding:15px">Switch Multiple Services Mode</td>
    <td style="padding:15px">{base_path}/{version}/contingency/service/switchMultiple?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">prepareSnapshot(callback)</td>
    <td style="padding:15px">Dump Database</td>
    <td style="padding:15px">{base_path}/{version}/db/dumpdb?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">applySnapshot(body, callback)</td>
    <td style="padding:15px">Apply Snapshot on Database</td>
    <td style="padding:15px">{base_path}/{version}/db/apply-snapshot?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">backupBinlog(callback)</td>
    <td style="padding:15px">Backup Binlog for Database</td>
    <td style="padding:15px">{base_path}/{version}/db/backup-binlog?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">applyBinlog(body, callback)</td>
    <td style="padding:15px">Apply Binlog for Database</td>
    <td style="padding:15px">{base_path}/{version}/db/apply-binlog?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">restoreDB(body, callback)</td>
    <td style="padding:15px">Restores Database</td>
    <td style="padding:15px">{base_path}/{version}/db/restoredb?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDBSnapshot(body, callback)</td>
    <td style="padding:15px">Delete Database Snapshot</td>
    <td style="padding:15px">{base_path}/{version}/db/deletedbsnapshot?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listAwsS3File(callback)</td>
    <td style="padding:15px">Lists s3 backup files</td>
    <td style="padding:15px">{base_path}/{version}/db/lists3files?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteAWSS3Files(body, callback)</td>
    <td style="padding:15px">Delete Backup Files</td>
    <td style="padding:15px">{base_path}/{version}/db/multideletes3files?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">downloadAWSS3File(body, callback)</td>
    <td style="padding:15px">Downloads AWS S3 File</td>
    <td style="padding:15px">{base_path}/{version}/db/downloadawss3file?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">restoreAWSS3File(body, callback)</td>
    <td style="padding:15px">Restore Database from an AWS S3 backup file</td>
    <td style="padding:15px">{base_path}/{version}/db/restoredbwithawss3?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">backupDbToAWSS3(callback)</td>
    <td style="padding:15px">Uploads Backup Database to AWS S3</td>
    <td style="padding:15px">{base_path}/{version}/db/backupdbtoaws?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getActiveLeases(serverAddress, callback)</td>
    <td style="padding:15px">Get Active Leases</td>
    <td style="padding:15px">{base_path}/{version}/dhcpActiveLeases/getActiveLeases?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createDHCPClass(body, callback)</td>
    <td style="padding:15px">Create DHCP Class</td>
    <td style="padding:15px">{base_path}/{version}/dhcpclass/add?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateDHCPClass(body, callback)</td>
    <td style="padding:15px">DHCP Class Edit</td>
    <td style="padding:15px">{base_path}/{version}/dhcpclass/edit?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDHCPClassSearchList(draw, start, length, filterRules, sort, order, callback)</td>
    <td style="padding:15px">DHCP Class Search</td>
    <td style="padding:15px">{base_path}/{version}/dhcpclass/search?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDHCPClassReferences(page, rows, tables, id, count, callback)</td>
    <td style="padding:15px">Find DHCP Class References</td>
    <td style="padding:15px">{base_path}/{version}/dhcpclass/references?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getClass(entityType, name, callback)</td>
    <td style="padding:15px">DHCP Class List</td>
    <td style="padding:15px">{base_path}/{version}/dhcpclass/get?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDhcpclassList(entityType, callback)</td>
    <td style="padding:15px">DHCP Class List</td>
    <td style="padding:15px">{base_path}/{version}/dhcpclass/list?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postDhcpclassDelete(id, name, entityType, callback)</td>
    <td style="padding:15px">DHCP Classes Delete</td>
    <td style="padding:15px">{base_path}/{version}/dhcpclass/delete?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">editdhcpOSF(body, callback)</td>
    <td style="padding:15px">DHCP option space update</td>
    <td style="padding:15px">{base_path}/{version}/dhcpOptionSpace/update?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDHCPServer(optionSpaceName, callback)</td>
    <td style="padding:15px">DHCP option space delete</td>
    <td style="padding:15px">{base_path}/{version}/dhcpOptionSpace/delete?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listSubOptionParams(optionSpaceName, callback)</td>
    <td style="padding:15px">User Defined sub options list</td>
    <td style="padding:15px">{base_path}/{version}/dhcpOptionSpace/list-sub-options?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listundefinedSubOptionParams(optionSpaceName, callback)</td>
    <td style="padding:15px">User Defined DHCP options list</td>
    <td style="padding:15px">{base_path}/{version}/dhcpOptionSpace/list-undefined-sub-options?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDhcpOptionSpaceList(callback)</td>
    <td style="padding:15px">Option Space List</td>
    <td style="padding:15px">{base_path}/{version}/dhcpOptionSpace/list?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postDhcpOptionSpaceCreate(body, callback)</td>
    <td style="padding:15px">DHCP option space add</td>
    <td style="padding:15px">{base_path}/{version}/dhcpOptionSpace/create?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postDhcpserverDelete(id, address, callback)</td>
    <td style="padding:15px">DHCP Appliance Delete</td>
    <td style="padding:15px">{base_path}/{version}/dhcpserver/delete?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">gen(address, organizationName, servername, callback)</td>
    <td style="padding:15px">Generate Server Configuration for sync</td>
    <td style="padding:15px">{base_path}/{version}/dhcpserver/generateConfig?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postDhcpserverEdit(body, callback)</td>
    <td style="padding:15px">DHCP Appliance Update</td>
    <td style="padding:15px">{base_path}/{version}/dhcpserver/edit?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfiguration(serverAddress, callback)</td>
    <td style="padding:15px">Server Configuration</td>
    <td style="padding:15px">{base_path}/{version}/dhcpserver/server-configuration/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">primaryList(inEdit, orgName, serverType, callback)</td>
    <td style="padding:15px">Primary DHCP Appliance List</td>
    <td style="padding:15px">{base_path}/{version}/dhcpserver/primary-servers-list?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDHCPLeaseReportForGrid(serverIp, callback)</td>
    <td style="padding:15px">DHCP Appliances Active Leases</td>
    <td style="padding:15px">{base_path}/{version}/dhcpserver/dhcpActiveLeases?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">searchDHCPServerReferences(page, rows, tables, id, count, callback)</td>
    <td style="padding:15px">DHCP Appliance References</td>
    <td style="padding:15px">{base_path}/{version}/dhcpserver/references?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listSvc(callback)</td>
    <td style="padding:15px">DHCP Appliance List</td>
    <td style="padding:15px">{base_path}/{version}/dhcpserver/list?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getFailoverServerConfiguration(serverAddress, callback)</td>
    <td style="padding:15px">Failover Server Configuration</td>
    <td style="padding:15px">{base_path}/{version}/dhcpserver/failover-server-configuration/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">synAll(body, organizationName, callback)</td>
    <td style="padding:15px">Generate Server Configuration for sync of all servers</td>
    <td style="padding:15px">{base_path}/{version}/dhcpserver/syncall?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">associatedSubnetList(address, callback)</td>
    <td style="padding:15px">Server Subnet References</td>
    <td style="padding:15px">{base_path}/{version}/dhcpserver/server-subnet-references?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">runDebugger(hardware, duration, server, callback)</td>
    <td style="padding:15px">DHCP Appliance Debugger</td>
    <td style="padding:15px">{base_path}/{version}/dhcpserver/debugger?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">serverIPCount(organizationName, callback)</td>
    <td style="padding:15px">DHCP Appliance IP Count</td>
    <td style="padding:15px">{base_path}/{version}/dhcpserver/serverIPCount?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">viewConfig(address, organizationName, callback)</td>
    <td style="padding:15px">View Configuration</td>
    <td style="padding:15px">{base_path}/{version}/dhcpserver/viewconfig?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSubSysResultForServer(serverName, serverIp, level, argc, callback)</td>
    <td style="padding:15px">Execute Command on DHCP Appliance</td>
    <td style="padding:15px">{base_path}/{version}/dhcpserver/subsys?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDNSServerHeartbeat(serverIp, serverType, callback)</td>
    <td style="padding:15px">DHCP Appliance Heartbeat</td>
    <td style="padding:15px">{base_path}/{version}/dhcpserver/heartbeat?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postDhcpserverAdd(body, callback)</td>
    <td style="padding:15px">DHCP Appliance Create</td>
    <td style="padding:15px">{base_path}/{version}/dhcpserver/add?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRules(callback)</td>
    <td style="padding:15px">Get DNS Analytics Rules</td>
    <td style="padding:15px">{base_path}/{version}/dnsanalytics/getrules?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAnomalousDomains(callback)</td>
    <td style="padding:15px">Get Domains Reputation Data</td>
    <td style="padding:15px">{base_path}/{version}/dnsanalytics/getanomalousdomains?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">blockDomains(body, callback)</td>
    <td style="padding:15px">Block Domains</td>
    <td style="padding:15px">{base_path}/{version}/dnsanalytics/blockdomains?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">whitelistDomains(body, callback)</td>
    <td style="padding:15px">Whitelist Domains</td>
    <td style="padding:15px">{base_path}/{version}/dnsanalytics/whitelistdomains?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDomains(body, callback)</td>
    <td style="padding:15px">Delete anomalous Domains</td>
    <td style="padding:15px">{base_path}/{version}/dnsanalytics/deletedomains?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateRules(body, callback)</td>
    <td style="padding:15px">Update DNS Analytics Rules</td>
    <td style="padding:15px">{base_path}/{version}/dnsanalytics/updaterules?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">importDomains(body, callback)</td>
    <td style="padding:15px">Import Domains Reputation Data</td>
    <td style="padding:15px">{base_path}/{version}/dnsanalytics/importdomains?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">retrainTunnelDetectionModel(body, callback)</td>
    <td style="padding:15px">Retrain DNS Tunnel Detection ML Model</td>
    <td style="padding:15px">{base_path}/{version}/dnsanalytics/retraintunneldetectionmodel?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteForwarders(categoryId, categoryName, callback)</td>
    <td style="padding:15px">Delete Forwarders Category</td>
    <td style="padding:15px">{base_path}/{version}/forwarders/dnsforwarders/delete?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createDNSForwarders(body, callback)</td>
    <td style="padding:15px">Add Forwarders</td>
    <td style="padding:15px">{base_path}/{version}/forwarders/dnsforwarders/add?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateDNSForwarders(body, categoryId, callback)</td>
    <td style="padding:15px">Add Forwarders</td>
    <td style="padding:15px">{base_path}/{version}/forwarders/dnsforwarders/update?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listDNSForwarders(categoryId, callback)</td>
    <td style="padding:15px">Forwarders List</td>
    <td style="padding:15px">{base_path}/{version}/forwarders/dnsforwarders/list?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listDNSForwardersCategory(callback)</td>
    <td style="padding:15px">Forwarders List</td>
    <td style="padding:15px">{base_path}/{version}/forwarders/dnsforwarders/majorlist?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listDNSServersAssociateForForwarders(categoryId, callback)</td>
    <td style="padding:15px">Forwarders List</td>
    <td style="padding:15px">{base_path}/{version}/forwarders/dnsforwarders/associateForwarders?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">dnsForwardersSearch(draw, start, length, filterRules, sort, order, callback)</td>
    <td style="padding:15px">DNS Forwarders Search</td>
    <td style="padding:15px">{base_path}/{version}/forwarders/dnsforwarders/search?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">searchDNSForwarderReferences(page, rows, tables, id, count, callback)</td>
    <td style="padding:15px">Find DNS forwarder References</td>
    <td style="padding:15px">{base_path}/{version}/forwarders/dnsforwarders/references?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postDnsserverDelete(id, address, callback)</td>
    <td style="padding:15px">DNS Appliance Delete</td>
    <td style="padding:15px">{base_path}/{version}/dnsserver/delete?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getServer(serverTypeCode, serverIp, callback)</td>
    <td style="padding:15px">DNS Appliance Details</td>
    <td style="padding:15px">{base_path}/{version}/dnsserver/get?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postDnsserverEdit(body, callback)</td>
    <td style="padding:15px">DNS Appliance Edit</td>
    <td style="padding:15px">{base_path}/{version}/dnsserver/edit?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDnsserverServerConfigurationServerAddressServerType(serverAddress, serverType, callback)</td>
    <td style="padding:15px">Appliance Configuration</td>
    <td style="padding:15px">{base_path}/{version}/dnsserver/server-configuration/{pathv1}/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">serverPage(page, rows, callback)</td>
    <td style="padding:15px">Appliance Search</td>
    <td style="padding:15px">{base_path}/{version}/dnsserver/search?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">zoneList(id, name, revZone, callback)</td>
    <td style="padding:15px">DNS Appliance Zone List</td>
    <td style="padding:15px">{base_path}/{version}/dnsserver/zonelist?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">searchDNSServerReferences(page, rows, tables, id, count, callback)</td>
    <td style="padding:15px">Appliance References In Zones</td>
    <td style="padding:15px">{base_path}/{version}/dnsserver/references?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">cacheApplianceList(orgName, callback)</td>
    <td style="padding:15px">DNS Cache Appliance List</td>
    <td style="padding:15px">{base_path}/{version}/dnsserver/internal_cache_list?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">serverPagelist(draw, start, length, q, filterRules, sort, order, callback)</td>
    <td style="padding:15px">DNS Appliance List</td>
    <td style="padding:15px">{base_path}/{version}/dnsserver/page?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createSvc(body, callback)</td>
    <td style="padding:15px">Add DNS Appliance</td>
    <td style="padding:15px">{base_path}/{version}/dnsserver/add?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">checkIfAssociatedWithProxyRootZone(serverIp, orgName, oldServerType, newServerType, callback)</td>
    <td style="padding:15px">Check if DNS Appliance is associated</td>
    <td style="padding:15px">{base_path}/{version}/dnsserver/check_if_server_is_associated?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">generateConfig(serverTypeCode, address, organizationName, callback)</td>
    <td style="padding:15px">Generate Configuration</td>
    <td style="padding:15px">{base_path}/{version}/dnsserver/genconfig?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDnsserverViewconfig(serverTypeCode, address, organizationName, callback)</td>
    <td style="padding:15px">View Configuration of DNS Appliance</td>
    <td style="padding:15px">{base_path}/{version}/dnsserver/viewconfig?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDigResultForServerSvc(serverName, serverIp, serverType, recordFqdn, recordType, callback)</td>
    <td style="padding:15px">Appliance Dig</td>
    <td style="padding:15px">{base_path}/{version}/dnsserver/digserver?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDnsserverSubsys(serverName, serverIp, level, argc, argv, callback)</td>
    <td style="padding:15px">Appliance Command</td>
    <td style="padding:15px">{base_path}/{version}/dnsserver/subsys?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDnsserverHeartbeat(serverIp, serverType, callback)</td>
    <td style="padding:15px">Appliance Heartbeat Check</td>
    <td style="padding:15px">{base_path}/{version}/dnsserver/heartbeat?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">serverRefList(id, address, callback)</td>
    <td style="padding:15px">Appliance References in Zone Template</td>
    <td style="padding:15px">{base_path}/{version}/dnsserver/server-references?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listForwarders(id, address, callback)</td>
    <td style="padding:15px">Forwarders List</td>
    <td style="padding:15px">{base_path}/{version}/dnsserver/forwarders/list?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createForwarders(body, callback)</td>
    <td style="padding:15px">Add Forwarders</td>
    <td style="padding:15px">{base_path}/{version}/dnsserver/forwarders/add?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateForwarders(body, callback)</td>
    <td style="padding:15px">Update Forwarders</td>
    <td style="padding:15px">{base_path}/{version}/dnsserver/forwarders/update?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postDnsserverForwardersDelete(body, callback)</td>
    <td style="padding:15px">Delete Forwarders</td>
    <td style="padding:15px">{base_path}/{version}/dnsserver/forwarders/delete?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">dnsFlush(body, callback)</td>
    <td style="padding:15px">Flush operation on appliance</td>
    <td style="padding:15px">{base_path}/{version}/dnsserver/dnsflush?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDnstapLogs(ip, name, type, callback)</td>
    <td style="padding:15px">Dnstap Logs</td>
    <td style="padding:15px">{base_path}/{version}/dnsserver/getDnstapLogs?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDnsserverList(callback)</td>
    <td style="padding:15px">DNS Appliance List</td>
    <td style="padding:15px">{base_path}/{version}/dnsserver/list?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">syncAll(body, fasterSync, callback)</td>
    <td style="padding:15px">Synchronize DNS Appliances</td>
    <td style="padding:15px">{base_path}/{version}/dnsserver/syncAll?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postDnsserverTemplateEdit(body, callback)</td>
    <td style="padding:15px">DNS Appliance Template Edit</td>
    <td style="padding:15px">{base_path}/{version}/dnsserver_template/edit?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">searchDomainReferences(page, rows, tables, id, count, callback)</td>
    <td style="padding:15px">Appliance Template References</td>
    <td style="padding:15px">{base_path}/{version}/dnsserver_template/references?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">dnsSrvrTemplatePage(page, rows, callback)</td>
    <td style="padding:15px">Appliance Template Search</td>
    <td style="padding:15px">{base_path}/{version}/dnsserver_template/search?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDnsserverTemplateGet(id, name, callback)</td>
    <td style="padding:15px">DNS Appliance Template Details</td>
    <td style="padding:15px">{base_path}/{version}/dnsserver_template/get?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDnsserverTemplateList(serverTypeId, serverTypeCode, callback)</td>
    <td style="padding:15px">DNS Appliance Template List</td>
    <td style="padding:15px">{base_path}/{version}/dnsserver_template/list?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postDnsserverTemplateAdd(body, callback)</td>
    <td style="padding:15px">DNS Appliance Template Add</td>
    <td style="padding:15px">{base_path}/{version}/dnsserver_template/add?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">traverse(body, callback)</td>
    <td style="padding:15px">Resource Record Traverse</td>
    <td style="padding:15px">{base_path}/{version}/dnstools/traverseRR?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">converge(body, callback)</td>
    <td style="padding:15px">Resource Record Converge</td>
    <td style="padding:15px">{base_path}/{version}/dnstools/converge?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">flushCacheOnServer(serverip, servertype, recordname, recordtype, domainname, callback)</td>
    <td style="padding:15px">Resource Record Sync</td>
    <td style="padding:15px">{base_path}/{version}/dnstools/sync?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">idnConverter(body, callback)</td>
    <td style="padding:15px">IDN Converter</td>
    <td style="padding:15px">{base_path}/{version}/dnstools/idnconverter?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getZoneDNSSECDelegationHTML(zoneName, orgName, cacheApplianceIp, callback)</td>
    <td style="padding:15px">DNSViz as HTML</td>
    <td style="padding:15px">{base_path}/{version}/dnsviz/getDNSVizHTML?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getZoneDNSSECDelegationPNG(zoneName, orgName, cacheApplianceIp, callback)</td>
    <td style="padding:15px">DNSViz as PNG</td>
    <td style="padding:15px">{base_path}/{version}/dnsviz/getDNSVizPNG?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">setAcceptStatus(body, isExternalUser, callback)</td>
    <td style="padding:15px">Set Accept Status of Discovery Command</td>
    <td style="padding:15px">{base_path}/{version}/discovery/setacceptstatus?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">acceptElements(body, isExternalUser, callback)</td>
    <td style="padding:15px">Accept Discovered Elements</td>
    <td style="padding:15px">{base_path}/{version}/discovery/acceptelements?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">setDiscardStatus(body, callback)</td>
    <td style="padding:15px">Set Discard Status of Discovery Command</td>
    <td style="padding:15px">{base_path}/{version}/discovery/setdiscardstatus?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getdiscoveredsubnetelements(cmdId, callback)</td>
    <td style="padding:15px">Discover Subnet Element List</td>
    <td style="padding:15px">{base_path}/{version}/discovery/getdiscoveredsubnetelements?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getsubnetelements(body, callback)</td>
    <td style="padding:15px">Discover Subnet Element List</td>
    <td style="padding:15px">{base_path}/{version}/discovery/getsubnetelements?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getswitchelements(body, callback)</td>
    <td style="padding:15px">Discover Subnet Element List</td>
    <td style="padding:15px">{base_path}/{version}/discovery/getswitchelements?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">discoverSubnet(body, callback)</td>
    <td style="padding:15px">Discover Subnet</td>
    <td style="padding:15px">{base_path}/{version}/discovery/discover_subnet?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">discoverMultipleSubnets(body, callback)</td>
    <td style="padding:15px">Discover Subnets</td>
    <td style="padding:15px">{base_path}/{version}/discovery/discover_multiple_subnets?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">discoverElement(body, callback)</td>
    <td style="padding:15px">Discover IP Information</td>
    <td style="padding:15px">{base_path}/{version}/discovery/discover_element?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">discoverRouterSubnets(body, callback)</td>
    <td style="padding:15px">Discover Subnet of Router</td>
    <td style="padding:15px">{base_path}/{version}/discovery/discover_router_subnets?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">handleObjectDiscoveryUpdate(body, callback)</td>
    <td style="padding:15px">Object Discovery Update</td>
    <td style="padding:15px">{base_path}/{version}/discovery/handle_object_discovery_update?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">handleSubnetDiscoveryUpdate(body, callback)</td>
    <td style="padding:15px">Subnet Discovery Update</td>
    <td style="padding:15px">{base_path}/{version}/discovery/handle_subnet_discovery_update?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">discoverSwitch(body, callback)</td>
    <td style="padding:15px">Discover Subnet</td>
    <td style="padding:15px">{base_path}/{version}/discovery/discover_switch?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateObjectDiscoveryData(body, callback)</td>
    <td style="padding:15px">Updates Object discovery data</td>
    <td style="padding:15px">{base_path}/{version}/discovery/updateObjectDiscoveryData?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">discoverReclaimSubnets(body, callback)</td>
    <td style="padding:15px">Discover Subnets</td>
    <td style="padding:15px">{base_path}/{version}/discovery/discover_reclaimable_subnets?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDiscoveryGetcmds(status, callback)</td>
    <td style="padding:15px">Discovery Commands List</td>
    <td style="padding:15px">{base_path}/{version}/discovery/getcmds?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">details(name, orgName, callback)</td>
    <td style="padding:15px">Get discovery template details</td>
    <td style="padding:15px">{base_path}/{version}/discovery_template/details?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDiscoveryDetails(address, type, orgName, callback)</td>
    <td style="padding:15px">Get discovery template details by network or subnet address</td>
    <td style="padding:15px">{base_path}/{version}/discovery_template/getdiscoverypreferences?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTemplateReferences(templateName, organizationName, organizationId, callback)</td>
    <td style="padding:15px">Discovery Template References</td>
    <td style="padding:15px">{base_path}/{version}/discovery_template/getreferences?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDomainList(orgId, draw, start, length, orgName, q, filterRules, sort, order, callback)</td>
    <td style="padding:15px">Discovery Template List by Organization</td>
    <td style="padding:15px">{base_path}/{version}/discovery_template/p_listbyorg?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDiscoveryReferences(body, callback)</td>
    <td style="padding:15px">Discovery Template References</td>
    <td style="padding:15px">{base_path}/{version}/discovery_template/deletereferences?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postDiscoveryTemplateEdit(body, callback)</td>
    <td style="padding:15px">Update Discovery Template</td>
    <td style="padding:15px">{base_path}/{version}/discovery_template/edit?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDiscoveryTemplateList(callback)</td>
    <td style="padding:15px">Discovery Template List</td>
    <td style="padding:15px">{base_path}/{version}/discovery_template/list?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postDiscoveryTemplateDelete(name, orgName, callback)</td>
    <td style="padding:15px">Delete Discovery Template</td>
    <td style="padding:15px">{base_path}/{version}/discovery_template/delete?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postDiscoveryTemplateAdd(body, callback)</td>
    <td style="padding:15px">Add Discovery Template</td>
    <td style="padding:15px">{base_path}/{version}/discovery_template/add?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">hierarchy(callback)</td>
    <td style="padding:15px">Domain List</td>
    <td style="padding:15px">{base_path}/{version}/domain/hierarchy?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">multiDelete(body, callback)</td>
    <td style="padding:15px">Domain Multidelete</td>
    <td style="padding:15px">{base_path}/{version}/domain/multidelete?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDomainPListbyorg(orgId, draw, start, length, orgName, q, inSubnetEdit, filterRules, sort, order, callback)</td>
    <td style="padding:15px">Domain List By Organization</td>
    <td style="padding:15px">{base_path}/{version}/domain/p_listbyorg?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">domainList(orgId, page, rows, orgName, q, inSubnetEdit, callback)</td>
    <td style="padding:15px">Domain List By Organization</td>
    <td style="padding:15px">{base_path}/{version}/domain/listbyorg?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDomainList2(callback)</td>
    <td style="padding:15px">Domain List</td>
    <td style="padding:15px">{base_path}/{version}/domain/list?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">domainPage(page, rows, callback)</td>
    <td style="padding:15px">Domain Search</td>
    <td style="padding:15px">{base_path}/{version}/domain/search?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">domainCreate(body, callback)</td>
    <td style="padding:15px">Domain Add</td>
    <td style="padding:15px">{base_path}/{version}/domain/add?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">domainUpdate(body, callback)</td>
    <td style="padding:15px">Domain Update</td>
    <td style="padding:15px">{base_path}/{version}/domain/update?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getReverseZoneList(orgId, draw, start, length, orgName, q, inSubnetEdit, filterRules, sort, order, callback)</td>
    <td style="padding:15px">Reverse Zone List By Organization</td>
    <td style="padding:15px">{base_path}/{version}/domain/p_revzonelistbyorg?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getADZoneList(orgId, draw, start, length, orgName, zoneName, q, inSubnetEdit, filterRules, sort, order, callback)</td>
    <td style="padding:15px">Domain List By Organization</td>
    <td style="padding:15px">{base_path}/{version}/domain/adzonelistbyorg?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDomainGet(id, name, orgName, callback)</td>
    <td style="padding:15px">Domain Details</td>
    <td style="padding:15px">{base_path}/{version}/domain/get?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getValuesByID(tableName, fieldValue, callback)</td>
    <td style="padding:15px">getValuesByID</td>
    <td style="padding:15px">{base_path}/{version}/dropdown/selectValueByID?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postExportComplianceMgmtMultidelete(body, callback)</td>
    <td style="padding:15px">ccTLD Multidelete</td>
    <td style="padding:15px">{base_path}/{version}/exportComplianceMgmt/multidelete?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getExportComplianceMgmtOfaccountriesgridlist(callback)</td>
    <td style="padding:15px">OFAC Country name and ccTLDs List</td>
    <td style="padding:15px">{base_path}/{version}/exportComplianceMgmt/ofaccountriesgridlist?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">ccTLDAdd(body, callback)</td>
    <td style="padding:15px">Domain Add</td>
    <td style="padding:15px">{base_path}/{version}/exportComplianceMgmt/add?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listSupportedTypes(callback)</td>
    <td style="padding:15px">Supported Export Types</td>
    <td style="padding:15px">{base_path}/{version}/export/supported_types?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">execute(type, organization, body, callback)</td>
    <td style="padding:15px">Export</td>
    <td style="padding:15px">{base_path}/{version}/export/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postFilesetEdit(body, callback)</td>
    <td style="padding:15px">Edit FileSet</td>
    <td style="padding:15px">{base_path}/{version}/fileset/edit?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteFolder(orgName, filesetName, callback)</td>
    <td style="padding:15px">Delete Folder</td>
    <td style="padding:15px">{base_path}/{version}/fileset/deletefolder?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteFileSet(name, organization, callback)</td>
    <td style="padding:15px">Delete File Set</td>
    <td style="padding:15px">{base_path}/{version}/fileset/delete?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getFileSet(name, organization, callback)</td>
    <td style="padding:15px">Details of FileSet</td>
    <td style="padding:15px">{base_path}/{version}/fileset/get?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getFilesetPrimaryServersList(orgName, callback)</td>
    <td style="padding:15px">Primary DHCP Appliance List</td>
    <td style="padding:15px">{base_path}/{version}/fileset/primary-servers-list?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getFilesetList(draw, start, length, filterRules, sort, order, callback)</td>
    <td style="padding:15px">Get File Set List</td>
    <td style="padding:15px">{base_path}/{version}/fileset/list?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getFilesetProtocols(callback)</td>
    <td style="padding:15px">Protocols List</td>
    <td style="padding:15px">{base_path}/{version}/fileset/protocols?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postFilesetAdd(body, callback)</td>
    <td style="padding:15px">Create FileSet</td>
    <td style="padding:15px">{base_path}/{version}/fileset/add?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">sync(body, callback)</td>
    <td style="padding:15px">FileSet Sync Operation</td>
    <td style="padding:15px">{base_path}/{version}/fileset/sync?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">u(twcChunkNumber, twcChunkSize, twcCurrentChunkSize, twcTotalSize, twcType, twcFilename, twcRelativePath, twcIdentifier, twcTotalChunks, callback)</td>
    <td style="padding:15px">Large file upload in chunks</td>
    <td style="padding:15px">{base_path}/{version}/files/largeFileUpload?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">uploadFile(fileName, body, callback)</td>
    <td style="padding:15px">Upload File</td>
    <td style="padding:15px">{base_path}/{version}/files/upload/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">uploadBinaryFile(fileName, body, callback)</td>
    <td style="padding:15px">Binary Upload</td>
    <td style="padding:15px">{base_path}/{version}/files/binaryupload/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">modify(body, callback)</td>
    <td style="padding:15px">Firewall Template Edit</td>
    <td style="padding:15px">{base_path}/{version}/firewall/template/modify?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listAssoc(address, templateName, orgName, callback)</td>
    <td style="padding:15px">Firewall Template List associated with the DNS Appliances</td>
    <td style="padding:15px">{base_path}/{version}/firewall/template/assoc/list?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateAssoc(address, templateName, callback)</td>
    <td style="padding:15px">Modify Firewall Template</td>
    <td style="padding:15px">{base_path}/{version}/firewall/template/assoc/update?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTemplate(name, orgName, forSelect, callback)</td>
    <td style="padding:15px">Firewall Template List</td>
    <td style="padding:15px">{base_path}/{version}/firewall/template/get?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postFirewallTemplateAdd(body, callback)</td>
    <td style="padding:15px">Add Firewall Template</td>
    <td style="padding:15px">{base_path}/{version}/firewall/template/add?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getFirewallTemplateList(orgName, forSelect, callback)</td>
    <td style="padding:15px">Firewall Template List</td>
    <td style="padding:15px">{base_path}/{version}/firewall/template/list?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postFirewallTemplateDelete(name, callback)</td>
    <td style="padding:15px">Firewall Template Delete</td>
    <td style="padding:15px">{base_path}/{version}/firewall/template/delete?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getGlobalOptionValue(optionName, callback)</td>
    <td style="padding:15px">Get Global Option</td>
    <td style="padding:15px">{base_path}/{version}/globals/getGlobalOption?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postGlobalsUpdate(body, callback)</td>
    <td style="padding:15px">Update Global Policies</td>
    <td style="padding:15px">{base_path}/{version}/globals/update?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getGlobalsList(callback)</td>
    <td style="padding:15px">Global Policies List</td>
    <td style="padding:15px">{base_path}/{version}/globals/list?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getMessagesAfter(serverIp, startDate, endDate, callback)</td>
    <td style="padding:15px">Server Heartbeat</td>
    <td style="padding:15px">{base_path}/{version}/heartbeat/getHeartbeat?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getHeartbeatMessages(ip, name, startDate, endDate, callback)</td>
    <td style="padding:15px">Heartbeat Messages from Server</td>
    <td style="padding:15px">{base_path}/{version}/heartbeat/messages?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listAddressBlocks(callback)</td>
    <td style="padding:15px">List of Address Blocks</td>
    <td style="padding:15px">{base_path}/{version}/hierarchy/address_block/list?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listAddressSpace(callback)</td>
    <td style="padding:15px">List of Address Space</td>
    <td style="padding:15px">{base_path}/{version}/hierarchy/address_space/list?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getBlockNetworkList(organization, space, address, mask, callback)</td>
    <td style="padding:15px">Address Block Network List</td>
    <td style="padding:15px">{base_path}/{version}/hierarchy/address_block/network/list?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listNetworks(orgName, space, address, callback)</td>
    <td style="padding:15px">List of Networks at the given node</td>
    <td style="padding:15px">{base_path}/{version}/hierarchy/network/list?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addAddressSpace(body, callback)</td>
    <td style="padding:15px">Add Address Space</td>
    <td style="padding:15px">{base_path}/{version}/hierarchy/address_space/add?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateAddressSpace(name, body, callback)</td>
    <td style="padding:15px">Update Address Space</td>
    <td style="padding:15px">{base_path}/{version}/hierarchy/address_space/update?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteAddressSpace(body, callback)</td>
    <td style="padding:15px">Delete Address Space</td>
    <td style="padding:15px">{base_path}/{version}/hierarchy/address_space/delete?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteAddressBlock(orgName, preserveChild, body, callback)</td>
    <td style="padding:15px">Delete Address Block</td>
    <td style="padding:15px">{base_path}/{version}/hierarchy/address_block/delete?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addSeedBlock(orgName, body, callback)</td>
    <td style="padding:15px">Add Address Space</td>
    <td style="padding:15px">{base_path}/{version}/hierarchy/seed_block/add?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateChildBlock(orgName, adjustHierarchy, space, body, callback)</td>
    <td style="padding:15px">Update Child Block</td>
    <td style="padding:15px">{base_path}/{version}/hierarchy/child_block/update?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateSeedBlock(orgName, adjustHierarchy, space, body, callback)</td>
    <td style="padding:15px">Update Seed Block</td>
    <td style="padding:15px">{base_path}/{version}/hierarchy/seed_block/update?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addChildBlock(orgName, adjustHierarchy, body, callback)</td>
    <td style="padding:15px">Add Child Block</td>
    <td style="padding:15px">{base_path}/{version}/hierarchy/child_block/add?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">splitSeedBlock(orgName, mask, body, callback)</td>
    <td style="padding:15px">Split Seed Block</td>
    <td style="padding:15px">{base_path}/{version}/hierarchy/seed_block/split?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">nextFreeNetworkList(orgName, mask, body, callback)</td>
    <td style="padding:15px">List of Address Blocks</td>
    <td style="padding:15px">{base_path}/{version}/hierarchy/next_free/networklist?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">nextFreeNetwork(orgName, mask, body, callback)</td>
    <td style="padding:15px">List of Address Blocks</td>
    <td style="padding:15px">{base_path}/{version}/hierarchy/next_free/network?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">nextFreeBlockList(orgName, mask, body, callback)</td>
    <td style="padding:15px">List of Address Blocks</td>
    <td style="padding:15px">{base_path}/{version}/hierarchy/next_free/blocklist?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">nextFreeBlock(orgName, mask, body, callback)</td>
    <td style="padding:15px">Address Block</td>
    <td style="padding:15px">{base_path}/{version}/hierarchy/next_free/block?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addNextFreeBlock(orgName, mask, name, body, callback)</td>
    <td style="padding:15px">Address Block</td>
    <td style="padding:15px">{base_path}/{version}/hierarchy/next_free/block/add?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addMultipleChildBlocks(orgName, adjustHierarchy, body, callback)</td>
    <td style="padding:15px">Add Multiple Child Blocks</td>
    <td style="padding:15px">{base_path}/{version}/hierarchy/child_block/add_multi?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">mapWave(callback)</td>
    <td style="padding:15px">Map of Network Hierarchy</td>
    <td style="padding:15px">{base_path}/{version}/hierarchy/map?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getHistoryListAsJsonString(draw, start, length, filterRules, sort, order, type, organization, id, callback)</td>
    <td style="padding:15px">List of Execution History</td>
    <td style="padding:15px">{base_path}/{version}/historytab/historylist?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getHomeObjectAlloc(organizationName, callback)</td>
    <td style="padding:15px">Object Allocation</td>
    <td style="padding:15px">{base_path}/{version}/home/objectAlloc?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getHomeObjectType(organizationName, callback)</td>
    <td style="padding:15px">Object Type</td>
    <td style="padding:15px">{base_path}/{version}/home/objectType?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDNSServerList(callback)</td>
    <td style="padding:15px">DNS Server List</td>
    <td style="padding:15px">{base_path}/{version}/home/getDNSServerList?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTopTenWidgetDataSvc(body, callback)</td>
    <td style="padding:15px">Full/Underutilized Networks and Subnets</td>
    <td style="padding:15px">{base_path}/{version}/home/top10WidgetData?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTopTenV6WidgetDataSvc(body, callback)</td>
    <td style="padding:15px">Full/Underutilized Networks and Subnets</td>
    <td style="padding:15px">{base_path}/{version}/home/top10v6WidgetData?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAdminRolesChart(organizationName, callback)</td>
    <td style="padding:15px">Admin roles information</td>
    <td style="padding:15px">{base_path}/{version}/home/adminRoles?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getIPAMSearchDetails(search, name, callback)</td>
    <td style="padding:15px">Get IPAM Search Details</td>
    <td style="padding:15px">{base_path}/{version}/home/getIPAMSearchDetails?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkDetails(ipAddress, callback)</td>
    <td style="padding:15px">Get network details by IPAddress</td>
    <td style="padding:15px">{base_path}/{version}/home/getNetworkDetails/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSubnetDetails(ipAddress, callback)</td>
    <td style="padding:15px">Subnet details by IP Address</td>
    <td style="padding:15px">{base_path}/{version}/home/getSubnetDetails/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getObjectDetails(ipAddress, subnetId, callback)</td>
    <td style="padding:15px">Get object details by IP Address/Subnet ID</td>
    <td style="padding:15px">{base_path}/{version}/home/getObjectDetails?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">determineIPDetail(ipAddress, callback)</td>
    <td style="padding:15px">Get the details of an IP Address</td>
    <td style="padding:15px">{base_path}/{version}/home/determineIPDetails/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getObbjectLineChartData(startDate, endDate, range, organizationName, callback)</td>
    <td style="padding:15px">Get Object Line Chart Data</td>
    <td style="padding:15px">{base_path}/{version}/home/getTotalObjectData?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTotalIPAMStats(organizationName, callback)</td>
    <td style="padding:15px">Get the IPAM statistics</td>
    <td style="padding:15px">{base_path}/{version}/home/getTotalIPAMStats?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getIPAMServerInfo(callback)</td>
    <td style="padding:15px">Get the IPAM server information</td>
    <td style="padding:15px">{base_path}/{version}/home/getIPAMServerInfo?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">faultMgmt(serverName, ip, faultLevel, serverType, draw, start, length, filterRules, q, sort, order, callback)</td>
    <td style="padding:15px">Get the notifications of appliances status by service name</td>
    <td style="padding:15px">{base_path}/{version}/home/faultMgmt?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCurrentAlarms(serverName, ip, faultLevel, serverType, draw, start, length, filterRules, q, sort, order, callback)</td>
    <td style="padding:15px">Get the notifications of appliances status by service name</td>
    <td style="padding:15px">{base_path}/{version}/home/getCurrentAlarms?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">faultMgmtp(draw, start, length, filterRules, q, sort, order, serverName, ip, faultLevel, serverType, callback)</td>
    <td style="padding:15px">Get the notifications of appliances status by service name</td>
    <td style="padding:15px">{base_path}/{version}/home/faultMgmtp?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">faultMgmtHosts(serverName, ip, faultLevel, serverType, callback)</td>
    <td style="padding:15px">Get the notifications of appliances status by host name</td>
    <td style="padding:15px">{base_path}/{version}/home/faultMgmtHosts?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getMonitoringCounts(serverName, ip, callback)</td>
    <td style="padding:15px">Get the monitoring status</td>
    <td style="padding:15px">{base_path}/{version}/home/monitoringServices?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getApplianceStatus(serverName, ip, callback)</td>
    <td style="padding:15px">Get all appliances status</td>
    <td style="padding:15px">{base_path}/{version}/home/applianceStatus?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getHealthIndices(callback)</td>
    <td style="padding:15px">Get values for health index scores</td>
    <td style="padding:15px">{base_path}/{version}/home/healthindices?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTotalTraffic(callback)</td>
    <td style="padding:15px">Get Total DNS traffic</td>
    <td style="padding:15px">{base_path}/{version}/home/getTotalTaffic?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDHCPTraffic(callback)</td>
    <td style="padding:15px">Get Total DHCP traffic</td>
    <td style="padding:15px">{base_path}/{version}/home/getDHCPTraffic?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCMDStats(callback)</td>
    <td style="padding:15px">Get CPU,Memory,Disk Usage</td>
    <td style="padding:15px">{base_path}/{version}/home/getCMDStats?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getIPAMSystemInfo(callback)</td>
    <td style="padding:15px">Get CPU, Memory, Disk Usage and other information.</td>
    <td style="padding:15px">{base_path}/{version}/home/getIPAMSystemInfo?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDHCPTrafficByServer(callback)</td>
    <td style="padding:15px">Get Total DNS traffic</td>
    <td style="padding:15px">{base_path}/{version}/home/getDHCPTafficByServer?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTotalTrafficByServer(callback)</td>
    <td style="padding:15px">Get Total DNS traffic</td>
    <td style="padding:15px">{base_path}/{version}/home/getTotalTafficByServer?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">faultMgmtServiceHistory(serverName, id, faultLevel, startDate, endDate, pageNo, pageSize, selectdb, callback)</td>
    <td style="padding:15px">Get Fault Management Service History</td>
    <td style="padding:15px">{base_path}/{version}/home/faultMgmtServiceHistory?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getServerHardwareInfo(serverId, serverType, callback)</td>
    <td style="padding:15px">Get server hardware information</td>
    <td style="padding:15px">{base_path}/{version}/home/getServerHardwareInfo?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getUserLastLogin(userName, userId, callback)</td>
    <td style="padding:15px">Get user last login information</td>
    <td style="padding:15px">{base_path}/{version}/home/getUserLastLogin?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getHtmlLoads(cpuCheck, memCheck, diskCheck, callback)</td>
    <td style="padding:15px">Get the appliance load ,memory and disk charts data</td>
    <td style="padding:15px">{base_path}/{version}/home/getRRdImageLoad?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getMYSQLData(address, callback)</td>
    <td style="padding:15px">Get MySQL charts data</td>
    <td style="padding:15px">{base_path}/{version}/home/getMYSQLCharts?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDNSHtmlLoads(service, callback)</td>
    <td style="padding:15px">Get chart data</td>
    <td style="padding:15px">{base_path}/{version}/home/getRRDDNSLoad?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getdhcpGroupedChart(service, callback)</td>
    <td style="padding:15px">Get chart data</td>
    <td style="padding:15px">{base_path}/{version}/home/getdhcpgroupedcharts?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getMonitorRRD(ip, type, callback)</td>
    <td style="padding:15px">Get Information of all Appliances</td>
    <td style="padding:15px">{base_path}/{version}/home/getMonitoringRRDCharts?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDNSDHCPServerList(callback)</td>
    <td style="padding:15px">list of DNS and DHCP Appliances</td>
    <td style="padding:15px">{base_path}/{version}/home/getDNSDHCPServerList?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getIpamAlerts(draw, start, length, filterRules, q, sort, order, startDate, endDate, callback)</td>
    <td style="padding:15px">Get IPAM server alerts</td>
    <td style="padding:15px">{base_path}/{version}/home/getIpamAlerts?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getIpamAlertsSummary(callback)</td>
    <td style="padding:15px">Get IPAM server alerts Summary</td>
    <td style="padding:15px">{base_path}/{version}/home/getIpamAlertsSummary?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getActiveSessions(page, rows, startDate, endDate, callback)</td>
    <td style="padding:15px">Get active sessions information</td>
    <td style="padding:15px">{base_path}/{version}/home/getActiveSessions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getActiveSessionsSummary(page, rows, callback)</td>
    <td style="padding:15px">Active Sessions Summary</td>
    <td style="padding:15px">{base_path}/{version}/home/getActiveSessionsSummary?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getActiveSessionsSnapshotSummary(callback)</td>
    <td style="padding:15px">Active Sessions Information Summary</td>
    <td style="padding:15px">{base_path}/{version}/home/getActiveSessionsSnapshotSummary?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getActiveSessionsChart(startDate, endDate, callback)</td>
    <td style="padding:15px">Get top ten sessions</td>
    <td style="padding:15px">{base_path}/{version}/home/getActiveSessionsChart?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAuditSummary(callback)</td>
    <td style="padding:15px">Audit Summary</td>
    <td style="padding:15px">{base_path}/{version}/home/auditSummary?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getMaskLength(name, callback)</td>
    <td style="padding:15px">Get mask length list</td>
    <td style="padding:15px">{base_path}/{version}/home/getMaskLength/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getServerDetailList(type, rows, page, filterRules, sort, order, callback)</td>
    <td style="padding:15px">Remote appliances list</td>
    <td style="padding:15px">{base_path}/{version}/home/serverDetailList?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getServerLogPaths(ip, scode, callback)</td>
    <td style="padding:15px">DNS appliance log paths</td>
    <td style="padding:15px">{base_path}/{version}/home/getServerLogPaths?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getLogData(type, filterRules, argc, ip, serverName, scode, noOfLine, logstart, logend, callback)</td>
    <td style="padding:15px">Remote Logs</td>
    <td style="padding:15px">{base_path}/{version}/home/logData?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getOMRReportData(ip, serverName, scode, type, callback)</td>
    <td style="padding:15px">Audit Logs Data</td>
    <td style="padding:15px">{base_path}/{version}/home/omreport?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAuditLogData(applianceIp, applianceName, applianceType, argc, q, draw, start, length, filterRules, logstart, logend, callback)</td>
    <td style="padding:15px">Audit Logs</td>
    <td style="padding:15px">{base_path}/{version}/home/auditLogDataNew?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getHomeAuditLogData(type, filterRules, argc, ip, serverName, logstart, logend, callback)</td>
    <td style="padding:15px">Audit Logs Data</td>
    <td style="padding:15px">{base_path}/{version}/home/auditLogData?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAuditTrendData(success, startDate, endDate, dateStyle, ipamIpAddress, callback)</td>
    <td style="padding:15px">Get Audit Trend Data</td>
    <td style="padding:15px">{base_path}/{version}/home/getAuditTrendData?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">subSys(argc, ip, argv, arga, callback)</td>
    <td style="padding:15px">Recent Command's Time</td>
    <td style="padding:15px">{base_path}/{version}/home/subsys?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getZoneDistributionChartData(organizationName, callback)</td>
    <td style="padding:15px">Data about Top ten Zones</td>
    <td style="padding:15px">{base_path}/{version}/home/getZoneDistChart?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getLicenseWarnInfo(callback)</td>
    <td style="padding:15px">License validation Information</td>
    <td style="padding:15px">{base_path}/{version}/home/licenseExpWarnInfo?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAMQTopicStats(topicName, startDate, endDate, pageNo, pageSize, callback)</td>
    <td style="padding:15px">Get AMQ Topics Stats</td>
    <td style="padding:15px">{base_path}/{version}/home/getAMQTopicStats?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">generateCsvReport(serverType, callback)</td>
    <td style="padding:15px">Export to CSV</td>
    <td style="padding:15px">{base_path}/{version}/home/exportcsv?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">generateVersionCSVReport(serverType, callback)</td>
    <td style="padding:15px">Export to CSV</td>
    <td style="padding:15px">{base_path}/{version}/home/exportversionstocsv?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">genrateTopTalkerCSVReport(ip, type, duration, callback)</td>
    <td style="padding:15px">Export to CSV</td>
    <td style="padding:15px">{base_path}/{version}/home/exporttoptalkerstocsv?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDNSHtmlInd(ip, type, service, callback)</td>
    <td style="padding:15px">Displays the number of objects allocated based on the object allocation type in the TCPWave IPAM.</td>
    <td style="padding:15px">{base_path}/{version}/home/getRRDDNSInd?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDHCPServerList(callback)</td>
    <td style="padding:15px">DHCP Appliance List</td>
    <td style="padding:15px">{base_path}/{version}/home/getDHCPServerList?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDNSLastSync(type, callback)</td>
    <td style="padding:15px">Server Last Sync Status</td>
    <td style="padding:15px">{base_path}/{version}/home/getDNSLastSync?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getClassfulNetworks(organizationName, callback)</td>
    <td style="padding:15px">Network classification</td>
    <td style="padding:15px">{base_path}/{version}/home/classnetworks?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSubnetCount(organizationName, callback)</td>
    <td style="padding:15px">Subnet classification</td>
    <td style="padding:15px">{base_path}/{version}/home/subnetscount?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getO(callback)</td>
    <td style="padding:15px">Object classification</td>
    <td style="padding:15px">{base_path}/{version}/home/objectcount?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSchedJobCount(callback)</td>
    <td style="padding:15px">Object classification</td>
    <td style="padding:15px">{base_path}/{version}/home/schedjobcount?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSchedJobGraph(startTime, callback)</td>
    <td style="padding:15px">Object classification</td>
    <td style="padding:15px">{base_path}/{version}/home/schedjobcountgraph?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getZoneCount(organizationName, callback)</td>
    <td style="padding:15px">Zone classification</td>
    <td style="padding:15px">{base_path}/{version}/home/zonecount?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDNSCount(organizationName, callback)</td>
    <td style="padding:15px">DNS Servers classification</td>
    <td style="padding:15px">{base_path}/{version}/home/dnscount?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDHCPCount(organizationName, callback)</td>
    <td style="padding:15px">DHCP Appliances classification</td>
    <td style="padding:15px">{base_path}/{version}/home/dhcpcount?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNDStats(id, callback)</td>
    <td style="padding:15px">DNS Servers classification</td>
    <td style="padding:15px">{base_path}/{version}/home/getndstats?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getIPAMSubVersion(callback)</td>
    <td style="padding:15px">IPAM subcomponents versions</td>
    <td style="padding:15px">{base_path}/{version}/home/getipamsubversion?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getIPAMConfiglist(callback)</td>
    <td style="padding:15px">IPAM configuration list</td>
    <td style="padding:15px">{base_path}/{version}/home/getipamconfiglist?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getdhcpScopes(callback)</td>
    <td style="padding:15px">Top ten DHCP scopes</td>
    <td style="padding:15px">{base_path}/{version}/home/getdhcpscopes?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRemoteVersions(type, callback)</td>
    <td style="padding:15px">Remote server versions</td>
    <td style="padding:15px">{base_path}/{version}/home/getRemoteVersions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDnsApplianceSummary(type, callback)</td>
    <td style="padding:15px">Remote server configuration</td>
    <td style="padding:15px">{base_path}/{version}/home/getdnsApplianceSummary?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getMonitoredDeviceStats(type, callback)</td>
    <td style="padding:15px">Monitored device statistics</td>
    <td style="padding:15px">{base_path}/{version}/home/getMonitoredDeviceStats?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getServerToZoneCount(callback)</td>
    <td style="padding:15px">DNS zone allocation chart</td>
    <td style="padding:15px">{base_path}/{version}/home/servertozonecount?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDNSTopTalkers(ip, type, duration, table, callback)</td>
    <td style="padding:15px">Server Top talker List</td>
    <td style="padding:15px">{base_path}/{version}/home/getToptalkers?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getHeatMask(id, ip, nstart, nend, orgName, callback)</td>
    <td style="padding:15px">Object classification</td>
    <td style="padding:15px">{base_path}/{version}/home/heatmapdata?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCloudProvidersCount(organizationName, callback)</td>
    <td style="padding:15px">Admin roles information</td>
    <td style="padding:15px">{base_path}/{version}/home/cloudProvidersCount?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getHAStatus(callback)</td>
    <td style="padding:15px">HA Configuration status</td>
    <td style="padding:15px">{base_path}/{version}/home/isHAConfigured?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getHAIPAMList(callback)</td>
    <td style="padding:15px">HA IPAM Appliances List</td>
    <td style="padding:15px">{base_path}/{version}/home/haList?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">generatePdfReport(report, ip, type, duration, callback)</td>
    <td style="padding:15px">PDF Report</td>
    <td style="padding:15px">{base_path}/{version}/home/pdfreport?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getLogsByPagination(applianceIp, applianceName, applianceType, applianceCode, argc, q, draw, start, length, filterRules, logstart, logend, callback)</td>
    <td style="padding:15px">Remote Logs</td>
    <td style="padding:15px">{base_path}/{version}/home/getLogs?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDNSTunnelDetectionData(ip, type, callback)</td>
    <td style="padding:15px">DNS tunnel detection data</td>
    <td style="padding:15px">{base_path}/{version}/home/dnsTunnelDetectionData?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSuspiciousQueries(ip, type, callback)</td>
    <td style="padding:15px">Get Suspicious Queries</td>
    <td style="padding:15px">{base_path}/{version}/home/getsuspiciousqueries?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getLongQueries(ip, type, callback)</td>
    <td style="padding:15px">Get Long Queries</td>
    <td style="padding:15px">{base_path}/{version}/home/getlongqueries?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">logsPage(draw, start, length, q, filterRules, sort, order, callback)</td>
    <td style="padding:15px">Search Admin Group</td>
    <td style="padding:15px">{base_path}/{version}/home/searchlogs?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postHomeDeletenotification(body, callback)</td>
    <td style="padding:15px">Delete alarms</td>
    <td style="padding:15px">{base_path}/{version}/home/deletenotification?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">hostName(name, callback)</td>
    <td style="padding:15px">Host Name Restriction</td>
    <td style="padding:15px">{base_path}/{version}/hostnamingpolicy/checkPolicy?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDetails(name, callback)</td>
    <td style="padding:15px">Host Name Policy Name Get</td>
    <td style="padding:15px">{base_path}/{version}/hostnamingpolicy/getdetails?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postHostnamingpolicyEdit(body, callback)</td>
    <td style="padding:15px">Host Naming Policy Edit</td>
    <td style="padding:15px">{base_path}/{version}/hostnamingpolicy/edit?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getHostnamingpolicyList(callback)</td>
    <td style="padding:15px">Host Naming policies List</td>
    <td style="padding:15px">{base_path}/{version}/hostnamingpolicy/list?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postHostnamingpolicyDelete(body, callback)</td>
    <td style="padding:15px">Host Naming Policy delete</td>
    <td style="padding:15px">{base_path}/{version}/hostnamingpolicy/delete?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postHostnamingpolicyAdd(body, callback)</td>
    <td style="padding:15px">Host Naming Policy Add</td>
    <td style="padding:15px">{base_path}/{version}/hostnamingpolicy/add?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getObjectForIPAM(address, callback)</td>
    <td style="padding:15px">Object Details</td>
    <td style="padding:15px">{base_path}/{version}/ipamAppliance/getObjectDetails?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">appliancePagelist(draw, start, length, q, filterRules, sort, order, callback)</td>
    <td style="padding:15px">IPAM Appliance List</td>
    <td style="padding:15px">{base_path}/{version}/ipamAppliance/list?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">ipamTemplateDelete(id, address, callback)</td>
    <td style="padding:15px">IPAM Appliance delete</td>
    <td style="padding:15px">{base_path}/{version}/ipamAppliance/delete?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">changeDebugSetting(address, debugPref, callback)</td>
    <td style="padding:15px">Appliance Debug Setting</td>
    <td style="padding:15px">{base_path}/{version}/ipamAppliance/debug-setting?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">manageService(body, callback)</td>
    <td style="padding:15px">Manage Appliance Services</td>
    <td style="padding:15px">{base_path}/{version}/ipamAppliance/manage?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getServiceStatus(body, callback)</td>
    <td style="padding:15px">IPAM Appliance Services Status</td>
    <td style="padding:15px">{base_path}/{version}/ipamAppliance/getStatus?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getLocalhostIP(callback)</td>
    <td style="padding:15px">Localhost IP address</td>
    <td style="padding:15px">{base_path}/{version}/ipamAppliance/getLocalhostIP?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfiguredType(callback)</td>
    <td style="padding:15px">Configured type either HA or DR</td>
    <td style="padding:15px">{base_path}/{version}/ipamAppliance/getConfiguredType?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRootAccessDtls(callback)</td>
    <td style="padding:15px">Get root access management details.</td>
    <td style="padding:15px">{base_path}/{version}/ipamAppliance/getRootAccessDtls?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSyncStatusDetails(address, callback)</td>
    <td style="padding:15px">Appliance Status GET</td>
    <td style="padding:15px">{base_path}/{version}/ipamAppliance/getSyncStatus?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">ipamApplianceSearch(draw, start, length, filterRules, sort, order, callback)</td>
    <td style="padding:15px">IPAM Appliance Search</td>
    <td style="padding:15px">{base_path}/{version}/ipamAppliance/search?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTrafficForIPAMAppliances(serverName, serverIp, argc, callback)</td>
    <td style="padding:15px">Get Live IPAM Traffic</td>
    <td style="padding:15px">{base_path}/{version}/ipamAppliance/ipamtraffic?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getIAPMApplianceName(address, callback)</td>
    <td style="padding:15px">GET Appliance Name</td>
    <td style="padding:15px">{base_path}/{version}/ipamAppliance/getApplianceName?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getProcessorsCount(address, callback)</td>
    <td style="padding:15px">Get number of Processors</td>
    <td style="padding:15px">{base_path}/{version}/ipamAppliance/getProcessorsCount?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">objectAssociatedOrgList(address, callback)</td>
    <td style="padding:15px">Get object associated organizations list</td>
    <td style="padding:15px">{base_path}/{version}/ipamAppliance/objectAssociatedOrgList?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getIpamApplianceGet(id, address, callback)</td>
    <td style="padding:15px">IPAM Template List</td>
    <td style="padding:15px">{base_path}/{version}/ipamAppliance/get?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postIpamApplianceUpdate(body, callback)</td>
    <td style="padding:15px">Update IPAM Appliance</td>
    <td style="padding:15px">{base_path}/{version}/ipamAppliance/update?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postIpamApplianceCreate(body, callback)</td>
    <td style="padding:15px">Create IPAM Appliance</td>
    <td style="padding:15px">{base_path}/{version}/ipamAppliance/create?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postIpamApplianceSyncAll(body, callback)</td>
    <td style="padding:15px">syncAll</td>
    <td style="padding:15px">{base_path}/{version}/ipamAppliance/syncAll?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getIPAMTemplateGet(callback)</td>
    <td style="padding:15px">IPAM Template List</td>
    <td style="padding:15px">{base_path}/{version}/IPAMTemplate/get?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getVersion(callback)</td>
    <td style="padding:15px">Get IPAM server version information</td>
    <td style="padding:15px">{base_path}/{version}/ipam/version?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postV6dhcpserverDelete(address, v4Address, callback)</td>
    <td style="padding:15px">Delete DHCP Appliance</td>
    <td style="padding:15px">{base_path}/{version}/v6dhcpserver/delete?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getV6dhcpserverGenerateConfig(v6Address, serverName, v4Address, organizationName, callback)</td>
    <td style="padding:15px">Generate Configuration</td>
    <td style="padding:15px">{base_path}/{version}/v6dhcpserver/generateConfig?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postV6dhcpserverSyncall(body, organizationName, callback)</td>
    <td style="padding:15px">Generate Configuration</td>
    <td style="padding:15px">{base_path}/{version}/v6dhcpserver/syncall?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postV6dhcpserverEdit(body, callback)</td>
    <td style="padding:15px">Update DHCP Appliance</td>
    <td style="padding:15px">{base_path}/{version}/v6dhcpserver/edit?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getV6dhcpserverServerConfiguration(address, callback)</td>
    <td style="padding:15px">Configuration information</td>
    <td style="padding:15px">{base_path}/{version}/v6dhcpserver/server-configuration?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getV6dhcpserverSearch(page, rows, callback)</td>
    <td style="padding:15px">Server Search</td>
    <td style="padding:15px">{base_path}/{version}/v6dhcpserver/search?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getV6dhcpserverPrimaryServersList(inEdit, orgName, callback)</td>
    <td style="padding:15px">Primary IPv6 DHCP Appliance List</td>
    <td style="padding:15px">{base_path}/{version}/v6dhcpserver/primary-servers-list?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getV6dhcpserverV6dhcpActiveLeases(serverIp, v6serverIp, callback)</td>
    <td style="padding:15px">DHCPv6 Appliance Active Leases</td>
    <td style="padding:15px">{base_path}/{version}/v6dhcpserver/v6dhcpActiveLeases?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listbyorg(organizationName, callback)</td>
    <td style="padding:15px">DHCP Appliance List</td>
    <td style="padding:15px">{base_path}/{version}/v6dhcpserver/listbyorg?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getV6dhcpserverServerSubnetReferences(address, callback)</td>
    <td style="padding:15px">Subnet Reference</td>
    <td style="padding:15px">{base_path}/{version}/v6dhcpserver/server-subnet-references?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getV6dhcpserverViewconfig(address, organizationName, callback)</td>
    <td style="padding:15px">Get Configuration</td>
    <td style="padding:15px">{base_path}/{version}/v6dhcpserver/viewconfig?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getV6dhcpserverList(callback)</td>
    <td style="padding:15px">DHCP Appliance List</td>
    <td style="padding:15px">{base_path}/{version}/v6dhcpserver/list?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postV6dhcpserverAdd(body, callback)</td>
    <td style="padding:15px">Add IPv6 DHCP Appliance</td>
    <td style="padding:15px">{base_path}/{version}/v6dhcpserver/add?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getV6scopeList(subnetAddress, orgName, draw, start, length, filterRules, sort, order, callback)</td>
    <td style="padding:15px">List IPv6 DHCP Scopes</td>
    <td style="padding:15px">{base_path}/{version}/v6scope/list?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteScope(body, callback)</td>
    <td style="padding:15px">IPv6 Scope Delete</td>
    <td style="padding:15px">{base_path}/{version}/v6scope/delete?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getScopeValidateRequestData(subAdr, organizationName, callback)</td>
    <td style="padding:15px">Scope Validation Request</td>
    <td style="padding:15px">{base_path}/{version}/v6scope/validate_request?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">autoProvision(body, callback)</td>
    <td style="padding:15px">Scope Create</td>
    <td style="padding:15px">{base_path}/{version}/v6scope/autoprovision?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteSingleScope(body, callback)</td>
    <td style="padding:15px">IPv6 Scope Delete</td>
    <td style="padding:15px">{base_path}/{version}/v6scope/singledelete?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">templateList(page, rows, q, filterRules, callback)</td>
    <td style="padding:15px">List IPv6 DHCP Option Templates</td>
    <td style="padding:15px">{base_path}/{version}/v6templates/option-template-list?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">pageList(serverTypeCode, entityTypeCode, referenceType, referenceId, page, rows, q, filterRules, inEdit, organizationName, callback)</td>
    <td style="padding:15px">Template Page</td>
    <td style="padding:15px">{base_path}/{version}/v6templates/page/{pathv1}/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">templateParamsList(templateId, templateName, organizationName, viewAll, callback)</td>
    <td style="padding:15px">List Configured Options Of IPv6 DHCP Option Template</td>
    <td style="padding:15px">{base_path}/{version}/v6templates/list-params?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">v6paramSearch(draw, start, length, filterRules, sort, order, callback)</td>
    <td style="padding:15px">IPv6 Template Parameter Name Search</td>
    <td style="padding:15px">{base_path}/{version}/v6templates/v6params-name/search?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">v6paramValueSearch(draw, start, length, filterRules, sort, order, callback)</td>
    <td style="padding:15px">IPv4 Template Parameter Value Search</td>
    <td style="padding:15px">{base_path}/{version}/v6templates/v6params-value/search?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postV6templatesUpdate(body, callback)</td>
    <td style="padding:15px">Updates IPv6 DHCP Option Template</td>
    <td style="padding:15px">{base_path}/{version}/v6templates/update?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postV6templatesCreate(body, callback)</td>
    <td style="padding:15px">Add IPv6 DHCP Option Template</td>
    <td style="padding:15px">{base_path}/{version}/v6templates/create?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postIpv6networkDelete(body, callback)</td>
    <td style="padding:15px">Delete IPV6 Network</td>
    <td style="padding:15px">{base_path}/{version}/ipv6network/delete?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getIpv6networkPage(draw, start, length, q, filterRules, sort, order, callback)</td>
    <td style="padding:15px">IPv6 Network list</td>
    <td style="padding:15px">{base_path}/{version}/ipv6network/page?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postIpv6networkEdit(body, callback)</td>
    <td style="padding:15px">Update IPv6 Network</td>
    <td style="padding:15px">{base_path}/{version}/ipv6network/edit?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">networkFinder(subnetAddress, organizationName, callback)</td>
    <td style="padding:15px">Subnet Property Finder</td>
    <td style="padding:15px">{base_path}/{version}/ipv6network/subnets/ip/{pathv1}/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getStats(id, address, callback)</td>
    <td style="padding:15px">IPv6 Network Statistics</td>
    <td style="padding:15px">{base_path}/{version}/ipv6network/stats?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">searchNetworks(page, rows, callback)</td>
    <td style="padding:15px">Search Networks</td>
    <td style="padding:15px">{base_path}/{version}/ipv6network/search?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getIpv6networkIpv6subnetsNetworkIdNwMaskLengthNwStartAddressAllocType(networkId, nwMaskLength, nwStartAddress, allocType, callback)</td>
    <td style="padding:15px">IPV6 Subnet List</td>
    <td style="padding:15px">{base_path}/{version}/ipv6network/ipv6subnets/{pathv1}/{pathv2}/{pathv3}/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postIpv6networkAdd(body, callback)</td>
    <td style="padding:15px">Add IPV6 Network</td>
    <td style="padding:15px">{base_path}/{version}/ipv6network/add?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postIpv6objectDeleteMultiple(body, callback)</td>
    <td style="padding:15px">Delete IPv6 Objects</td>
    <td style="padding:15px">{base_path}/{version}/ipv6object/delete-multiple?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getIpv6objectPage(subnetId, organization, subnetAddress, draw, start, length, q, filterRules, sort, order, callback)</td>
    <td style="padding:15px">IPV6 object list</td>
    <td style="padding:15px">{base_path}/{version}/ipv6object/page?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getIpv6objectSearch(draw, start, length, filterRules, sort, order, callback)</td>
    <td style="padding:15px">Search Objects</td>
    <td style="padding:15px">{base_path}/{version}/ipv6object/search?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postIpv6objectEdit(body, callback)</td>
    <td style="padding:15px">Update IPv6 Object</td>
    <td style="padding:15px">{base_path}/{version}/ipv6object/edit?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">ping(ip, callback)</td>
    <td style="padding:15px">Ping IPv6 Address</td>
    <td style="padding:15px">{base_path}/{version}/ipv6object/tools/ping?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">jobexecute(body, callback)</td>
    <td style="padding:15px">Schedule Object Job</td>
    <td style="padding:15px">{base_path}/{version}/ipv6object/schedObjectOperation?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">schedAdd(body, repeat, frequency, runtime, starttime, endtime, day, description, repeatInterval, repeatCount, jobId, callback)</td>
    <td style="padding:15px">Scheduled object add operation.</td>
    <td style="padding:15px">{base_path}/{version}/ipv6object/sched_add?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDHCPDetails(id, address, callback)</td>
    <td style="padding:15px">Object DHCP Details</td>
    <td style="padding:15px">{base_path}/{version}/ipv6object/getDHCPDetails?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">traceroute(ip, callback)</td>
    <td style="padding:15px">Trace Route Object</td>
    <td style="padding:15px">{base_path}/{version}/ipv6object/tools/traceroute?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">ipv6DuplicateResult(name, domainName, orgName, ipAddr, callback)</td>
    <td style="padding:15px">Object Duplicate Name Checker</td>
    <td style="padding:15px">{base_path}/{version}/ipv6object/IPv6duplicateNameChecker?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getIpv6objectGet(id, address, organizationName, callback)</td>
    <td style="padding:15px">Get IPv6 Object</td>
    <td style="padding:15px">{base_path}/{version}/ipv6object/get?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postIpv6objectRrDelete(body, callback)</td>
    <td style="padding:15px">Resource Record Delete</td>
    <td style="padding:15px">{base_path}/{version}/ipv6object/rr/delete?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postIpv6objectRrAdd(body, callback)</td>
    <td style="padding:15px">Network Resource Record Add</td>
    <td style="padding:15px">{base_path}/{version}/ipv6object/rr/add?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postIpv6objectAdd(body, callback)</td>
    <td style="padding:15px">Add IPv6 Object</td>
    <td style="padding:15px">{base_path}/{version}/ipv6object/add?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postIpv6subnetgroupDeleteAll(body, callback)</td>
    <td style="padding:15px">IPV6 Subnet Group Delete All</td>
    <td style="padding:15px">{base_path}/{version}/ipv6subnetgroup/deleteAll?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listSubnets(subnetGrpId, subnetGrpName, orgName, draw, start, length, filterRules, sort, order, callback)</td>
    <td style="padding:15px">List Subnets present in the given Subnet Group</td>
    <td style="padding:15px">{base_path}/{version}/ipv6subnetgroup/listSubnets?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">disassociateSubnet(fullAddress, orgName, callback)</td>
    <td style="padding:15px">Disassociate Subnet</td>
    <td style="padding:15px">{base_path}/{version}/ipv6subnetgroup/disassociate?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSubnet(id, name, organization, callback)</td>
    <td style="padding:15px">IPV6 Subnet Group Get</td>
    <td style="padding:15px">{base_path}/{version}/ipv6subnetgroup/get?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postIpv6subnetgroupUpdate(body, callback)</td>
    <td style="padding:15px">IPV6 Subnet Group Update</td>
    <td style="padding:15px">{base_path}/{version}/ipv6subnetgroup/update?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getIpv6subnetgroupList(callback)</td>
    <td style="padding:15px">IPV6 Subnet Group List</td>
    <td style="padding:15px">{base_path}/{version}/ipv6subnetgroup/list?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getIpv6subnetgroupListbyorg(organizationId, organizationName, callback)</td>
    <td style="padding:15px">IPV6 Subnet Group Organization List</td>
    <td style="padding:15px">{base_path}/{version}/ipv6subnetgroup/listbyorg?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postIpv6subnetgroupAdd(body, callback)</td>
    <td style="padding:15px">IPv6 Subnet Group Add</td>
    <td style="padding:15px">{base_path}/{version}/ipv6subnetgroup/add?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postIpv6subnetDelete(body, orgName, callback)</td>
    <td style="padding:15px">Deletes IPv6 subnets</td>
    <td style="padding:15px">{base_path}/{version}/ipv6subnet/delete?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getIpv6subnetPaged(networkAddress, orgName, draw, start, length, q, filterRules, sort, order, callback)</td>
    <td style="padding:15px">Subnet list page</td>
    <td style="padding:15px">{base_path}/{version}/ipv6subnet/paged?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postIpv6subnetEdit(body, callback)</td>
    <td style="padding:15px">IPv6 Subnet Edit</td>
    <td style="padding:15px">{base_path}/{version}/ipv6subnet/edit?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createSingle(body, callback)</td>
    <td style="padding:15px">IPv6 Subnet Add</td>
    <td style="padding:15px">{base_path}/{version}/ipv6subnet/add?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">searchSubnets(page, rows, callback)</td>
    <td style="padding:15px">Subnet Search</td>
    <td style="padding:15px">{base_path}/{version}/ipv6subnet/search?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getIpv6SubnetDetails(subnetAddress, orgName, callback)</td>
    <td style="padding:15px">IPv6 Subnet Details</td>
    <td style="padding:15px">{base_path}/{version}/ipv6subnet/getIpv6SubnetDetails?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSubNetByAddress(address, organizationName, callback)</td>
    <td style="padding:15px">Get Subnet</td>
    <td style="padding:15px">{base_path}/{version}/ipv6subnet/ip/{pathv1}/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getIpv6subnetActiveLeases(page, rows, serverAddress, v4serverAddress, subnetAddress, organizationName, callback)</td>
    <td style="padding:15px">List Active Leases</td>
    <td style="padding:15px">{base_path}/{version}/ipv6subnet/activeLeases?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getIpv6subnetListv6subnet(networkIp, orgName, page, rows, q, filterRules, sort, order, callback)</td>
    <td style="padding:15px">Subnet List Page</td>
    <td style="padding:15px">{base_path}/{version}/ipv6subnet/listv6subnet?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCloudAttributeNames(type, subnetID, callback)</td>
    <td style="padding:15px">getCloudAttributeNames</td>
    <td style="padding:15px">{base_path}/{version}/importcloud/attr?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getClousHostedSubnets(organization, callback)</td>
    <td style="padding:15px">getClousHostedSubnets</td>
    <td style="padding:15px">{base_path}/{version}/importcloud/subnet/page?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateAutoImportSubnets(body, callback)</td>
    <td style="padding:15px">updateAutoImportSubnets</td>
    <td style="padding:15px">{base_path}/{version}/importcloud/updateAutoImportSubnets?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">doImport(body, callback)</td>
    <td style="padding:15px">Import from cloud</td>
    <td style="padding:15px">{base_path}/{version}/importcloud/import?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getImportSupportedTypes(callback)</td>
    <td style="padding:15px">Supported Import File Types</td>
    <td style="padding:15px">{base_path}/{version}/import/supported_types?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listFields(type, callback)</td>
    <td style="padding:15px">Import Field's Type</td>
    <td style="padding:15px">{base_path}/{version}/import/fields?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postImportType(type, body, callback)</td>
    <td style="padding:15px">Import</td>
    <td style="padding:15px">{base_path}/{version}/import/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRoothintsEdit(body, callback)</td>
    <td style="padding:15px">Internet Root Hint Edit</td>
    <td style="padding:15px">{base_path}/{version}/roothints/edit?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRoothintsList(callback)</td>
    <td style="padding:15px">Internet Root Hint List</td>
    <td style="padding:15px">{base_path}/{version}/roothints/list?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRoothintsSync(callback)</td>
    <td style="padding:15px">Internet Root Hints Sync</td>
    <td style="padding:15px">{base_path}/{version}/roothints/sync?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postLocationDelete(body, callback)</td>
    <td style="padding:15px">Delete Location</td>
    <td style="padding:15px">{base_path}/{version}/location/delete?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postLocationUpdate(body, callback)</td>
    <td style="padding:15px">Edit Location</td>
    <td style="padding:15px">{base_path}/{version}/location/update?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getFullLocations(organizationName, organizationId, page, rows, q, draw, start, length, inEdit, callback)</td>
    <td style="padding:15px">Get Location</td>
    <td style="padding:15px">{base_path}/{version}/location/getFullLocations?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">locationPage(page, rows, callback)</td>
    <td style="padding:15px">Search Location</td>
    <td style="padding:15px">{base_path}/{version}/location/search?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getLocationReferences(body, callback)</td>
    <td style="padding:15px">Location List References</td>
    <td style="padding:15px">{base_path}/{version}/location/locationReferences?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getLocReferences(page, rows, tables, id, count, callback)</td>
    <td style="padding:15px">Location List References</td>
    <td style="padding:15px">{base_path}/{version}/location/references?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postLocationGet(body, callback)</td>
    <td style="padding:15px">Get Location</td>
    <td style="padding:15px">{base_path}/{version}/location/get?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postLocationAdd(body, callback)</td>
    <td style="padding:15px">Add Location</td>
    <td style="padding:15px">{base_path}/{version}/location/add?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getLogcatList(callback)</td>
    <td style="padding:15px">Log Categories List</td>
    <td style="padding:15px">{base_path}/{version}/logcat/list?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postLogchannelDelete(id, channel, callback)</td>
    <td style="padding:15px">Delete Log Channels</td>
    <td style="padding:15px">{base_path}/{version}/logchannel/delete?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postLogchannelEdit(body, callback)</td>
    <td style="padding:15px">Update Log Channels</td>
    <td style="padding:15px">{base_path}/{version}/logchannel/edit?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">logChannelPage(page, rows, callback)</td>
    <td style="padding:15px">Search Log Channels</td>
    <td style="padding:15px">{base_path}/{version}/logchannel/search?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">searchLogChannelReferences(page, rows, tables, id, count, callback)</td>
    <td style="padding:15px">Log Channel References</td>
    <td style="padding:15px">{base_path}/{version}/logchannel/references?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getLogchannelGet(channel, callback)</td>
    <td style="padding:15px">Get Log Channel</td>
    <td style="padding:15px">{base_path}/{version}/logchannel/get?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getLogchannelList(callback)</td>
    <td style="padding:15px">List of Log Channel</td>
    <td style="padding:15px">{base_path}/{version}/logchannel/list?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postLogchannelAdd(body, callback)</td>
    <td style="padding:15px">Add Log Channel</td>
    <td style="padding:15px">{base_path}/{version}/logchannel/add?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postServicesManage(body, callback)</td>
    <td style="padding:15px">Manage Appliance Services</td>
    <td style="padding:15px">{base_path}/{version}/services/manage?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postServicesGetStatus(body, callback)</td>
    <td style="padding:15px">Appliance Status</td>
    <td style="padding:15px">{base_path}/{version}/services/getStatus?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getMirroredzoneGetdetails(name, id, callback)</td>
    <td style="padding:15px">Get Mirrored Zone Details</td>
    <td style="padding:15px">{base_path}/{version}/mirroredzone/getdetails?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postMirroredzoneEdit(body, callback)</td>
    <td style="padding:15px">Edit Mirrored Zone</td>
    <td style="padding:15px">{base_path}/{version}/mirroredzone/edit?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getMirroredzoneListbypage(draw, start, length, filterRules, q, sort, order, callback)</td>
    <td style="padding:15px">Mirrored Zones List</td>
    <td style="padding:15px">{base_path}/{version}/mirroredzone/listbypage?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postMirroredzoneDelete(body, callback)</td>
    <td style="padding:15px">Delete Mirrored Zone</td>
    <td style="padding:15px">{base_path}/{version}/mirroredzone/delete?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postMirroredzoneAdd(body, callback)</td>
    <td style="padding:15px">Add Mirrored Zones</td>
    <td style="padding:15px">{base_path}/{version}/mirroredzone/add?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postMonitorEdit(body, callback)</td>
    <td style="padding:15px">Monitoring Service</td>
    <td style="padding:15px">{base_path}/{version}/monitor/edit?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateServiceStatus(body, status, callback)</td>
    <td style="padding:15px">Monitoring Service</td>
    <td style="padding:15px">{base_path}/{version}/monitor/updateServiceStatus?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">changeMonitoring(body, callback)</td>
    <td style="padding:15px">Enable or Disable Monitoring</td>
    <td style="padding:15px">{base_path}/{version}/monitor/monitoring?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listMonitorServiceConfig(callback)</td>
    <td style="padding:15px">List Monitoring Services</td>
    <td style="padding:15px">{base_path}/{version}/monitor/listconfig?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateMonitorService(body, callback)</td>
    <td style="padding:15px">Update Monitoring Service</td>
    <td style="padding:15px">{base_path}/{version}/monitor/update?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">changeMultiMonitoring(body, callback)</td>
    <td style="padding:15px">Enable or Disable Monitoring</td>
    <td style="padding:15px">{base_path}/{version}/monitor/multimonitoring?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">enableDisableMonitoring(body, callback)</td>
    <td style="padding:15px">Enables or Disables Monitoring on specified resources</td>
    <td style="padding:15px">{base_path}/{version}/monitor/monitoringUpdate?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getMonitoringServiceDetails(serviceName, callback)</td>
    <td style="padding:15px">Monitoring Service Details</td>
    <td style="padding:15px">{base_path}/{version}/monitor/getServiceDetails?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getMonitoringHierachy(callback)</td>
    <td style="padding:15px">Monitoring Hierachy Details</td>
    <td style="padding:15px">{base_path}/{version}/monitor/monitoring_hierarchy?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">monitorServiceConfigHistory(serviceName, draw, start, length, filterRules, q, sort, order, callback)</td>
    <td style="padding:15px">Monitoring Services History</td>
    <td style="padding:15px">{base_path}/{version}/monitor/history?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getMsgcertmgmtCertlist(callback)</td>
    <td style="padding:15px">List Certificate</td>
    <td style="padding:15px">{base_path}/{version}/msgcertmgmt/certlist?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postMsgcertmgmtDuplicateAlias(body, callback)</td>
    <td style="padding:15px">Duplicate Alias check</td>
    <td style="padding:15px">{base_path}/{version}/msgcertmgmt/duplicateAlias?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getMsgcertmgmtCertValidate(cfile, callback)</td>
    <td style="padding:15px">Validation Certificate</td>
    <td style="padding:15px">{base_path}/{version}/msgcertmgmt/certValidate?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getMsgcertmgmtAllcertValidDays(callback)</td>
    <td style="padding:15px">Get number of days all certificates are valid</td>
    <td style="padding:15px">{base_path}/{version}/msgcertmgmt/allcertValidDays?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postMsgcertmgmtCertDelete(body, callback)</td>
    <td style="padding:15px">Delete Certificate</td>
    <td style="padding:15px">{base_path}/{version}/msgcertmgmt/certDelete?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postMsgcertmgmtChangeKeystorePassword(body, callback)</td>
    <td style="padding:15px">Change Keystore Password</td>
    <td style="padding:15px">{base_path}/{version}/msgcertmgmt/changeKeystorePassword?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postMsgcertmgmtJettyRestart(callback)</td>
    <td style="padding:15px">Restart Jetty Server</td>
    <td style="padding:15px">{base_path}/{version}/msgcertmgmt/jettyRestart?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">keystorageValidate(body, callback)</td>
    <td style="padding:15px">Validate Keystore</td>
    <td style="padding:15px">{base_path}/{version}/msgcertmgmt/keystoreValidate?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">msgcertificateImport(body, callback)</td>
    <td style="padding:15px">Import Message Certificate</td>
    <td style="padding:15px">{base_path}/{version}/msgcertmgmt/msgcertImport?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">msgcreateSelfSignedCert(body, callback)</td>
    <td style="padding:15px">Create and Import a Self-Signed Message Certificate</td>
    <td style="padding:15px">{base_path}/{version}/msgcertmgmt/createSelfSignedCert?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">certPublishToRemotes(body, callback)</td>
    <td style="padding:15px">Publish Certificate</td>
    <td style="padding:15px">{base_path}/{version}/msgcertmgmt/certPublish?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">truststoreDownload(callback)</td>
    <td style="padding:15px">Download Truststore</td>
    <td style="padding:15px">{base_path}/{version}/msgcertmgmt/truststoreDownload?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postNetworkDelete(body, callback)</td>
    <td style="padding:15px">Network Delete</td>
    <td style="padding:15px">{base_path}/{version}/network/delete?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkPaged(draw, start, length, filterRules, q, sort, order, callback)</td>
    <td style="padding:15px">Network Details</td>
    <td style="padding:15px">{base_path}/{version}/network/paged?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postNetworkEdit(body, callback)</td>
    <td style="padding:15px">Edit Network</td>
    <td style="padding:15px">{base_path}/{version}/network/edit?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNextAvailableNetwork(orgId, orgName, mask, callback)</td>
    <td style="padding:15px">Get next available network</td>
    <td style="padding:15px">{base_path}/{version}/network/getNextAvailableNetwork?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postNetworkAddMultiple(body, callback)</td>
    <td style="padding:15px">Network Add</td>
    <td style="padding:15px">{base_path}/{version}/network/add_multiple?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRRDataEdit(ownerName, callback)</td>
    <td style="padding:15px">Network Resource Record Expand View Edit</td>
    <td style="padding:15px">{base_path}/{version}/network/getRRDataExpandViewEdit/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkSubnetsIpSubnetAddressOrganizationName(subnetAddress, organizationName, callback)</td>
    <td style="padding:15px">Subnet Property Finder</td>
    <td style="padding:15px">{base_path}/{version}/network/subnets/ip/{pathv1}/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">networkPropertyFinder(networkAddress, organizationName, callback)</td>
    <td style="padding:15px">Network Property Finder</td>
    <td style="padding:15px">{base_path}/{version}/network/propertyFinder/ip/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkStats(id, address, callback)</td>
    <td style="padding:15px">Network Statistics</td>
    <td style="padding:15px">{base_path}/{version}/network/stats?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkSearch(page, rows, callback)</td>
    <td style="padding:15px">Search Networks</td>
    <td style="padding:15px">{base_path}/{version}/network/search?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkDetailsByIP(addr1, addr2, addr3, addr4, organizationName, callback)</td>
    <td style="padding:15px">Network Details by IP</td>
    <td style="padding:15px">{base_path}/{version}/network/detailsByIP?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createNextFreeNetwork(orgName, name, zoneTemplate, mask, body, callback)</td>
    <td style="padding:15px">Network Add</td>
    <td style="padding:15px">{base_path}/{version}/network/add_next?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkGetRRData(networkId, ipAddress, organizationName, callback)</td>
    <td style="padding:15px">Network Resource Record List</td>
    <td style="padding:15px">{base_path}/{version}/network/getRRData?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postNetworkRrEdit(existingOwner, existingData, body, callback)</td>
    <td style="padding:15px">Network Resource Record Update</td>
    <td style="padding:15px">{base_path}/{version}/network/rr/edit?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkList(page, rows, callback)</td>
    <td style="padding:15px">Network List</td>
    <td style="padding:15px">{base_path}/{version}/network/list?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postNetworkRrDelete(body, callback)</td>
    <td style="padding:15px">Delete Resource Records</td>
    <td style="padding:15px">{base_path}/{version}/network/rr/delete?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">check(body, callback)</td>
    <td style="padding:15px">Associated Servers Check</td>
    <td style="padding:15px">{base_path}/{version}/network/associated-servers-check?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postNetworkRrAdd(body, callback)</td>
    <td style="padding:15px">Network Resource Record Add</td>
    <td style="padding:15px">{base_path}/{version}/network/rr/add?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postNetworkAdd(body, callback)</td>
    <td style="padding:15px">Network Add</td>
    <td style="padding:15px">{base_path}/{version}/network/add?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postOspatchUpload(body, callback)</td>
    <td style="padding:15px">Patch Upload</td>
    <td style="padding:15px">{base_path}/{version}/ospatch/upload?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">osPatchList(callback)</td>
    <td style="padding:15px">Firmware Patch List</td>
    <td style="padding:15px">{base_path}/{version}/ospatch/list?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deployAPatch(patchName, appliances, callback)</td>
    <td style="padding:15px">Deploy a Firmware Patch</td>
    <td style="padding:15px">{base_path}/{version}/ospatch/deploy?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getObjectSearch(draw, start, length, filterRules, sort, order, callback)</td>
    <td style="padding:15px">List Object Search</td>
    <td style="padding:15px">{base_path}/{version}/object/search?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getObjectPaged(subnetId, subnetAddress, draw, start, length, orgName, filterRules, sort, order, callback)</td>
    <td style="padding:15px">Object List</td>
    <td style="padding:15px">{base_path}/{version}/object/paged?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postObjectEdit(body, callback)</td>
    <td style="padding:15px">Object Edit</td>
    <td style="padding:15px">{base_path}/{version}/object/edit?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getObjectToolsPing(ip, callback)</td>
    <td style="padding:15px">Ping IP Address</td>
    <td style="padding:15px">{base_path}/{version}/object/tools/ping?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listUnused(subnetId, subnetAddress, organizationName, callback)</td>
    <td style="padding:15px">Unused Object List</td>
    <td style="padding:15px">{base_path}/{version}/object/list/unused?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getIPAddress(fqdn, organizationName, callback)</td>
    <td style="padding:15px">Get IP Address</td>
    <td style="padding:15px">{base_path}/{version}/object/getip?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNextFreeIp(subnetAddress, organizationName, callback)</td>
    <td style="padding:15px">Next Free IP</td>
    <td style="padding:15px">{base_path}/{version}/object/nextfreeip?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postObjectSchedObjectOperation(body, callback)</td>
    <td style="padding:15px">Schedule Object Job</td>
    <td style="padding:15px">{base_path}/{version}/object/schedObjectOperation?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postObjectSchedAdd(body, repeat, frequency, runtime, starttime, endtime, day, description, repeatInterval, repeatCount, jobId, callback)</td>
    <td style="padding:15px">Scheduled object add operation.</td>
    <td style="padding:15px">{base_path}/{version}/object/sched_add?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">validateVMWareAuthSource(objectIP, objectName, domainName, body, callback)</td>
    <td style="padding:15px">Validate VMWare Object</td>
    <td style="padding:15px">{base_path}/{version}/object/validatevmwareobject?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">schedEdit(body, repeat, frequency, runtime, starttime, endtime, day, description, repeatInterval, repeatCount, jobId, callback)</td>
    <td style="padding:15px">Edit Schedule Operation</td>
    <td style="padding:15px">{base_path}/{version}/object/sched_edit?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">schedDeleteMultiple(body, repeat, frequency, runtime, starttime, endtime, day, description, repeatInterval, repeatCount, jobId, callback)</td>
    <td style="padding:15px">Schedule Multiple Object Delete</td>
    <td style="padding:15px">{base_path}/{version}/object/sched_deletemultiple?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">delMultiple(body, callback)</td>
    <td style="padding:15px">Object Delete</td>
    <td style="padding:15px">{base_path}/{version}/object/delete-multiple?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getObjectReferences(addressList, organizationName, callback)</td>
    <td style="padding:15px">Get Object References</td>
    <td style="padding:15px">{base_path}/{version}/object/get-referenced-rrs?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateNSMultipleForobject(body, callback)</td>
    <td style="padding:15px">Object Dynamic NS Flag Update</td>
    <td style="padding:15px">{base_path}/{version}/object/update-multiple-ns?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getForUser(id, address, organizationName, callback)</td>
    <td style="padding:15px">Object for User</td>
    <td style="padding:15px">{base_path}/{version}/object/getforuser?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getObjectGetDHCPDetails(id, address, orgName, callback)</td>
    <td style="padding:15px">Object DHCP Details</td>
    <td style="padding:15px">{base_path}/{version}/object/getDHCPDetails?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRRDataAdd(ownerName, data, callback)</td>
    <td style="padding:15px">Object Resource Record Details</td>
    <td style="padding:15px">{base_path}/{version}/object/getObjectRRDataExpandView/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">generateCsvForSearch(callback)</td>
    <td style="padding:15px">CSV export of Object Search Grid</td>
    <td style="padding:15px">{base_path}/{version}/object/csvforsearch?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">searchObjectReferences(page, rows, tables, id, count, callback)</td>
    <td style="padding:15px">Find Object References</td>
    <td style="padding:15px">{base_path}/{version}/object/references?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">duplicateResult(name, domainName, orgName, ipAddr, callback)</td>
    <td style="padding:15px">Object Duplicate Name Checker</td>
    <td style="padding:15px">{base_path}/{version}/object/duplicateNameChecker?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">pingcheck(address, callback)</td>
    <td style="padding:15px">Ping before create</td>
    <td style="padding:15px">{base_path}/{version}/object/pingcheck?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getObjectToolsTraceroute(ip, callback)</td>
    <td style="padding:15px">Trace Route Object</td>
    <td style="padding:15px">{base_path}/{version}/object/tools/traceroute?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postObjectTraverse(body, callback)</td>
    <td style="padding:15px">Resource Record Traverse</td>
    <td style="padding:15px">{base_path}/{version}/object/traverse?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">dcObjectslist(organizationId, organizationName, callback)</td>
    <td style="padding:15px">Domain Controller Objects</td>
    <td style="padding:15px">{base_path}/{version}/object/dcobjectlist?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">dcObjectslistByDomain(organizationId, organizationName, domainName, callback)</td>
    <td style="padding:15px">DC object List by Domain</td>
    <td style="padding:15px">{base_path}/{version}/object/dcobjectlistbydomain?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteLeaseRequest(objectAddress, serverAddress, callback)</td>
    <td style="padding:15px">Delete Active DHCP Lease</td>
    <td style="padding:15px">{base_path}/{version}/object/deleteDHCPLease?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSubnetAddress(address, callback)</td>
    <td style="padding:15px">Subnet Address</td>
    <td style="padding:15px">{base_path}/{version}/object/getSnAddr?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">syncObjectToDNSServers(objectIps, organizationName, callback)</td>
    <td style="padding:15px">Sync Objects</td>
    <td style="padding:15px">{base_path}/{version}/object/sync?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">exportRRs(address, fileName, scope, defaultARec, callback)</td>
    <td style="padding:15px">Export Resource Records</td>
    <td style="padding:15px">{base_path}/{version}/object/exportRRs?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">multiEdit(body, callback)</td>
    <td style="padding:15px">MultiObject Edit</td>
    <td style="padding:15px">{base_path}/{version}/object/multiedit?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getFingerprintData(mac, callback)</td>
    <td style="padding:15px">Get Fingerprint Information</td>
    <td style="padding:15px">{base_path}/{version}/object/getFingerPrintData?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getLeaseCount(organizationName, callback)</td>
    <td style="padding:15px">Get Active and Expired Lease Counts</td>
    <td style="padding:15px">{base_path}/{version}/object/getLeaseCounts?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNextFreeIP(subnetId, subnetAddr, orgName, callback)</td>
    <td style="padding:15px">Get the next available free IP in a given subnet.</td>
    <td style="padding:15px">{base_path}/{version}/object/getNextFreeIP?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDiscoveredObjectData(id, callback)</td>
    <td style="padding:15px">Object Discovered Details</td>
    <td style="padding:15px">{base_path}/{version}/object/getDiscoveredData?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">changeReference(existingIP, newIP, organizationName, callback)</td>
    <td style="padding:15px">Object Resource Record Update</td>
    <td style="padding:15px">{base_path}/{version}/object/changeReference?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">reclaimDHCPObjects(body, callback)</td>
    <td style="padding:15px">Objects Reclaim</td>
    <td style="padding:15px">{base_path}/{version}/object/reclaimObjects?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postObjectRrEdit(existingOwner, existingData, body, callback)</td>
    <td style="padding:15px">Object Resource Record Update</td>
    <td style="padding:15px">{base_path}/{version}/object/rr/edit?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getObjectList(subnetId, subnetAddress, page, rows, organizationName, callback)</td>
    <td style="padding:15px">Object List</td>
    <td style="padding:15px">{base_path}/{version}/object/list?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getObjectListCriteria(subnetId, subnetAddress, classCode, allocType, page, rows, callback)</td>
    <td style="padding:15px">Object List by Criteria</td>
    <td style="padding:15px">{base_path}/{version}/object/listCriteria?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postObjectRrDelete(body, callback)</td>
    <td style="padding:15px">Resource Record Delete</td>
    <td style="padding:15px">{base_path}/{version}/object/rr/delete?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postObjectAdd(body, callback)</td>
    <td style="padding:15px">Object Add</td>
    <td style="padding:15px">{base_path}/{version}/object/add?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postObjectRrAdd(body, callback)</td>
    <td style="padding:15px">Object Resource Record Add</td>
    <td style="padding:15px">{base_path}/{version}/object/rr/add?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">export(name, body, callback)</td>
    <td style="padding:15px">Export Organization Data</td>
    <td style="padding:15px">{base_path}/{version}/orgbulkops/export?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">exportOrganizationData(name, body, callback)</td>
    <td style="padding:15px">Export Organization Data</td>
    <td style="padding:15px">{base_path}/{version}/orgbulkops/orgexport?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">downloadZipFile(fileName, callback)</td>
    <td style="padding:15px">Export Organization Data</td>
    <td style="padding:15px">{base_path}/{version}/orgbulkops/download?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getProgressStatus(operationCode, callback)</td>
    <td style="padding:15px">Get the IPAM statistics</td>
    <td style="padding:15px">{base_path}/{version}/orgbulkops/getProgressStatus?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getOrgStatus(operationCode, callback)</td>
    <td style="padding:15px">TIMS-7003: You are not authorized to perform this operation.</td>
    <td style="padding:15px">{base_path}/{version}/orgbulkops/getProgressAllStatus?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postOrgbulkopsImport(body, callback)</td>
    <td style="padding:15px">Import Organization Data</td>
    <td style="padding:15px">{base_path}/{version}/orgbulkops/import?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postOrganizationDelete(body, callback)</td>
    <td style="padding:15px">Organization Delete</td>
    <td style="padding:15px">{base_path}/{version}/organization/delete?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">orgRootZone(body, callback)</td>
    <td style="padding:15px">Enable Root Zone</td>
    <td style="padding:15px">{base_path}/{version}/organization/enablerootzone?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">isRootZoneEnabled(callback)</td>
    <td style="padding:15px">Is Root Zone Enabled</td>
    <td style="padding:15px">{base_path}/{version}/organization/isrootzoneenabled?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">rootZoneEnabledList(callback)</td>
    <td style="padding:15px">Root Zone Enabled List</td>
    <td style="padding:15px">{base_path}/{version}/organization/rootzoneenabledlist?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">organizationPage(page, rows, callback)</td>
    <td style="padding:15px">Search Organization</td>
    <td style="padding:15px">{base_path}/{version}/organization/search?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getOrganizationGet(name, callback)</td>
    <td style="padding:15px">Get Organization</td>
    <td style="padding:15px">{base_path}/{version}/organization/get?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postOrganizationUpdate(body, callback)</td>
    <td style="padding:15px">Organization Edit</td>
    <td style="padding:15px">{base_path}/{version}/organization/update?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getOrganizationList(callback)</td>
    <td style="padding:15px">Organization List</td>
    <td style="padding:15px">{base_path}/{version}/organization/list?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postOrganizationAdd(body, callback)</td>
    <td style="padding:15px">Organization Add</td>
    <td style="padding:15px">{base_path}/{version}/organization/add?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">paramUserDefCreate(body, callback)</td>
    <td style="padding:15px">User Defined DHCP Options Create</td>
    <td style="padding:15px">{base_path}/{version}/params/dhcp-option-custom-create?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">paramUserDefUpdate(body, callback)</td>
    <td style="padding:15px">User Defined DHCP Options Edit</td>
    <td style="padding:15px">{base_path}/{version}/params/dhcp-option-custom-update?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">customFolderCreate(body, callback)</td>
    <td style="padding:15px">Create Custom Folder</td>
    <td style="padding:15px">{base_path}/{version}/params/custom-folder-create?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">customFolderUpdate(body, callback)</td>
    <td style="padding:15px">Edit Custom Folder</td>
    <td style="padding:15px">{base_path}/{version}/params/custom-folder-update?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postParamsCustomFolderDelete(folderName, callback)</td>
    <td style="padding:15px">Delete Custom Folder</td>
    <td style="padding:15px">{base_path}/{version}/params/custom-folder-delete?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">paramUserDefDelete(name, subOption, groupName, callback)</td>
    <td style="padding:15px">User Defined DHCP Options Delete</td>
    <td style="padding:15px">{base_path}/{version}/params/dhcp-option-custom-delete?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">dhcpGroupsList(callback)</td>
    <td style="padding:15px">Custom Folders List</td>
    <td style="padding:15px">{base_path}/{version}/params/groupsList?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">dhcpGroupSearch(page, rows, callback)</td>
    <td style="padding:15px">Custom Folders Search List</td>
    <td style="padding:15px">{base_path}/{version}/params/search?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">v6ParamsList(callback)</td>
    <td style="padding:15px">IPV6 Param List</td>
    <td style="padding:15px">{base_path}/{version}/params/v6ParamsList?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">objectMove(body, callback)</td>
    <td style="padding:15px">Object Move</td>
    <td style="padding:15px">{base_path}/{version}/qmove/object?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">objectScheduleMoveCheck(body, callback)</td>
    <td style="padding:15px">Object Move</td>
    <td style="padding:15px">{base_path}/{version}/qmove/objectSchedChecks?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRpzTemplateModify(body, callback)</td>
    <td style="padding:15px">Edit DNS Response Zone Policy(RPZ) Template</td>
    <td style="padding:15px">{base_path}/{version}/rpz/template/modify?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRpzTemplateAssocList(address, templateName, orgName, callback)</td>
    <td style="padding:15px">List DNS Remote Association with RPZ Template</td>
    <td style="padding:15px">{base_path}/{version}/rpz/template/assoc/list?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRpzTemplateAssocUpdate(address, templateName, callback)</td>
    <td style="padding:15px">Update DNS Remote Instances Associated with Template</td>
    <td style="padding:15px">{base_path}/{version}/rpz/template/assoc/update?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">policyFileslist(callback)</td>
    <td style="padding:15px">List the Response Policy Zone (RPZ) rules files.</td>
    <td style="padding:15px">{base_path}/{version}/rpz/policy/fileslist?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updatePolicyFile(body, callback)</td>
    <td style="padding:15px">Update RPZ policy file</td>
    <td style="padding:15px">{base_path}/{version}/rpz/policy/updatepolicyfile?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deletePolicyFile(pfile, callback)</td>
    <td style="padding:15px">Delete RPZ policy file</td>
    <td style="padding:15px">{base_path}/{version}/rpz/policy/deletepolicyfile?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPolicyFileRules(page, rows, callback)</td>
    <td style="padding:15px">List the Response Policy Zone (RPZ) rules from the temporary table.</td>
    <td style="padding:15px">{base_path}/{version}/rpz/policy/getpolicyfilerules?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">resetEditMode(callback)</td>
    <td style="padding:15px">Reset the exclusive edit mode lock.</td>
    <td style="padding:15px">{base_path}/{version}/rpz/policy/reseteditmode?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">emptyPolicyRulesTable(callback)</td>
    <td style="padding:15px">Empty policy rule table</td>
    <td style="padding:15px">{base_path}/{version}/rpz/policy/emptypolicyrulestable?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">loadPolicyFileRules(body, callback)</td>
    <td style="padding:15px">Load the Response Policy Zone(RPZ) policy rules from specified file into the temporary table.</td>
    <td style="padding:15px">{base_path}/{version}/rpz/policy/loadpolicyfilerules?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">policyfileDownload(pfile, callback)</td>
    <td style="padding:15px">Download the Response Policy Zone (RPZ) rules file.</td>
    <td style="padding:15px">{base_path}/{version}/rpz/policy/policyfiledownload?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">policyfileValidate(pfile, callback)</td>
    <td style="padding:15px">Validate the RPZ rules in the file.</td>
    <td style="padding:15px">{base_path}/{version}/rpz/policy/policyfilevalidate?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">policyfileImport(body, callback)</td>
    <td style="padding:15px">Import the Response Policy Zone (RPZ) policy rules file.</td>
    <td style="padding:15px">{base_path}/{version}/rpz/policy/policyfileImport?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">policyruleDelete(body, callback)</td>
    <td style="padding:15px">Delete the Response Zone Policy(RPZ) rules file</td>
    <td style="padding:15px">{base_path}/{version}/rpz/policy/policyruleDelete?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updatePolicyRule(body, callback)</td>
    <td style="padding:15px">Add or Update the Response Policy Zone (RPZ) policy rule in the temporary table in the TCPWave IPAM.
 Below example shows sample request JSON:
 {  "policy_trigger":"yahoo1.com",  "policy_rr":"CNAME",  "policy_rhvalue":"xen.tcpwave.com."  }</td>
    <td style="padding:15px">{base_path}/{version}/rpz/policy/updatePolicyRule?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRpzTemplateAdd(body, callback)</td>
    <td style="padding:15px">Add DNS Response Zone Policy(RPZ) Template</td>
    <td style="padding:15px">{base_path}/{version}/rpz/template/add?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRpzTemplateGet(name, callback)</td>
    <td style="padding:15px">Get DNS Response Policy Zone(RPZ) Template</td>
    <td style="padding:15px">{base_path}/{version}/rpz/template/get?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRpzTemplateList(orgName, callback)</td>
    <td style="padding:15px">List DNS Response Zone Policy(RPZ) templates</td>
    <td style="padding:15px">{base_path}/{version}/rpz/template/list?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRpzTemplateDelete(name, callback)</td>
    <td style="padding:15px">Delete DNS Response Zone Policy(RPZ) Template</td>
    <td style="padding:15px">{base_path}/{version}/rpz/template/delete?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRevzoneMultidelete(body, callback)</td>
    <td style="padding:15px">Reverse Zone Delete Multiple</td>
    <td style="padding:15px">{base_path}/{version}/revzone/multidelete?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRevZone(body, callback)</td>
    <td style="padding:15px">Reverse Zone Get</td>
    <td style="padding:15px">{base_path}/{version}/revzone/getRevZone?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">revZonePageList(draw, start, length, filterRules, sort, order, callback)</td>
    <td style="padding:15px">Reverse Zone List</td>
    <td style="padding:15px">{base_path}/{version}/revzone/page?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkMask(address, orgId, orgName, callback)</td>
    <td style="padding:15px">Get Network Mask For a Subnet</td>
    <td style="padding:15px">{base_path}/{version}/revzone/getNetworkMask?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">zoneForceSync(zoneOrgNames, callback)</td>
    <td style="padding:15px">Reverse Zone Force Sync</td>
    <td style="padding:15px">{base_path}/{version}/revzone/forcesynczones?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">zonePage(page, rows, callback)</td>
    <td style="padding:15px">Reverse Zone Search</td>
    <td style="padding:15px">{base_path}/{version}/revzone/search?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRevzoneGet(revZoneId, revZoneName, orgName, callback)</td>
    <td style="padding:15px">Get RR List</td>
    <td style="padding:15px">{base_path}/{version}/revzone/get?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRevzoneRrEdit(existingOwner, existingData, body, callback)</td>
    <td style="padding:15px">Reverse Zone Resource Record Update</td>
    <td style="padding:15px">{base_path}/{version}/revzone/rr/edit?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRevzoneUpdate(body, callback)</td>
    <td style="padding:15px">Reverse Zone Edit</td>
    <td style="padding:15px">{base_path}/{version}/revzone/update?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRevzoneRrDelete(body, callback)</td>
    <td style="padding:15px">Delete Resource Records</td>
    <td style="padding:15px">{base_path}/{version}/revzone/rr/delete?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRevzoneAdd(body, callback)</td>
    <td style="padding:15px">Reverse Zone Add</td>
    <td style="padding:15px">{base_path}/{version}/revzone/add?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRevzoneRrAdd(body, callback)</td>
    <td style="padding:15px">Reverse Zone Resource Record Add</td>
    <td style="padding:15px">{base_path}/{version}/revzone/rr/add?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRoleList(callback)</td>
    <td style="padding:15px">Admin Role List</td>
    <td style="padding:15px">{base_path}/{version}/role/list?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">configure(body, callback)</td>
    <td style="padding:15px">Configure root access authentication on appliances</td>
    <td style="padding:15px">{base_path}/{version}/rootaccessmgmt/configure?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listVaultTypes(callback)</td>
    <td style="padding:15px">Supported Vault Types</td>
    <td style="padding:15px">{base_path}/{version}/rootaccessmgmt/list_vault_types?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getVaultDetails(callback)</td>
    <td style="padding:15px">Get Vault preferences</td>
    <td style="padding:15px">{base_path}/{version}/rootaccessmgmt/get?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getLogLevel(callback)</td>
    <td style="padding:15px">Get Debug level</td>
    <td style="padding:15px">{base_path}/{version}/sched/opers/loginfo?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getJobExecStatusAfter(exectime, callback)</td>
    <td style="padding:15px">Get Job Execution status</td>
    <td style="padding:15px">{base_path}/{version}/sched/execstatus/after/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getJobIdStatus(id, page, rows, utcdiff, callback)</td>
    <td style="padding:15px">Get Scheduled Jobs</td>
    <td style="padding:15px">{base_path}/{version}/sched/execstatus/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteSchedExecstatusId(id, callback)</td>
    <td style="padding:15px">Delete All Job Execution status</td>
    <td style="padding:15px">{base_path}/{version}/sched/execstatus/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addExecStatus(body, callback)</td>
    <td style="padding:15px">Add Schedule Job Execution Status</td>
    <td style="padding:15px">{base_path}/{version}/sched/execstatus/add?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postSchedJobExecute(body, callback)</td>
    <td style="padding:15px">Scheduled Job Execution</td>
    <td style="padding:15px">{base_path}/{version}/sched/job/execute?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">displayList(utcdiff, callback)</td>
    <td style="padding:15px">Currently Available Scheduled Tasks List</td>
    <td style="padding:15px">{base_path}/{version}/sched/job/displayList?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">jobexecuteNow(jid, callback)</td>
    <td style="padding:15px">Execute Scheduled Job Now</td>
    <td style="padding:15px">{base_path}/{version}/sched/job/executeNow?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">multipleDelete(body, callback)</td>
    <td style="padding:15px">Scheduled job multi delete</td>
    <td style="padding:15px">{base_path}/{version}/sched/job/multipleDelete?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">purgeJobs(body, callback)</td>
    <td style="padding:15px">Purge Job</td>
    <td style="padding:15px">{base_path}/{version}/sched/job/purgeJobs?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">ipamDelete(id, callback)</td>
    <td style="padding:15px">Current Scheduled Job Delete</td>
    <td style="padding:15px">{base_path}/{version}/sched/job/ipamDelete/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">ipamEdit(body, repeat, frequency, runtime, starttime, endtime, day, description, repeatInterval, repeatCount, utcdiff, callback)</td>
    <td style="padding:15px">Scheduled Job Edit</td>
    <td style="padding:15px">{base_path}/{version}/sched/job/ipamEdit?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">ipamAdd(repeat, frequency, runtime, starttime, endtime, day, description, repeatInterval, repeatCount, jid, jtype, jdata, jarg, utcdiff, callback)</td>
    <td style="padding:15px">Scheduled Job Add</td>
    <td style="padding:15px">{base_path}/{version}/sched/job/ipamAdd?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">jobTypeList(callback)</td>
    <td style="padding:15px">List Scheduled Jobs Type</td>
    <td style="padding:15px">{base_path}/{version}/sched/job/jobTypeList?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSchedJobGetserversyncscheduledetails(ip, entity, callback)</td>
    <td style="padding:15px">Scheduled Appliance Sync Details</td>
    <td style="padding:15px">{base_path}/{version}/sched/job/getserversyncscheduledetails?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSchedJobId(id, callback)</td>
    <td style="padding:15px">Get Scheduled Job</td>
    <td style="padding:15px">{base_path}/{version}/sched/job/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteSchedJobId(id, callback)</td>
    <td style="padding:15px">Delete Scheduled Job</td>
    <td style="padding:15px">{base_path}/{version}/sched/job/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postSchedJobEdit(body, callback)</td>
    <td style="padding:15px">Scheduled Job Edit</td>
    <td style="padding:15px">{base_path}/{version}/sched/job/edit?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSchedJobList(callback)</td>
    <td style="padding:15px">Scheduled Tasks List</td>
    <td style="padding:15px">{base_path}/{version}/sched/job/list?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postSchedJobAdd(body, callback)</td>
    <td style="padding:15px">Schedule Job Add</td>
    <td style="padding:15px">{base_path}/{version}/sched/job/add?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postSchedhandlerExecute(body, callback)</td>
    <td style="padding:15px">Schedule Object Job</td>
    <td style="padding:15px">{base_path}/{version}/schedhandler/execute?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postSchedhandlerSchedule(body, repeat, frequency, runtime, starttime, endtime, day, description, repeatInterval, repeatCount, jobId, callback)</td>
    <td style="padding:15px">Schedule an operation</td>
    <td style="padding:15px">{base_path}/{version}/schedhandler/schedule?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteFromScheduledRpt(reportType, ids, callback)</td>
    <td style="padding:15px">Audit Report Delete</td>
    <td style="padding:15px">{base_path}/{version}/Scheduledreports/delete?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getScheduledreportsScheduledreportlist(reportType, draw, start, length, filterRules, callback)</td>
    <td style="padding:15px">Scheduled Audit Report List</td>
    <td style="padding:15px">{base_path}/{version}/Scheduledreports/Scheduledreportlist?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">subscribeToScheduledRpt(reportType, period, body, callback)</td>
    <td style="padding:15px">Subscribe Audit Report</td>
    <td style="padding:15px">{base_path}/{version}/Scheduledreports/subscribe?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getScopeList(subnetAddress, orgName, filterRules, draw, start, length, sort, order, serverType, callback)</td>
    <td style="padding:15px">Scope List</td>
    <td style="padding:15px">{base_path}/{version}/scope/list?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createScope(body, callback)</td>
    <td style="padding:15px">Scope Create</td>
    <td style="padding:15px">{base_path}/{version}/scope/create?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postScopeDelete(body, callback)</td>
    <td style="padding:15px">Scope Delete</td>
    <td style="padding:15px">{base_path}/{version}/scope/delete?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getScopeValidateRequest(minAddr, maxAddr, organizationName, callback)</td>
    <td style="padding:15px">Scope Validation Request</td>
    <td style="padding:15px">{base_path}/{version}/scope/validate_request?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getIPAMSearchResults(searchTerm, searchCategory, searchType, fieldName, fieldKey, orgId, orgName, callback)</td>
    <td style="padding:15px">Get IPAM Search details</td>
    <td style="padding:15px">{base_path}/{version}/search/getIPAMSearchResults?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">rebuildIndex(callback)</td>
    <td style="padding:15px">Rebuild Search Index</td>
    <td style="padding:15px">{base_path}/{version}/search/rebuildIndex?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">licenseCheck(callback)</td>
    <td style="padding:15px">Check Valid license</td>
    <td style="padding:15px">{base_path}/{version}/server_license/check?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateRemoteLicense(address, license, serverType, serverName, callback)</td>
    <td style="padding:15px">Remote license Update</td>
    <td style="padding:15px">{base_path}/{version}/server_license/update_remote?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postServerLicenseAdd(body, callback)</td>
    <td style="padding:15px">License Add</td>
    <td style="padding:15px">{base_path}/{version}/server_license/add?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getServerLicenseList(serverType, callback)</td>
    <td style="padding:15px">Server license List</td>
    <td style="padding:15px">{base_path}/{version}/server_license/list?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getServerDebugSetting(serverType, address, debugPref, callback)</td>
    <td style="padding:15px">Server Debug Setting</td>
    <td style="padding:15px">{base_path}/{version}/server/debug-setting?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getServerGetProcessorsCount(address, applianceType, callback)</td>
    <td style="padding:15px">Get number of Processors</td>
    <td style="padding:15px">{base_path}/{version}/server/getProcessorsCount?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfigForClone(address, organization, callback)</td>
    <td style="padding:15px">Server Get Clone Configuration</td>
    <td style="padding:15px">{base_path}/{version}/server/getCloneConfiguration?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllServerAddresses(serverType, callback)</td>
    <td style="padding:15px">DNS Server List</td>
    <td style="padding:15px">{base_path}/{version}/server/list?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getIPDetails(address, organization, nicType, callback)</td>
    <td style="padding:15px">Object Details</td>
    <td style="padding:15px">{base_path}/{version}/server/getObjectDetailsForNIC?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">sameSubnetCheck(body, nicType, callback)</td>
    <td style="padding:15px">Check Objects</td>
    <td style="padding:15px">{base_path}/{version}/server/checkObjectsInSameSubnet?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">isDNSDHCP(address, callback)</td>
    <td style="padding:15px">DNS, DHCP and Both</td>
    <td style="padding:15px">{base_path}/{version}/server/isDNSDHCP?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDuplexSetting(device, nicType, address, applianceType, callback)</td>
    <td style="padding:15px">Get NIC duplex settings</td>
    <td style="padding:15px">{base_path}/{version}/server/getDuplexSettings?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRtTables(nicType, address, applianceType, callback)</td>
    <td style="padding:15px">Get Routing Tables</td>
    <td style="padding:15px">{base_path}/{version}/server/getRtTables?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRoutes(nicType, address, applianceType, callback)</td>
    <td style="padding:15px">Get Routes of Network</td>
    <td style="padding:15px">{base_path}/{version}/server/getRoutes?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAttachedNICs(nicType, address, applianceType, callback)</td>
    <td style="padding:15px">GET Attached Network Interfaces</td>
    <td style="padding:15px">{base_path}/{version}/server/getAttachedNics?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">dnsTunnelDetection(serverType, address, detect, callback)</td>
    <td style="padding:15px">Enable/Disable DNS Tunnel Detection</td>
    <td style="padding:15px">{base_path}/{version}/server/dnstunneldetection?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPatchHistory(address, applianceType, callback)</td>
    <td style="padding:15px">Get patch deployment history</td>
    <td style="padding:15px">{base_path}/{version}/server/getPatchHistory?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">syncNics(syncStatus, address, applianceType, callback)</td>
    <td style="padding:15px">Sync Network Interfaces</td>
    <td style="padding:15px">{base_path}/{version}/server/syncNics?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">changeRemoteNIC(syncStatus, appliance, callback)</td>
    <td style="padding:15px">Update the value of NIC from CLI</td>
    <td style="padding:15px">{base_path}/{version}/server/changeRemoteNIC?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getServerGet(address, organization, callback)</td>
    <td style="padding:15px">Object Details</td>
    <td style="padding:15px">{base_path}/{version}/server/get?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getServersyncGet(serverId, address, serverType, orgName, isV6, callback)</td>
    <td style="padding:15px">Appliance Sync Status GET</td>
    <td style="padding:15px">{base_path}/{version}/serversync/get?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listO(organizationId, organizationName, callback)</td>
    <td style="padding:15px">Subnet Group Organization List</td>
    <td style="padding:15px">{base_path}/{version}/subnetgroup/listbyorg?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">subnetGroupPage(page, rows, callback)</td>
    <td style="padding:15px">Subnet Group Search</td>
    <td style="padding:15px">{base_path}/{version}/subnetgroup/search?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">searchSubnetGroupReferences(page, rows, tables, id, count, callback)</td>
    <td style="padding:15px">Subnet Group References</td>
    <td style="padding:15px">{base_path}/{version}/subnetgroup/references?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSubnetgroupListSubnets(subnetGrpId, subnetGrpName, orgName, draw, start, length, filterRules, sort, order, callback)</td>
    <td style="padding:15px">List Subnets present in the given Subnet Group</td>
    <td style="padding:15px">{base_path}/{version}/subnetgroup/listSubnets?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postSubnetgroupDisassociate(fullAddress, orgName, callback)</td>
    <td style="padding:15px">Disassociate Subnet</td>
    <td style="padding:15px">{base_path}/{version}/subnetgroup/disassociate?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSubnetgroupGet(id, name, organization, callback)</td>
    <td style="padding:15px">Subnet Group Get</td>
    <td style="padding:15px">{base_path}/{version}/subnetgroup/get?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postSubnetgroupUpdate(body, callback)</td>
    <td style="padding:15px">Subnet Group Edit</td>
    <td style="padding:15px">{base_path}/{version}/subnetgroup/update?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postSubnetgroupDelete(body, callback)</td>
    <td style="padding:15px">Subnet Group Delete Multiple</td>
    <td style="padding:15px">{base_path}/{version}/subnetgroup/delete?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postSubnetgroupAdd(body, callback)</td>
    <td style="padding:15px">Subnet Group Add</td>
    <td style="padding:15px">{base_path}/{version}/subnetgroup/add?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postSubnetDelete(body, callback)</td>
    <td style="padding:15px">Subnet Statistics</td>
    <td style="padding:15px">{base_path}/{version}/subnet/delete?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSubnetPaged(networkAddress, orgName, draw, start, length, q, filterRules, sort, order, callback)</td>
    <td style="padding:15px">Subnet list page</td>
    <td style="padding:15px">{base_path}/{version}/subnet/paged?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postSubnetEdit(body, callback)</td>
    <td style="padding:15px">Updating Subnet</td>
    <td style="padding:15px">{base_path}/{version}/subnet/edit?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSubnetData(orgName, objIp, subnetAddress, callback)</td>
    <td style="padding:15px">Subnet Details</td>
    <td style="padding:15px">{base_path}/{version}/subnet/getSubnetData?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postSubnetAdd(body, callback)</td>
    <td style="padding:15px">Subnet Add</td>
    <td style="padding:15px">{base_path}/{version}/subnet/add?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSubnetSearch(draw, start, length, filterRules, sort, order, callback)</td>
    <td style="padding:15px">Subnet Search</td>
    <td style="padding:15px">{base_path}/{version}/subnet/search?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">routerList(subnetAddress, orgName, callback)</td>
    <td style="padding:15px">Router Details</td>
    <td style="padding:15px">{base_path}/{version}/subnet/routers?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createAll(body, callback)</td>
    <td style="padding:15px">Subnet Add All</td>
    <td style="padding:15px">{base_path}/{version}/subnet/addall?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSubnetIpAddressOrganizationName(address, organizationName, callback)</td>
    <td style="padding:15px">Get Subnet</td>
    <td style="padding:15px">{base_path}/{version}/subnet/ip/{pathv1}/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSubnetActiveLeases(page, rows, serverAddress, subnetAddress, organizationName, dhcpType, callback)</td>
    <td style="padding:15px">Active Leases List</td>
    <td style="padding:15px">{base_path}/{version}/subnet/activeLeases?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSubnetGetSubnetDetails(subnetId, subnetAddress, orgName, callback)</td>
    <td style="padding:15px">Subnet Details</td>
    <td style="padding:15px">{base_path}/{version}/subnet/getSubnetDetails?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postSubnetMultiadd(body, callback)</td>
    <td style="padding:15px">Subnet Add Multiple</td>
    <td style="padding:15px">{base_path}/{version}/subnet/multiadd?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSubnetStats(id, address, callback)</td>
    <td style="padding:15px">Subnet Stats</td>
    <td style="padding:15px">{base_path}/{version}/subnet/stats?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listInRange(networkIp, startSubnet, endSubnet, organizationName, callback)</td>
    <td style="padding:15px">Subnet Range List</td>
    <td style="padding:15px">{base_path}/{version}/subnet/listinrange?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">exMerge(body, callback)</td>
    <td style="padding:15px">Subnet Merge</td>
    <td style="padding:15px">{base_path}/{version}/subnet/merge?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">searchSubnetReferences(page, rows, tables, id, count, callback)</td>
    <td style="padding:15px">Get Subnet References</td>
    <td style="padding:15px">{base_path}/{version}/subnet/references?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listFreeSubnets(ipAddress, organizationName, callback)</td>
    <td style="padding:15px">Free Subnet List</td>
    <td style="padding:15px">{base_path}/{version}/subnet/listFreeSubnets?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getViewsAssociated(id, subnetAddress, organizationName, callback)</td>
    <td style="padding:15px">Subnet Details</td>
    <td style="padding:15px">{base_path}/{version}/subnet/getViewsAssociated?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSubnetGet(subnetId, address, organizationName, callback)</td>
    <td style="padding:15px">Subnet Details</td>
    <td style="padding:15px">{base_path}/{version}/subnet/get?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">split(body, callback)</td>
    <td style="padding:15px">Subnet Split</td>
    <td style="padding:15px">{base_path}/{version}/subnet/split?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSubnetTemplateSearch(draw, start, length, filterRules, sort, order, callback)</td>
    <td style="padding:15px">List Subnet Template Search elements</td>
    <td style="padding:15px">{base_path}/{version}/subnet_template/search?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSubnetTemplateDetails(name, orgName, callback)</td>
    <td style="padding:15px">Subnet Template Get</td>
    <td style="padding:15px">{base_path}/{version}/subnet_template/details?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSubnetTemplatePListbyorg(orgId, draw, start, length, orgName, q, inSubnetEdit, filterRules, sort, order, callback)</td>
    <td style="padding:15px">Subnet Template List By Organization</td>
    <td style="padding:15px">{base_path}/{version}/subnet_template/p_listbyorg?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postSubnetTemplateEdit(body, callback)</td>
    <td style="padding:15px">Update Subnet Template</td>
    <td style="padding:15px">{base_path}/{version}/subnet_template/edit?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSubnetTemplateList(callback)</td>
    <td style="padding:15px">Subnet Template List</td>
    <td style="padding:15px">{base_path}/{version}/subnet_template/list?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postSubnetTemplateDelete(name, orgName, callback)</td>
    <td style="padding:15px">Subnet Template Delete</td>
    <td style="padding:15px">{base_path}/{version}/subnet_template/delete?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postSubnetTemplateAdd(body, callback)</td>
    <td style="padding:15px">Subnet Template Add</td>
    <td style="padding:15px">{base_path}/{version}/subnet_template/add?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">switchPage(draw, start, length, q, filterRules, sort, order, callback)</td>
    <td style="padding:15px">Switch Details</td>
    <td style="padding:15px">{base_path}/{version}/switchdisco/switchpage?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">vlanPage(draw, start, length, q, filterRules, sort, order, switchIP, callback)</td>
    <td style="padding:15px">Vlan Details</td>
    <td style="padding:15px">{base_path}/{version}/switchdisco/vlanpage?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">interfacePage(draw, start, length, q, filterRules, sort, order, switchIP, callback)</td>
    <td style="padding:15px">Interface Details</td>
    <td style="padding:15px">{base_path}/{version}/switchdisco/interfacepage?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">elementPage(draw, start, length, q, filterRules, sort, order, switchIP, callback)</td>
    <td style="padding:15px">Element Details</td>
    <td style="padding:15px">{base_path}/{version}/switchdisco/elementpage?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSwitch(switchIP, callback)</td>
    <td style="padding:15px">Switch Details</td>
    <td style="padding:15px">{base_path}/{version}/switchdisco/switchdetails?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteSwitches(body, callback)</td>
    <td style="padding:15px">Deletes switch results</td>
    <td style="padding:15px">{base_path}/{version}/switchdisco/deleteSwitches?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">zoneTemplateParams(templateName, organizationId, organizationName, callback)</td>
    <td style="padding:15px">Zone Template Parameter List</td>
    <td style="padding:15px">{base_path}/{version}/templates/zoneparams?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">dnsTemplateParams(templateId, templateName, entityTypeCode, serverTypeCode, organizationId, organizationName, callback)</td>
    <td style="padding:15px">Option Template DNS Params List</td>
    <td style="padding:15px">{base_path}/{version}/templates/list/dnsparams?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">serversList(organizationId, callback)</td>
    <td style="padding:15px">Template List DNS Appliances</td>
    <td style="padding:15px">{base_path}/{version}/templates/listdnsservers?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">nsdTemplateParams(templateId, templateName, entityTypeCode, serverTypeCode, organizationId, organizationName, callback)</td>
    <td style="padding:15px">Option Template NSD Params List</td>
    <td style="padding:15px">{base_path}/{version}/templates/list/nsdparams?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">dataTypeList(datatypeFlag, callback)</td>
    <td style="padding:15px">Template List Data Types</td>
    <td style="padding:15px">{base_path}/{version}/templates/list-datatypes?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTemplatesListServerTypeCodeEntityTypeCode(serverTypeCode, entityTypeCode, referenceType, referenceId, q, filterRules, draw, start, length, callback)</td>
    <td style="padding:15px">Template List</td>
    <td style="padding:15px">{base_path}/{version}/templates/list/{pathv1}/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTemplatesPageServerTypeCodeEntityTypeCode(serverTypeCode, entityTypeCode, referenceType, referenceId, q, filterRules, inEdit, draw, start, length, organizationName, callback)</td>
    <td style="padding:15px">Template Page</td>
    <td style="padding:15px">{base_path}/{version}/templates/page/{pathv1}/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">dnsTemplateList(entityTypeCode, organizationId, organizationName, inEdit, callback)</td>
    <td style="padding:15px">DNS Template List</td>
    <td style="padding:15px">{base_path}/{version}/templates/dnstemplatelist?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">dhcpParamsList(serverType, entityType, callback)</td>
    <td style="padding:15px">DHCP Options Template List Params</td>
    <td style="padding:15px">{base_path}/{version}/templates/list/params/{pathv1}/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">templateParamsListAllById(templateId, templateName, serverType, entityType, organizationName, callback)</td>
    <td style="padding:15px">Template List all params</td>
    <td style="padding:15px">{base_path}/{version}/templates/list-all/params?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listCustomDefinedParams(templateId, templateName, callback)</td>
    <td style="padding:15px">User Defined DHCP Options List</td>
    <td style="padding:15px">{base_path}/{version}/templates/list-custom-defined-params?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">templateParamsListByName(templateName, callback)</td>
    <td style="padding:15px">Template Params List By Name</td>
    <td style="padding:15px">{base_path}/{version}/templates/list/paramsbyname/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">dnsTemplateInfo(templateId, templateName, entityTypeCode, serverTypeCode, organizationId, organizationName, callback)</td>
    <td style="padding:15px">Option Template DNS Params List</td>
    <td style="padding:15px">{base_path}/{version}/templates/list/dnsTemplateInfo?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">nsdTemplateParamsInfo(templateId, templateName, entityTypeCode, serverTypeCode, organizationId, organizationName, callback)</td>
    <td style="padding:15px">Option Template NSD Params List</td>
    <td style="padding:15px">{base_path}/{version}/templates/list/nsdparamsinfo?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">templateCreate(body, callback)</td>
    <td style="padding:15px">Template Create</td>
    <td style="padding:15px">{base_path}/{version}/templates/create?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">templateUpdate(body, callback)</td>
    <td style="padding:15px">Zone Template Update</td>
    <td style="padding:15px">{base_path}/{version}/templates/update?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">policyTemplateUpdate(body, callback)</td>
    <td style="padding:15px">DHCP Policy Template Update</td>
    <td style="padding:15px">{base_path}/{version}/templates/dhcp-policy-template-update?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">templateDHCPUpdate(body, callback)</td>
    <td style="padding:15px">DHCP Option Template Update</td>
    <td style="padding:15px">{base_path}/{version}/templates/dhcp-option-template-update?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">templateDeleteByIds(body, callback)</td>
    <td style="padding:15px">Delete DHCP Option and Policy Templates</td>
    <td style="padding:15px">{base_path}/{version}/templates/delete/dhcpTmpl?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">templateDeleteByNames(body, callback)</td>
    <td style="padding:15px">Zone Template Delete</td>
    <td style="padding:15px">{base_path}/{version}/templates/delete/names?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getZoneTemplateDetails(templateName, organizationId, organizationName, callback)</td>
    <td style="padding:15px">Zone Template Parameter List</td>
    <td style="padding:15px">{base_path}/{version}/templates/getZoneTemplateDetails?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">dhcpTemplateCreate(body, callback)</td>
    <td style="padding:15px">Add DHCP Option and Policy Templates</td>
    <td style="padding:15px">{base_path}/{version}/templates/dhcp-template-create?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">templatePage(draw, start, length, filterRules, sort, order, callback)</td>
    <td style="padding:15px">Template Search</td>
    <td style="padding:15px">{base_path}/{version}/templates/search?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">paramSearch(draw, start, length, filterRules, sort, order, callback)</td>
    <td style="padding:15px">Template Search</td>
    <td style="padding:15px">{base_path}/{version}/templates/params-name/search?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">paramValueSearch(draw, start, length, filterRules, sort, order, callback)</td>
    <td style="padding:15px">Template Search</td>
    <td style="padding:15px">{base_path}/{version}/templates/params-value/search?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">searchZoneTemplateReferences(page, rows, tables, id, count, callback)</td>
    <td style="padding:15px">Find DHCP Option Template References</td>
    <td style="padding:15px">{base_path}/{version}/templates/references?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listSubOptions(templateId, templateName, callback)</td>
    <td style="padding:15px">DHCP Sub Options List</td>
    <td style="padding:15px">{base_path}/{version}/templates/list-template-option-spaces?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTimeZone(callback)</td>
    <td style="padding:15px">Time Zone of the Server</td>
    <td style="padding:15px">{base_path}/{version}/timezone/getTimeZone?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTimezoneList(callback)</td>
    <td style="padding:15px">Get list of all Time Zones</td>
    <td style="padding:15px">{base_path}/{version}/timezone/list?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">purge(body, callback)</td>
    <td style="padding:15px">UndoData Purge</td>
    <td style="padding:15px">{base_path}/{version}/undo/purge?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postUndoPurgeAll(callback)</td>
    <td style="padding:15px">UndoData Purge</td>
    <td style="padding:15px">{base_path}/{version}/undo/purgeAll?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getUndoList(draw, start, length, q, filterRules, sort, order, callback)</td>
    <td style="padding:15px">UndoData List</td>
    <td style="padding:15px">{base_path}/{version}/undo/list?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postUndoRestore(body, callback)</td>
    <td style="padding:15px">UndoData Restore</td>
    <td style="padding:15px">{base_path}/{version}/undo/restore?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getUser(serialNum, callback)</td>
    <td style="padding:15px">Get User ID</td>
    <td style="padding:15px">{base_path}/{version}/user_cert/get_user?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getUserCertList(callback)</td>
    <td style="padding:15px">User Cert List</td>
    <td style="padding:15px">{base_path}/{version}/user_cert/list?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postUserCertImport(cfile, user, callback)</td>
    <td style="padding:15px">User Cert Import</td>
    <td style="padding:15px">{base_path}/{version}/user_cert/import?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getUsers(callback)</td>
    <td style="padding:15px">User List</td>
    <td style="padding:15px">{base_path}/{version}/user_cert/userlist?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postUserCertDelete(body, callback)</td>
    <td style="padding:15px">User Cert Delete</td>
    <td style="padding:15px">{base_path}/{version}/user_cert/delete?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postUserEdit(body, callback)</td>
    <td style="padding:15px">Admin Update</td>
    <td style="padding:15px">{base_path}/{version}/user/edit?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">searchAdminReferences(page, rows, tables, id, count, callback)</td>
    <td style="padding:15px">Lists Admin References</td>
    <td style="padding:15px">{base_path}/{version}/user/references?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">sessionUser(callback)</td>
    <td style="padding:15px">Session User Information</td>
    <td style="padding:15px">{base_path}/{version}/user/session_user?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">delLogin(loginName, callback)</td>
    <td style="padding:15px">Admin Delete</td>
    <td style="padding:15px">{base_path}/{version}/user/deleteLogin?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getChangeTicket(callback)</td>
    <td style="padding:15px">Get Change Ticket</td>
    <td style="padding:15px">{base_path}/{version}/user/change_ticket?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postUserChangeTicket(body, callback)</td>
    <td style="padding:15px">Update Change Ticket</td>
    <td style="padding:15px">{base_path}/{version}/user/change_ticket?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getInternalUser(callback)</td>
    <td style="padding:15px">Internal User Information</td>
    <td style="padding:15px">{base_path}/{version}/user/getInternalUser?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">userPage(page, rows, orgId, orgName, callback)</td>
    <td style="padding:15px">Searches User Page</td>
    <td style="padding:15px">{base_path}/{version}/user/search?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">expandedPermissions(userId, loginName, callback)</td>
    <td style="padding:15px">User Permission List</td>
    <td style="padding:15px">{base_path}/{version}/user/permexpndlist?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listPermissions(callback)</td>
    <td style="padding:15px">Permission List.</td>
    <td style="padding:15px">{base_path}/{version}/user/listpermission?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getUserGet(id, firstName, middleName, lastName, emailId, callback)</td>
    <td style="padding:15px">Admin Details</td>
    <td style="padding:15px">{base_path}/{version}/user/get?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">clone(userId, cfirstName, cmiddleName, clastName, cemailId, body, callback)</td>
    <td style="padding:15px">Admin Clone</td>
    <td style="padding:15px">{base_path}/{version}/user/clone?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getUserPage(orgId, orgName, grpId, grpName, draw, start, length, q, filterRules, sort, order, callback)</td>
    <td style="padding:15px">Admin List Page</td>
    <td style="padding:15px">{base_path}/{version}/user/page?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getUserList(orgId, orgName, grpId, grpName, callback)</td>
    <td style="padding:15px">Admin List</td>
    <td style="padding:15px">{base_path}/{version}/user/list?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">permissions(userId, loginName, callback)</td>
    <td style="padding:15px">Admin List Permissions</td>
    <td style="padding:15px">{base_path}/{version}/user/permlist?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postUserAdd(body, callback)</td>
    <td style="padding:15px">Admin Add</td>
    <td style="padding:15px">{base_path}/{version}/user/add?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postV6auditreportsCsvreport(body, action, callback)</td>
    <td style="padding:15px">CSV Report</td>
    <td style="padding:15px">{base_path}/{version}/v6auditreports/csvreport?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postV6auditreportsPdfreport(body, callback)</td>
    <td style="padding:15px">PDF Report</td>
    <td style="padding:15px">{base_path}/{version}/v6auditreports/pdfreport?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postV6auditreportsEmailreport(body, callback)</td>
    <td style="padding:15px">Email Report</td>
    <td style="padding:15px">{base_path}/{version}/v6auditreports/emailreport?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postV6auditreportsSchedEmailreport(body, repeat, frequency, runtime, starttime, endtime, day, description, repeatInterval, repeatCount, adminName, adminRole, callback)</td>
    <td style="padding:15px">Schedule Report Email</td>
    <td style="padding:15px">{base_path}/{version}/v6auditreports/sched_emailreport?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getV6auditreportsV6networkslist(actionType, orgId, page, rows, q, callback)</td>
    <td style="padding:15px">IPv6 Network List</td>
    <td style="padding:15px">{base_path}/{version}/v6auditreports/v6networkslist?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getMonitReportRecordsForGrid(reportType, fromDate, toDate, ipAddressDomainName, orgId, organization, draw, start, length, filterRules, action, sort, order, callback)</td>
    <td style="padding:15px">IPv6 Audit</td>
    <td style="padding:15px">{base_path}/{version}/v6auditreports/reportlist?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAuditSubnetGrpList(orgId, page, rows, q, callback)</td>
    <td style="padding:15px">IPv6 Subnet Group List (Audit Report)</td>
    <td style="padding:15px">{base_path}/{version}/v6auditreports/v6subnetgrplist?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getIPv6NetworkList(callback)</td>
    <td style="padding:15px">IPv6 Network List (Audit Report)</td>
    <td style="padding:15px">{base_path}/{version}/v6auditreports/ipv6Network?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getIPv6SubnetList(callback)</td>
    <td style="padding:15px">IPv6 Subnet List (Audit Report)</td>
    <td style="padding:15px">{base_path}/{version}/v6auditreports/ipv6Subnet?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listForServer(organizationId, organizationName, serverId, serverIp, callback)</td>
    <td style="padding:15px">View List For Server</td>
    <td style="padding:15px">{base_path}/{version}/view/listforserver?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postViewEdit(body, callback)</td>
    <td style="padding:15px">View Edit</td>
    <td style="padding:15px">{base_path}/{version}/view/edit?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listViewsForZone(organizationId, organizationName, zoneName, callback)</td>
    <td style="padding:15px">View List for Zone</td>
    <td style="padding:15px">{base_path}/{version}/view/listforzone?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listForZoneTemplate(organizationId, organizationName, templateId, templateName, callback)</td>
    <td style="padding:15px">View List For Zone Template</td>
    <td style="padding:15px">{base_path}/{version}/view/listforzonetemplate?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">checkViewListAssociatedWithTemplate(organizationId, organizationName, templateId, templateName, callback)</td>
    <td style="padding:15px">View List Associated with template</td>
    <td style="padding:15px">{base_path}/{version}/view/listassociatedwithtemplate?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listForZone(viewId, viewName, organizationId, callback)</td>
    <td style="padding:15px">Zone List for View</td>
    <td style="padding:15px">{base_path}/{version}/view/zonelistforview?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listcountries(callback)</td>
    <td style="padding:15px">Countries List</td>
    <td style="padding:15px">{base_path}/{version}/view/countries?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getViewGet(organizationId, organizationName, viewId, viewName, callback)</td>
    <td style="padding:15px">View Get</td>
    <td style="padding:15px">{base_path}/{version}/view/get?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getViewList(organizationId, organizationName, applyOrg, callback)</td>
    <td style="padding:15px">View List</td>
    <td style="padding:15px">{base_path}/{version}/view/list?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postViewDelete(organizationId, organizationName, body, callback)</td>
    <td style="padding:15px">View Delete</td>
    <td style="padding:15px">{base_path}/{version}/view/delete?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postViewCreate(body, callback)</td>
    <td style="padding:15px">View Create</td>
    <td style="padding:15px">{base_path}/{version}/view/create?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateWidgetStatus(widgetName, status, callback)</td>
    <td style="padding:15px">Network Statistics</td>
    <td style="padding:15px">{base_path}/{version}/widget/update?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getWidgetList(category, callback)</td>
    <td style="padding:15px">Network Statistics</td>
    <td style="padding:15px">{base_path}/{version}/widget/list?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postWsdhcpserverDelete(address, callback)</td>
    <td style="padding:15px">Microsoft DHCP Appliance Delete</td>
    <td style="padding:15px">{base_path}/{version}/wsdhcpserver/delete?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getWsdhcpserverPage(organizationName, draw, start, length, filterRules, q, sort, order, callback)</td>
    <td style="padding:15px">Microsoft DHCP Appliance List</td>
    <td style="padding:15px">{base_path}/{version}/wsdhcpserver/page?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getWsdhcpserverDhcpActiveLeases(serverIp, organizationName, callback)</td>
    <td style="padding:15px">Get DHCP Appliances Active Leases</td>
    <td style="padding:15px">{base_path}/{version}/wsdhcpserver/dhcpActiveLeases?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">restartDHCPService(serverIp, orgName, callback)</td>
    <td style="padding:15px">Restart DHCP service on Microsoft DHCP Appliance</td>
    <td style="padding:15px">{base_path}/{version}/wsdhcpserver/restartService?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">syncConfig(address, organizationName, callback)</td>
    <td style="padding:15px">Generate appliance Configuration for sync</td>
    <td style="padding:15px">{base_path}/{version}/wsdhcpserver/syncConfig?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listServers(orgName, callback)</td>
    <td style="padding:15px">Microsoft DHCP appliance list</td>
    <td style="padding:15px">{base_path}/{version}/wsdhcpserver/list?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getApplianceDetails(ip, orgname, callback)</td>
    <td style="padding:15px">Get Appliance Configuration</td>
    <td style="padding:15px">{base_path}/{version}/wsdhcpserver/getdetails?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateDHCPappliance(body, callback)</td>
    <td style="padding:15px">Microsoft DHCP Appliance Update</td>
    <td style="padding:15px">{base_path}/{version}/wsdhcpserver/update?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postWsdhcpserverAdd(body, callback)</td>
    <td style="padding:15px">Microsoft DHCP Appliance Add</td>
    <td style="padding:15px">{base_path}/{version}/wsdhcpserver/add?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">serverList(draw, start, length, filterRules, sort, order, callback)</td>
    <td style="padding:15px">The Microsoft DNS Appliance List</td>
    <td style="padding:15px">{base_path}/{version}/windowsdns/page?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getWindowsdnsSearch(page, rows, callback)</td>
    <td style="padding:15px">Appliance Search</td>
    <td style="padding:15px">{base_path}/{version}/windowsdns/search?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">syncServer(ip, orgname, orgid, callback)</td>
    <td style="padding:15px">Server Configuration</td>
    <td style="padding:15px">{base_path}/{version}/windowsdns/sync?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAssociatedMicrosoftAppliance(templateName, orgname, callback)</td>
    <td style="padding:15px">Server Configuration</td>
    <td style="padding:15px">{base_path}/{version}/windowsdns/getassociatedmicrosoftappliances?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getapplianceDetailsByOrg(orgName, orgId, callback)</td>
    <td style="padding:15px">Server Configuration</td>
    <td style="padding:15px">{base_path}/{version}/windowsdns/getdetailsbyorg?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getWindowsdnsReferences(page, rows, tables, id, count, callback)</td>
    <td style="padding:15px">Microsoft Appliance References In Zones</td>
    <td style="padding:15px">{base_path}/{version}/windowsdns/references?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createServer(body, callback)</td>
    <td style="padding:15px">Add Microsoft DNS Appliance</td>
    <td style="padding:15px">{base_path}/{version}/windowsdns/add?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getapplianceDetails(ip, orgname, callback)</td>
    <td style="padding:15px">Server Configuration</td>
    <td style="padding:15px">{base_path}/{version}/windowsdns/getdetails?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">editServer(body, callback)</td>
    <td style="padding:15px">The Microsoft DNS appliance Edit</td>
    <td style="padding:15px">{base_path}/{version}/windowsdns/edit?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteServer(ip, callback)</td>
    <td style="padding:15px">The Microsoft DNS appliance delete</td>
    <td style="padding:15px">{base_path}/{version}/windowsdns/delete?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">pingServer(body, callback)</td>
    <td style="padding:15px">The Microsoft DNS appliance ping and port availability check</td>
    <td style="padding:15px">{base_path}/{version}/windowsdns/pingPortCheck?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">modifyWorkflow(body, callback)</td>
    <td style="padding:15px">modifyWorkflow</td>
    <td style="padding:15px">{base_path}/{version}/workflow/modify?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteWorkFlow(body, callback)</td>
    <td style="padding:15px">deleteWorkFlow</td>
    <td style="padding:15px">{base_path}/{version}/workflow/delete?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getWorkFlowProfile(id, callback)</td>
    <td style="padding:15px">getWorkFlowProfile</td>
    <td style="padding:15px">{base_path}/{version}/workflow/profile?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getWorkFlows(callback)</td>
    <td style="padding:15px">getWorkFlows</td>
    <td style="padding:15px">{base_path}/{version}/workflow/listrows?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">stageWorkflow(body, callback)</td>
    <td style="padding:15px">stageWorkflow</td>
    <td style="padding:15px">{base_path}/{version}/workflow/add?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getZone(body, callback)</td>
    <td style="padding:15px">Zone Get</td>
    <td style="padding:15px">{base_path}/{version}/zone/getZone?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postZoneMultidelete(body, callback)</td>
    <td style="padding:15px">Zone Delete Multiple</td>
    <td style="padding:15px">{base_path}/{version}/zone/multidelete?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getZoneForcesynczones(zoneOrgNames, callback)</td>
    <td style="padding:15px">Zone Force Sync</td>
    <td style="padding:15px">{base_path}/{version}/zone/forcesynczones?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getZoneSearch(page, rows, callback)</td>
    <td style="padding:15px">Zone Search</td>
    <td style="padding:15px">{base_path}/{version}/zone/search?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getZoneGetZoneRRDataExpandViewOwnerName(ownerName, callback)</td>
    <td style="padding:15px">Zone RR Expand View</td>
    <td style="padding:15px">{base_path}/{version}/zone/getZoneRRDataExpandView/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getZoneListServers(type, name, organization, callback)</td>
    <td style="padding:15px">List Appliances</td>
    <td style="padding:15px">{base_path}/{version}/zone/list-servers?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getZoneList(revZone, callback)</td>
    <td style="padding:15px">Zone List</td>
    <td style="padding:15px">{base_path}/{version}/zone/list?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">dnssecEnabledZoneList(orgName, callback)</td>
    <td style="padding:15px">Zone List</td>
    <td style="padding:15px">{base_path}/{version}/zone/dnssec_enabled_list?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">zonePageList(draw, start, length, filterRules, sort, order, callback)</td>
    <td style="padding:15px">Zone List</td>
    <td style="padding:15px">{base_path}/{version}/zone/page?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postZoneMonitoring(body, callback)</td>
    <td style="padding:15px">Zone Monitoring Update</td>
    <td style="padding:15px">{base_path}/{version}/zone/monitoring?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">rootZoneList(callback)</td>
    <td style="padding:15px">Root Zone List</td>
    <td style="padding:15px">{base_path}/{version}/zone/root/list?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">rootZoneSearch(page, rows, callback)</td>
    <td style="padding:15px">Root Zone Search</td>
    <td style="padding:15px">{base_path}/{version}/zone/rootzone/search?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">proxyRootZoneList(callback)</td>
    <td style="padding:15px">Proxy Root Zone List</td>
    <td style="padding:15px">{base_path}/{version}/zone/proxyroot/list?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">proxyRootZoneSearchList(page, rows, callback)</td>
    <td style="padding:15px">Proxy Root Zone Search List</td>
    <td style="padding:15px">{base_path}/{version}/zone/proxyroot/search?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createRootZone(body, callback)</td>
    <td style="padding:15px">Root Zone Add</td>
    <td style="padding:15px">{base_path}/{version}/zone/root/add?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createProxyRootZone(body, callback)</td>
    <td style="padding:15px">Proxy Root Zone Add</td>
    <td style="padding:15px">{base_path}/{version}/zone/proxyroot/add?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">rootZoneMultiDelete(body, callback)</td>
    <td style="padding:15px">Root Zone Multiple Delete</td>
    <td style="padding:15px">{base_path}/{version}/zone/root/multidelete?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">rootProxyZoneMultiDelete(body, callback)</td>
    <td style="padding:15px">Proxy Root Zone Multiple Delete</td>
    <td style="padding:15px">{base_path}/{version}/zone/proxyroot/multidelete?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateRootZone(body, callback)</td>
    <td style="padding:15px">Root Zone Edit</td>
    <td style="padding:15px">{base_path}/{version}/zone/root/update?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateProxyRootZone(body, callback)</td>
    <td style="padding:15px">Proxy Root Zone Edit</td>
    <td style="padding:15px">{base_path}/{version}/zone/proxyroot/update?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">checkACNAMEconflict(ownerName, orgId, orgName, recordType, callback)</td>
    <td style="padding:15px">Zone Resource Record Check CNAME Conflict</td>
    <td style="padding:15px">{base_path}/{version}/zone/rr/checkACNAMEconflict?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">checkRROrphan(ownerName, orgId, orgName, recordType, externalRR, callback)</td>
    <td style="padding:15px">Zone Resource Record Check Orphan</td>
    <td style="padding:15px">{base_path}/{version}/zone/rr/checkRROrphan?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">rrPage(draw, start, length, filterRules, sort, order, callback)</td>
    <td style="padding:15px">Zone Resource Record Search</td>
    <td style="padding:15px">{base_path}/{version}/zone/search/rr?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getZoneReferences(page, rows, tables, id, count, callback)</td>
    <td style="padding:15px">Lists Zone References</td>
    <td style="padding:15px">{base_path}/{version}/zone/references?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getZoneReferencesRr(page, rows, tables, id, count, callback)</td>
    <td style="padding:15px">Find Zone Resource Record References</td>
    <td style="padding:15px">{base_path}/{version}/zone/references/rr?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getZoneStatus(zoneName, orgName, callback)</td>
    <td style="padding:15px">Zone Status</td>
    <td style="padding:15px">{base_path}/{version}/zone/zoneStatus?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCommandResultForZone(zoneName, orgName, argc, callback)</td>
    <td style="padding:15px">Zone Command</td>
    <td style="padding:15px">{base_path}/{version}/zone/subsys?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">zoneListForServerView(serverId, serverIp, viewId, viewName, callback)</td>
    <td style="padding:15px">Zone List For Appliance View</td>
    <td style="padding:15px">{base_path}/{version}/zone/listforserverview?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">checkNSRecordInProxyServer(owner, data, orgName, callback)</td>
    <td style="padding:15px">Check NS Record In Proxy Root Zone</td>
    <td style="padding:15px">{base_path}/{version}/zone/check_ns_in_proxy?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">checkNSRecordInRootServer(owner, orgName, callback)</td>
    <td style="padding:15px">Zone Check NS Record In Root Zone</td>
    <td style="padding:15px">{base_path}/{version}/zone/check_ns_in_root?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">checkARecordExists(owner, callback)</td>
    <td style="padding:15px">Verification of A Resource Record</td>
    <td style="padding:15px">{base_path}/{version}/zone/check_for_A_RR?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">checkAAAARecordExists(owner, callback)</td>
    <td style="padding:15px">Verification of AAAA Resource Record</td>
    <td style="padding:15px">{base_path}/{version}/zone/check_for_AAAA_RR?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">checkMXRecordExists(owner, callback)</td>
    <td style="padding:15px">Verification of MX Resource Record</td>
    <td style="padding:15px">{base_path}/{version}/zone/check_for_MX_RR?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">checkRRIsRtdZOne(owner, zoneName, orgId, orgName, callback)</td>
    <td style="padding:15px">Check RR is Restricted Zone</td>
    <td style="padding:15px">{base_path}/{version}/zone/check_RR_is_rtd_zone?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getResultForIsZoneFrozen(zoneName, orgName, callback)</td>
    <td style="padding:15px">Zone is Frozen or not</td>
    <td style="padding:15px">{base_path}/{version}/zone/isFrozen?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getZoneTTLValue(domainName, orgName, callback)</td>
    <td style="padding:15px">Get Zone ttl</td>
    <td style="padding:15px">{base_path}/{version}/zone/getZoneTTL?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getManagedDomainList(orgId, draw, start, length, orgName, q, inSubnetEdit, filterRules, sort, order, callback)</td>
    <td style="padding:15px">Managed Domains List By Organization</td>
    <td style="padding:15px">{base_path}/{version}/zone/managedDomainListbyorg?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDSRecords(orgId, orgName, id, name, callback)</td>
    <td style="padding:15px">Get DS(Delegation Signer) record for the Managed Zone</td>
    <td style="padding:15px">{base_path}/{version}/zone/getDSRecord?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDNSSECInfo(orgId, orgName, id, name, isKsk, callback)</td>
    <td style="padding:15px">Get DNSSEC Keys information for the Managed DNS Zone</td>
    <td style="padding:15px">{base_path}/{version}/zone/getDNSSECInfo?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">manualKeyRollover(orgId, orgName, id, name, isKsk, callback)</td>
    <td style="padding:15px">Manual Key Rollover for the Managed DNS Zone.</td>
    <td style="padding:15px">{base_path}/{version}/zone/manualKeyRollover?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getZoneObjectARRList(orgId, orgName, id, name, start, length, filterRules, sort, order, callback)</td>
    <td style="padding:15px">Gets all the A records for a given zone in a given organization.</td>
    <td style="padding:15px">{base_path}/{version}/zone/getAllARecords?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addDC(body, callback)</td>
    <td style="padding:15px">Add Domain Controllers</td>
    <td style="padding:15px">{base_path}/{version}/zone/addDC?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDC(body, callback)</td>
    <td style="padding:15px">Delete domain controllers</td>
    <td style="padding:15px">{base_path}/{version}/zone/deleteDC?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">checkDuplicateZoneRRName(name, domainName, orgName, callback)</td>
    <td style="padding:15px">Object Duplicate Name Checker</td>
    <td style="padding:15px">{base_path}/{version}/zone/checkDuplicateZoneRRName?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">zoneListByOrg(orgName, callback)</td>
    <td style="padding:15px">Zone List By Organization</td>
    <td style="padding:15px">{base_path}/{version}/zone/getzonelistbyorg?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">aliasResourceRecordsList(zonename, orgname, callback)</td>
    <td style="padding:15px">Zone List</td>
    <td style="padding:15px">{base_path}/{version}/zone/aliasRRslist?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">syncZoneWithCloud(zonename, orgname, syncType, cloudProviderType, callback)</td>
    <td style="padding:15px">Sync Zone with cloud</td>
    <td style="padding:15px">{base_path}/{version}/zone/synczonewithcloud?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getGeoLocations(orgname, callback)</td>
    <td style="padding:15px">Geo Locations List</td>
    <td style="padding:15px">{base_path}/{version}/zone/geolocations?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getHealthChecks(orgname, callback)</td>
    <td style="padding:15px">Health Check ids List</td>
    <td style="padding:15px">{base_path}/{version}/zone/healthchecks?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">importAliasResourceRecords(zonename, orgname, importType, callback)</td>
    <td style="padding:15px">importAliasResourceRecords</td>
    <td style="padding:15px">{base_path}/{version}/zone/importaliasrecords?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getZoneData(domainName, orgName, callback)</td>
    <td style="padding:15px">Zone Get Data</td>
    <td style="padding:15px">{base_path}/{version}/zone/getZoneData?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getZoneGet(zoneID, domainName, orgName, isProxy, callback)</td>
    <td style="padding:15px">Get RR List</td>
    <td style="padding:15px">{base_path}/{version}/zone/get?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getZoneGetProxyRR(zoneID, domainName, orgID, orgName, isProxy, callback)</td>
    <td style="padding:15px">Get Proxy Root Zone RR List</td>
    <td style="padding:15px">{base_path}/{version}/zone/getProxyRR?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postZoneClone(existingZone, newZone, orgName, callback)</td>
    <td style="padding:15px">Zone Clone</td>
    <td style="padding:15px">{base_path}/{version}/zone/clone?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postZoneUpdate(body, callback)</td>
    <td style="padding:15px">Zone Edit</td>
    <td style="padding:15px">{base_path}/{version}/zone/update?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postZoneRrEdit(existingOwner, existingData, body, callback)</td>
    <td style="padding:15px">Zone Resource Record Add</td>
    <td style="padding:15px">{base_path}/{version}/zone/rr/edit?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postZoneRrDelete(body, callback)</td>
    <td style="padding:15px">Zone Resource Record Delete</td>
    <td style="padding:15px">{base_path}/{version}/zone/rr/delete?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getZoneDeleteZoneName(zoneName, callback)</td>
    <td style="padding:15px">Zone Delete</td>
    <td style="padding:15px">{base_path}/{version}/zone/delete/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postZoneRrAdd(body, callback)</td>
    <td style="padding:15px">Zone Resource Record Add</td>
    <td style="padding:15px">{base_path}/{version}/zone/rr/add?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postZoneAdd(body, callback)</td>
    <td style="padding:15px">Zone Add</td>
    <td style="padding:15px">{base_path}/{version}/zone/add?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCloudNetworkGetNextAvailableNetwork(cloudProviderName, organizationName, mask, callback)</td>
    <td style="padding:15px">Gets Next Available Network Address</td>
    <td style="padding:15px">{base_path}/{version}/cloudNetwork/getNextAvailableNetwork?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createNetwork(body, callback)</td>
    <td style="padding:15px">Create Network in Cloud</td>
    <td style="padding:15px">{base_path}/{version}/cloudNetwork/createNetwork?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteNetwork(body, callback)</td>
    <td style="padding:15px">Delete Network in Cloud</td>
    <td style="padding:15px">{base_path}/{version}/cloudNetwork/deleteNetwork?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNextAvailableSubnet(cloudProviderName, organizationName, mask, vpcId, networkAddress, callback)</td>
    <td style="padding:15px">Gets Next Available Network Address</td>
    <td style="padding:15px">{base_path}/{version}/cloudSubnet/getNextAvailableSubnet?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createSubnet(body, callback)</td>
    <td style="padding:15px">Create Subnet in Cloud</td>
    <td style="padding:15px">{base_path}/{version}/cloudSubnet/createSubnet?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteSubnet(body, callback)</td>
    <td style="padding:15px">Delete Subnet in Cloud</td>
    <td style="padding:15px">{base_path}/{version}/cloudSubnet/deleteSubnet?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">discover(all, body, callback)</td>
    <td style="padding:15px">Discover cloud hosted subnets</td>
    <td style="padding:15px">{base_path}/{version}/cloudSubnet/discover?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDiscoveryCommands(callback)</td>
    <td style="padding:15px">Discovery commands</td>
    <td style="padding:15px">{base_path}/{version}/cloudSubnet/discovery/getCmds?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDiscoveryCommandStatus(cmdId, callback)</td>
    <td style="padding:15px">Discovery command</td>
    <td style="padding:15px">{base_path}/{version}/cloudSubnet/discovery/getCmdStatus?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">discardCmds(body, callback)</td>
    <td style="padding:15px">Discard discovery results</td>
    <td style="padding:15px">{base_path}/{version}/cloudSubnet/discovery/discardCmds?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDiscoveredSubnets(cmdId, callback)</td>
    <td style="padding:15px">Discovered cloud subnet list</td>
    <td style="padding:15px">{base_path}/{version}/cloudSubnet/getDiscoveredSubnets?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">importDiscoverdSubnets(cmdId, body, callback)</td>
    <td style="padding:15px">Create the discovered cloud subnets</td>
    <td style="padding:15px">{base_path}/{version}/cloudSubnet/createDiscoverdSubnets?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getVmwareAuthtypelist(callback)</td>
    <td style="padding:15px">getCloudAttributeNames</td>
    <td style="padding:15px">{base_path}/{version}/vmware/authtypelist?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAuthSource(objectId, ipType, callback)</td>
    <td style="padding:15px">getAuthSource</td>
    <td style="padding:15px">{base_path}/{version}/vmware/authsource?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getV6subnetTemplateDetails(name, orgName, callback)</td>
    <td style="padding:15px">IPv6 Subnet Template Get</td>
    <td style="padding:15px">{base_path}/{version}/v6subnet_template/details?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getV6subnetTemplatePListbyorg(orgId, draw, start, length, orgName, q, inSubnetEdit, filterRules, sort, order, callback)</td>
    <td style="padding:15px">IPv6 Subnet Template List By Organization</td>
    <td style="padding:15px">{base_path}/{version}/v6subnet_template/p_listbyorg?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postV6subnetTemplateEdit(body, callback)</td>
    <td style="padding:15px">Update IPv6 Subnet Template</td>
    <td style="padding:15px">{base_path}/{version}/v6subnet_template/edit?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getV6subnetTemplateList(callback)</td>
    <td style="padding:15px">IPv6 Subnet Template List</td>
    <td style="padding:15px">{base_path}/{version}/v6subnet_template/list?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postV6subnetTemplateDelete(name, orgName, callback)</td>
    <td style="padding:15px">IPv6 Subnet Template Delete</td>
    <td style="padding:15px">{base_path}/{version}/v6subnet_template/delete?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postV6subnetTemplateAdd(body, callback)</td>
    <td style="padding:15px">IPv6 Subnet Template Add</td>
    <td style="padding:15px">{base_path}/{version}/v6subnet_template/add?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
