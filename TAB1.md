# Overview 

This adapter is used to integrate the Itential Automation Platform (IAP) with the Tcpwave System. The API that was used to build the adapter for Tcpwave is usually available in the report directory of this adapter. The adapter utilizes the Tcpwave API to provide the integrations that are deemed pertinent to IAP. The ReadMe file is intended to provide information on this adapter it is generated from various other Markdown files.

## Details 
The TCPWave adapter from Itential is used to integrate the Itential Automation Platform (IAP) with TCPWave DDI to offer a DNS, DHCP and IPAM solution for configuration and orchestration. 

With this adapter you have the ability to perform operations with TCPWave such as:

- IPAM
- DNS
- DHCP

For further technical details on how to install and use this adapter, please click the Technical Documentation tab. 
